﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AlteraSenha.aspx.cs" Inherits="ApiAutenticacao.Page.AlteraSenha" Async="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Ferramenta para gerenciar seu convênio, colaboradores e contas da maneira mais humana">
    <meta name="keywords" content="cartao alimentacao, cartao convenio, convenio">
    <meta name="author" content="Vegas Card">
    <title>Login - Vegas Card Convênio
    </title>
    <link rel="apple-touch-icon" href="https://convenio.vegascard.com.br/app-assets/images/ico/apple-icon-120-v.png">
    <link rel="shortcut icon" type="image/x-icon" href="https://convenio.vegascard.com.br/app-assets/images/ico/favicon-v.ico">
    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700&display=swap" rel="stylesheet">
    <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://convenio.vegascard.com.br/app-assets/css/MaterialDesign/css/materialdesignicons.min.css ">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/app-assets/css/vendors.css">
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/app-assets/vendors/css/extensions/toastr.css">
    <!-- END VENDOR CSS-->
    <!-- BEGIN MODERN CSS-->
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/app-assets/css/app.css">
    <!-- END MODERN CSS-->
    <!-- BEGIN Page Level CSS-->
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/app-assets/css/core/menu/menu-types/horizontal-menu.css">
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/app-assets/css/core/colors/palette-gradient.css">
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/app-assets/css/pages/login-register.css">
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="https://convenio.vegascard.com.br/assets/css/style-v.css?v=0.0.23">
    <!-- END Custom CSS-->
</head>
<body  class="horizontal-layout horizontal-menu horizontal-menu-padding 1-column menu-expanded blank-page blank-page p-0" data-open="click" data-menu="horizontal-menu" data-col="1-column">
    <div class="app-content container center-layout mt-0">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-md-12 d-flex align-items-center justify-content-center">
                        <div class="col-md-6 p-0">
                            <div class="card border-grey border-lighten-3 m-0">
                                <div class="card-header border-0 p-0">
                                    <div class="card-title text-center">
                                        <div class="p-1">
                                            <img src="https://convenio.vegascard.com.br/assets/img/logo-v.png" style="max-width: 80px;" alt="Vegas Card">
                                        </div>
                                    </div>
                                    <h6 class="card-subtitle line-on-side text-muted text-center font-small-3 pt-2">
                                        <span>Alterar Senha</span>
                                    </h6>
                                </div>
                                <div class="card-content">
                                    <div class="card-body">
                                        <div class="alert alert-message mb-2" id="Erro" role="alert" runat="server" visible="false">
                                            <div class="alert-text">Senha e confirmação de senha não conferem.</div>
                                        </div>
                                        <div class="alert alert-message mb-2" id="Sucesso" role="alert" runat="server" visible="false">
                                            <div class="alert-text">Senha Alterada com sucesso</div>
                                        </div>
                                        <div class="alert alert-message mb-2" id="Erro2" role="alert" runat="server" visible="false">
                                            <div class="alert-text">Erro ao enviar dados.</div>
                                        </div>
                                        <form id="form1" runat="server">    
                                            <div class="form-group position-relative has-icon-left">
                                                <asp:TextBox ID="Senha" class="form-control form-control-lg input-lg" placeholder="Digite a Senha" runat="server" TextMode="Password"></asp:TextBox>
                                                <div class="form-control-position" style="top:0;">
                                                </div>
                                            </div>
                                            <div class="form-group position-relative has-icon-left">
                                                <asp:TextBox ID="txtCSenha" class="form-control form-control-lg input-lg" placeholder="Confirme a Senha" runat="server" TextMode="Password"></asp:TextBox>
                                                <div class="form-control-position" style="top:0;">
                                                </div>
                                            </div>
                                            <asp:Button ID="btnEnviar" runat="server" Text="Alterar Senha" class="btn btn-primary btn-lg btn-block m-0" OnClick="btnEnviar_Click" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</body>
</html>
