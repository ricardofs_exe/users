﻿using ApiAutenticacao.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ApiAutenticacao.Page
{
    public partial class AlteraSenha : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Erro.Visible = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {

                if ((Senha.Text != txtCSenha.Text) || (Senha.Text == "" || txtCSenha.Text== ""))
                {
                    Erro.Visible = true;
                }
                else
                {
                    string UserId = Request.QueryString["UserId"].ToString();
                    //string Token = Request.QueryString["token"].ToString();
                    string Email = Request.QueryString["email"].ToString();
                    string SenhaE = Senha.Text;

                    AlterarSenha(UserId, Email, SenhaE);
                }
            }
            catch (Exception ex)
            {
                //XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }

        private async void AlterarSenha(string id, string mail, string senha)
        {
            try
            {
                string url = "http://localhost:58260/api/account/AlteraConta";

                ConfirmacaoAlteracaoSenha _conf = new ConfirmacaoAlteracaoSenha();

                _conf.Email = mail;
                _conf.NovaSenha = senha;
                _conf.Token = "";

                using (var client = new HttpClient())
                {
                    var serializedUsuario = JsonConvert.SerializeObject(_conf);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        Sucesso.Visible = true;
                        form1.Visible = false;

                    }
                    else
                    {
                        Erro2.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Erro2.Visible = true;
            }
        }
    }
}