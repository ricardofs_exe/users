﻿using ApiAutenticacao.API;
using ApiAutenticacao.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ApiAutenticacao.API.App_Start.Identity;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data.Entity;

namespace ApiAutenticacao
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _authContext;
        IDataProtector _protector;

        private UserManager<IdentityUser> _userManager;

        public AuthRepository(IDataProtectionProvider provider)
        {
            _protector = provider.Create("ApiAutenticacao");
        }


        public AuthRepository()
        {
            _authContext = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_authContext));
            


            var provider = new DpapiDataProtectionProvider("ApiAutenticacao");
            _userManager.UserTokenProvider = new DataProtectorTokenProvider<IdentityUser>(provider.Create("ApiAutenticacao"));
            

        }

        public async Task<IdentityResult> RegisterUser(UserModel userModel)
        {

            IdentityUser user = new IdentityUser()
            {
                UserName = userModel.UserName,
                Email = userModel.Email
            };

            var result = await _userManager.CreateAsync(user, userModel.Password);

            try
            {
                return result;
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                return result;
            }

        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            IdentityUser user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public async Task<IdentityUser> FindMail(string Email)
        {
            IdentityUser user = await _userManager.FindByEmailAsync(Email);

            return user;
        }


        public async Task<String> AlterarSenha(string userid, string token, string novasenha )
        {
            string dados;

            try
            {
                var token1 = await _userManager.GeneratePasswordResetTokenAsync(userid);
                


                var resultado = await _userManager.ResetPasswordAsync(
                    userid,
                    token1,
                    novasenha
                    );

                if (resultado.Succeeded)
                    dados = "OK";
                else
                    dados = "Erro";

                return dados;

            }
            catch (Exception ex)
            {
                dados = ex.Message.ToString();
                return dados;
            }
        }

        public async Task<String> GerarToken(string Email)
        {
            string token = "";

            try
            {
                IdentityUser user = await _userManager.FindByEmailAsync(Email);

                string Login = user.UserName;
                token = "Ok";

                string email =
                "    <div style='display: flex; flex-flow: row wrap;  padding: 1px; #opcional' >" +
                "    <div style=' width: 100%; height: 200px; text-align: center; color: #ff0000; line-height: 200px; background-color: #ffffff' > <img src='https://convenio.vegascard.com.br/assets/img/logo-horizontal-v.png' alt='Logo Vegas Card' height='100'></div >" +
                "    </div >" +
                "   <div style='display: flex; flex-flow: row wrap;  padding: 1px; #opcional' >" +
                "    <div style='width: 100%; height:150px; text-align:center; color: #ffffff; line-height:150px; font-size: 35px; background-color: #2658A6' > Alteração de senha de usuario</div >" +
                "	</div >" +
                "    <div style='display: flex; flex-flow: row wrap;  padding: 1px; #opcional' >" +
                "    <div style=' width:100%; text-align:center; font-size:20px;  background-color: #ffffff'>" +
                "    <p >" +
                "    Tudo bem " + Login.ToString() + " ?<br > <br >" +
                "    <a href='http://localhost:58260/Page/AlteraSenha.aspx?UserId=" + user.Id + "&email=" + Email + "' > Clique aqui alteração de senha </a >" +
                "    </p >" +
                "	</div>" +
                "    </div>";


                _userManager.EmailService = new EmailServico();
                await _userManager.SendEmailAsync(user.Id, "Envio de Token de Alteração de Senha", email);

            }
            catch (Exception ex)
            {
                token = ex.ToString();
            }

            return token;
        }

        /*public async Task<String> ListaUser()
        {
            

            try
            {
                var users = _userManager.Users;
                dados = users.ToString();
            }
            catch (Exception ex)
            {
                dados = ex.ToString();
            }

            return dados;
        }*/

        public void Dispose()
        {
            _authContext.Dispose();
            _userManager.Dispose();
        }
    }
}