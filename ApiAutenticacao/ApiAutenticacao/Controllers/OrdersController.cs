﻿using ApiAutenticacao.API;
using ApiAutenticacao.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace ApiAutenticacao.Controllers
{

  
    [RoutePrefix("api/Orders")]
    public class OrdersController : ApiController
    {
        AuthContext db = new AuthContext();
        BaseDadosDBContext dba = new BaseDadosDBContext();


        [Authorize]
        [Route("Acesso")]
        public IHttpActionResult Get()
        {
            return Ok("ok");
        }
        /// <summary>
        /// Traz todos os usuários do sistema
        /// </summary>
        /// <returns>Retorna Jason de usuários</returns>
        [Authorize]
        [Route("Usuarios")]
        [HttpGet]
        public IHttpActionResult Usuarios()
        {
            
            var usuarios = dba.AspNetUsers.ToList();
            var statusUsuarios = dba.usuario_status.ToList();

            //Dados que serão enviados para o Grid
            var query = from user in usuarios
                        join users in statusUsuarios on user.usuario_status equals users.usuario_status_id
                        select new
                        {
                            Id = user.Id,
                            Email = user.Email,
                            Login = user.UserName,
                            Data = user.data,
                            Criado = user.criado_por,
                            Atualizado = user.atualizado,
                            Status = user.usuario_status,
                            Especial = user.especial,
                            Cargo = user.cargo,
                            Nome = user.nomeCompleto,
                            Mstatus = users.status
                        };
            
            var Dados = JsonConvert.SerializeObject(query);
            return Ok(Dados);
        }

        [Authorize]
        [Route("Status")]
        [HttpGet]
        public IHttpActionResult Status()
        {

            var dado = dba.usuario_status.ToList();
            var Dados = JsonConvert.SerializeObject(dado);
            return Ok(Dados);
        }

        [Authorize]
        [Route("InstStatus")]
        [HttpGet]
        public IHttpActionResult InstStatus()
        {

            var dado = dba.instituicao_status.ToList();
            var Dados = JsonConvert.SerializeObject(dado);
            return Ok(Dados);
        }


        /// <summary>
        /// Traz todas as instituições
        /// </summary>
        /// <returns>Json com as Intituições</returns>
        [Authorize]
        [Route("Instituicao")]
        [HttpGet]
        public IHttpActionResult Instituicao()
        {
            var instituicao = dba.instituicao.ToList();
            var instituicaoS = dba.instituicao_status.ToList();
            var query = from inst in instituicao
                        join instS in instituicaoS on inst.instituicao_status equals instS.instituicao_status_id
                        select new
                        {
                            instituicao_id = inst.instituicao_id,
                            data = inst.data,
                            instituicao_status = inst.instituicao_status,
                            status = instS.status,
                            nome = inst.nome,
                            descricao = inst.descricao,
                            cnpj = inst.cnpj,
                            atualizado = inst.atualizado,
                            usuario = inst.usuario,
                            criado_por = inst.criado_por
                        };

            var Dados = JsonConvert.SerializeObject(query);
            return Ok(Dados);
        }

        /// <summary>
        /// Traz todas a s instituições responsáveis
        /// </summary>
        /// <returns>Json com as Instituições Responsáveis</returns>
        [Authorize]
        [Route("InstituicaoResponsavel")]
        [HttpGet]
        public IHttpActionResult InstituicaoResponsavel()
        {
            var instituicao = dba.instituicao.ToList();
            var instRes = dba.instituicao_responsavel.ToList();
            var query = from inst in instituicao
                        join instr in instRes on inst.instituicao_id equals instr.instituicao
                        select new
                        {
                            instituicao_responsavel_id = instr.instituicao_responsavel_id,
                            instituicao = instr.instituicao,
                            nome = instr.nome,
                            email = instr.email,
                            telefone = instr.telefone,
                            celular = instr.celular,
                            descricao = instr.descricao,
                            cpf_cnpj = instr.cpf_cnpj,
                            criado_por = instr.criado_por,
                            atulizado = instr.atualizado,
                            usuario = instr.usuario,
                            instituicaoN = inst.nome,
                        };

            var Dados = JsonConvert.SerializeObject(query);
            return Ok(Dados);
        }

        /// <summary>
        /// Traz todos Centros Administrativos
        /// </summary>
        /// <returns>Json com as Centros Administrativos</returns>
        [Authorize]
        [Route("CentroAdministrativo")]
        [HttpGet]
        public IHttpActionResult CentroAdministrativo()
        {
            var instituicao = dba.instituicao.ToList();
            var ca = dba.centro_administrativo.ToList();
            var query = from inst in instituicao
                        join cadn in ca on inst.instituicao_id equals cadn.instituicao
                        select new
                        {
                            centro_administrativo_id = cadn.centro_administrativo_id,
                            data = cadn.data,
                            instituicao = cadn.instituicao,
                            nome = cadn.nome,
                            descricao = cadn.descricao,
                            cpf_cnpj = cadn.cpf_cnpj,
                            criado_por = cadn.criado_por,
                            atualizado = cadn.atualizado,
                            usuario = cadn.usuario,
                            instituicaoN = inst.nome
                        };

            var Dados = JsonConvert.SerializeObject(query);
            return Ok(Dados);
        }

        [Authorize]
        [Route("AtualizarUsuario")]
        [HttpPost]
        public async Task<IHttpActionResult> AtualizarUsuario(AspNetUsers userModel)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.AspNetUsers.Find(userModel.Id);
                dado.nomeCompleto = userModel.nomeCompleto;
                dado.UserName = userModel.UserName;
                dado.data = userModel.data;
                dado.criado_por = userModel.criado_por;
                dado.atualizado = userModel.atualizado;
                dado.usuario_status = userModel.usuario_status;
                dado.especial = userModel.especial;
                dado.cargo = userModel.cargo;
                dado.Email = userModel.Email;
                dba.SaveChanges();
                

                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("AtualizarStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> AtualizarStatus(usuario_status _userstatus)
        {
            if (ModelState.IsValid)
            {
                
                var dado = dba.usuario_status.Find(_userstatus.usuario_status_id);
                dado.status = _userstatus.status;
                dado.descricao = _userstatus.descricao;
                dado.atualizado = _userstatus.atualizado;
                dado.usuario = _userstatus.usuario;
                dba.SaveChanges();
                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("AtualizarInstituicao")]
        [HttpPost]
        public async Task<IHttpActionResult> AtualizarInstituicao(instituicao_ _inst)
        {
            if (ModelState.IsValid)
            {

                var dado = dba.instituicao.Find(_inst.instituicao_id);
                dado.nome = _inst.nome;
                dado.descricao = _inst.descricao;
                dado.cnpj = _inst.cnpj;
                dado.instituicao_status = _inst.instituicao_status;
                dado.atualizado = _inst.atualizado;
                dba.SaveChanges();
                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("AtualizarInstituicaoResponsavel")]
        [HttpPost]
        public async Task<IHttpActionResult> AtualizarInstituicaoResponsavel(instituicao_responsavel _instr)
        {
            if (ModelState.IsValid)
            {

                var dado = dba.instituicao_responsavel.Find(_instr.instituicao_responsavel_id);
                dado.instituicao = _instr.instituicao;
                dado.nome = _instr.nome;
                dado.email = _instr.email;
                dado.telefone = _instr.telefone;
                dado.celular = _instr.celular;
                dado.descricao = _instr.descricao;
                dado.cpf_cnpj = _instr.cpf_cnpj;
                dado.atualizado = _instr.atualizado;
                dado.usuario = _instr.usuario;
                dba.SaveChanges();
                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("AtualizarInstStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> AtualizarInstStatus(instituicao_status _userInst)
        {
            if (ModelState.IsValid)
            {

                var dado = dba.instituicao_status.Find(_userInst.instituicao_status_id);
                dado.status = _userInst.status;
                dado.descricao = _userInst.descricao;
                dado.atualizado = _userInst.atualizado;
                dba.SaveChanges();
                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("AtualizarCentroAdministrativo")]
        [HttpPost]
        public async Task<IHttpActionResult> AtualizarCentroAdministrativo(centro_administrativo _ca)
        {
            if (ModelState.IsValid)
            {

                var dado = dba.centro_administrativo.Find(_ca.centro_administrativo_id);
                dado.instituicao = _ca.instituicao;
                dado.nome = _ca.nome;
                dado.descricao = _ca.descricao;
                dado.cpf_cnpj = _ca.cpf_cnpj;
                dado.atualizado = _ca.atualizado;
                dado.usuario = _ca.usuario;
                dba.SaveChanges();
                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("InserirStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> InserirStatus(usuario_status _userstatus)
        {
            if (ModelState.IsValid)
            {
                /*var dados = dba.AspNetUsers.ToList();
                var query = from g in dados
                            where g.UserName == _userstatus.usuario
                            select g.Id.ToList();*/

                usuario_status _user = new usuario_status();
                _user.data = _userstatus.data;
                _user.status = _userstatus.status;
                _user.descricao = _userstatus.descricao;
                _user.usuario = _userstatus.usuario;
                var dado = dba.usuario_status.Add(_userstatus);
                dba.SaveChanges();

                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("InserirInstStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> InserirInstStatus (instituicao_status _userInst)
        {
            if (ModelState.IsValid)
            {
                /*var dados = dba.AspNetUsers.ToList();
                var query = from g in dados
                            where g.UserName == _userstatus.usuario
                            select g.Id.ToList();*/

                instituicao_status _inst = new instituicao_status();
                _inst.data = _userInst.data;
                _inst.status = _userInst.status;
                _inst.descricao = _userInst.descricao;
                _inst.usuario = _userInst.usuario;
                var dado = dba.instituicao_status.Add(_inst);
                dba.SaveChanges();

                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("InserirInstituicao")]
        [HttpPost]
        public async Task<IHttpActionResult> InserirInstituicao(instituicao_ _inst)
        {
            if (ModelState.IsValid)
            {


                instituicao_ _instN = new instituicao_();
                _instN.data = _inst.data;
                _instN.instituicao_status = _inst.instituicao_status;
                _instN.nome = _inst.nome;
                _instN.descricao = _inst.descricao;
                _instN.cnpj = _inst.cnpj;

                var dado = dba.instituicao.Add(_instN);
                dba.SaveChanges();

                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("inseririnstituicaoresponsavel")]
        [HttpPost]
        public async Task<IHttpActionResult> inseririnstituicaoresponsavel(instituicao_responsavel _inst)
        {
            if (ModelState.IsValid)
            {
                instituicao_responsavel _instr = new instituicao_responsavel();
                _instr.data = _inst.data;
                _instr.instituicao = _inst.instituicao;
                _instr.nome = _inst.nome;
                _instr.email = _inst.email;
                _instr.telefone = _inst.telefone;
                _instr.celular = _inst.celular;
                _instr.descricao = _inst.descricao;
                _instr.cpf_cnpj = _inst.cpf_cnpj;
                _instr.atualizado = _inst.atualizado;
                _instr.usuario = _inst.usuario;

                var dado = dba.instituicao_responsavel.Add(_instr);
                dba.SaveChanges();

                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("InserirCentroAdministrativo")]
        [HttpPost]
        public async Task<IHttpActionResult> InserirCentroAdministrativo(centro_administrativo _ca)
        {
            if (ModelState.IsValid)
            {
                centro_administrativo _car = new centro_administrativo();
                _car.data = _ca.data;
                _car.instituicao = _ca.instituicao;
                _car.nome = _ca.nome;
                _car.descricao = _ca.descricao;
                _car.cpf_cnpj = _ca.cpf_cnpj;
                _car.atualizado = _ca.atualizado;
                _car.usuario = _ca.usuario;

                var dado = dba.centro_administrativo.Add(_car);
                dba.SaveChanges();

                return Ok("Dados Gravados");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("ApagaUsuario")]
        [HttpPost]
        public async Task<IHttpActionResult> ApagaUsuario(AspNetUsers userModel)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.AspNetUsers.Find(userModel.Id);

                var dados = dba.AspNetUsers.Remove(dado);
                dba.SaveChanges();
                return Ok("Dados Excluidos");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("ApagaInstituicao")]
        [HttpPost]
        public async Task<IHttpActionResult> ApagaInstituicao(instituicao_ inst)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.instituicao.Find(inst.instituicao_id);
                var dados = dba.instituicao.Remove(dado);
                dba.SaveChanges();
                return Ok("Dados Excluidos");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("ApagaCentroAdministrativo")]
        [HttpPost]
        public async Task<IHttpActionResult> ApagaCentroAdministrativoi(centro_administrativo _ca)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.centro_administrativo.Find(_ca.centro_administrativo_id);
                var dados = dba.centro_administrativo.Remove(dado);
                dba.SaveChanges();
                return Ok("Dados Excluidos");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("ApagaStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> ApagaStatus(usuario_status _userstatus)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.usuario_status.Find(_userstatus.usuario_status_id);

                var dados = dba.usuario_status.Remove(dado);
                dba.SaveChanges();
                return Ok("Dados Excluidos");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("ApagaInstStatus")]
        [HttpPost]
        public async Task<IHttpActionResult> ApagaInstStatus (instituicao_status _inst)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.instituicao_status.Find(_inst.instituicao_status_id);

                var dados = dba.instituicao_status.Remove(dado);
                dba.SaveChanges();
                return Ok("Dados Excluidos");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [Authorize]
        [Route("ApagaInstituicaoResponsavel")]
        [HttpPost]
        public async Task<IHttpActionResult> ApagaInstituicaoResponsavel(instituicao_responsavel _inst)
        {
            if (ModelState.IsValid)
            {
                var dado = dba.instituicao_responsavel.Find(_inst.instituicao_responsavel_id);

                var dados = dba.instituicao_responsavel.Remove(dado);
                dba.SaveChanges();
                return Ok("Dados Excluidos");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

    }
}