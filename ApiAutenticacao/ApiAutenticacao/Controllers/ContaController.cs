﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ApiAutenticacao.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using System.Web;

namespace ApiAutenticacao.Controllers
{
    public class ContaController : ApiController
    {

        private UserManager<UserModel> _userManager;
        public UserManager<UserModel> UserManager
        {
            get
            {
                if(_userManager == null)
                {
                    var contextOwin = HttpContext.Current.GetOwinContext();
                    _userManager = contextOwin.GetUserManager<UserManager<UserModel>>();
                }
                return _userManager;
            }
            set { _userManager = value; }
        }


        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}