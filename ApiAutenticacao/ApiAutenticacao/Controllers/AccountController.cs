﻿using ApiAutenticacao.API;
using ApiAutenticacao.API.Providers;
using ApiAutenticacao.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Owin.Security.DataProtection;
using Microsoft.Owin.Security;

namespace ApiAutenticacao.Controllers
{
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private AuthRepository _repo = null;
        private AuthContext _authContext;

        private UserManager<UserModel> _userManager;

        public UserManager<UserModel> UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    var contextOwin = HttpContext.Current.GetOwinContext();
                    _userManager = contextOwin.GetUserManager<UserManager<UserModel>>();
                }
                return _userManager;
            }
            set { _userManager = value; }
        }

        private SignInManager<UserModel, string> _signInManager;
        public SignInManager<UserModel, string> SignInManager
        {
            get
            {
                if (_signInManager == null)
                {
                    var contextOwin = HttpContext.Current.GetOwinContext();
                    _signInManager = contextOwin.GetUserManager<SignInManager<UserModel, string>>();
                }
                return _signInManager;
            }
            set
            {
                _signInManager = value;
            }
        }

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                var contextoOwin = Request.GetOwinContext();
                return contextoOwin.Authentication;
            }
        }

        public AccountController()
        {
            _repo = new AuthRepository();
            
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Registrar")]
        public async Task<IHttpActionResult> Register(UserModel userModel)
        {
            if (ModelState.IsValid)
            {
                var novoUsuario = new UserModel();
                novoUsuario.Email = userModel.Email;
                novoUsuario.UserName = userModel.UserName;

                
                
                


                var usuario = await _repo.FindMail(novoUsuario.Email);
                var usuarioJaExite = usuario != null;

                if (usuarioJaExite)
                {
                    return BadRequest("Email já Existe");
                }
                else
                {
                    IdentityResult result = await _repo.RegisterUser(userModel);

                    IHttpActionResult errorResult = GetErrorResult(result);

                    if (errorResult != null)
                    {
                        return errorResult;
                    }

                    return Ok();
                }
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [AllowAnonymous]
        [Route("EsqueciSenha")]
        [HttpPost]
        public async Task<IHttpActionResult> EsqueciSenha(ContaEsqueciSenha modelo)
        {
            if (ModelState.IsValid)
            {


                var usuario = await _repo.FindMail(modelo.Email);
                if (usuario != null)
                {
                    string token = await _repo.GerarToken(usuario.Email);
                    return Ok("Email enviado com Token");
                }

                return BadRequest("Email não encontrado");
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [AllowAnonymous]
        [Route("AlteraConta")]
        [HttpPost]
        public async Task<IHttpActionResult> AlteraConta(ConfirmacaoAlteracaoSenha modelo)
        {
            if (ModelState.IsValid)
            {
                var novoUsuario = new ConfirmacaoAlteracaoSenha();
                novoUsuario.Email = modelo.Email;  
                var usuario = await _repo.FindMail(novoUsuario.Email);
                novoUsuario.UsuarioId = usuario.Id;
                novoUsuario.Token = modelo.Token;
                novoUsuario.NovaSenha = modelo.NovaSenha;

                string result = await _repo.AlterarSenha(novoUsuario.UsuarioId, novoUsuario.Token, novoUsuario.NovaSenha);

                if(result == "OK")
                    return Ok("Senha Alterada");
                else
                    return BadRequest(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        
        /*[AllowAnonymous]
        [Route("Funcionarios")]
        [HttpGet]
        public async Task<IHttpActionResult> Funcionarios()
        {
            if (ModelState.IsValid)
            {
                var result = await _repo.ListaUser();
                return Ok(result);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }*/


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _repo.Dispose();
            }

            base.Dispose(disposing);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

    }
}
