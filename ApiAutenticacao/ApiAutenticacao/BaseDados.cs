﻿using ApiAutenticacao.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ApiAutenticacao.Models
{
    public class BaseDadosDBContext : DbContext

    {
        public BaseDadosDBContext() : base("name=AuthContext")
        {
           
        }
        public virtual DbSet<instituicao_> instituicao { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<usuario_status> usuario_status { get; set; }
        public virtual DbSet<instituicao_status> instituicao_status { get; set; }
        public virtual DbSet<instituicao_responsavel> instituicao_responsavel { get; set; }
        public virtual DbSet<centro_administrativo> centro_administrativo { get; set; }

    }
}