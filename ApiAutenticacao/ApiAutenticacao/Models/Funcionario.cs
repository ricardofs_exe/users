﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiAutenticacao.Models
{
    public class Funcionario
    {
        public string Email { get; set; }
        public string UserName { get; set;}
        public DateTime data { get; set; }
    }
}