﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiAutenticacao.Models
{
    public class instituicao_responsavel
    {
        [Key]
        public int instituicao_responsavel_id { get; set; }

        public DateTime? data { get; set; }

        public int instituicao { get; set; }

        public string nome { get; set; }

        public string email { get; set; }

        public string telefone { get; set; }

        public string celular { get; set; }

        public string descricao { get; set; }

        public string cpf_cnpj { get; set; }

        public DateTime? atualizado { get; set; }

        public string usuario { get; set; }

        public string criado_por { get; set; }
    }
}