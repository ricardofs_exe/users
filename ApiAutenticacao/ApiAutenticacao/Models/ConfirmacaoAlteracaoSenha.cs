﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ApiAutenticacao.Models
{
    public class ConfirmacaoAlteracaoSenha
    {
        public string Email { get; set; }
        public string UsuarioId { get; set;}
        public string Token { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Nova senha")]
        public string NovaSenha { get; set; }
    }
}