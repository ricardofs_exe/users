﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ApiAutenticacao.Models
{
    public class instituicao_
    {
        public int instituicao_id { get; set; }

        public DateTime? data { get; set; }

        public int instituicao_status { get; set; }

        public string nome { get; set; }

        public string descricao { get; set; }

        public string cnpj { get; set; }

        public DateTime? atualizado { get; set; }

        public string usuario { get; set; }

        public string criado_por { get; set; }

    }
}