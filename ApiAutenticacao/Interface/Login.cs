﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using DevExpress.Skins.XtraForm;
using DevExpress.Utils.Drawing;
using DevExpress.Utils;
using DevExpress.LookAndFeel;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Net.Http;


namespace Interface
{
    public partial class Login : DevExpress.XtraEditors.XtraForm
    {

        public Login()
        {
            InitializeComponent();
            
            this.BackColor = Color.White;
            this.AcceptButton = btnLogar;


        }


        protected override FormPainter CreateFormBorderPainter()
        {
            return new MyFormPainter(this, DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveLookAndFeel);
        }

        public class MyFormPainter : FormPainter
        {
            private HorzAlignment CaptionAlignment;

            public MyFormPainter(Control owner, ISkinProvider provider) : base(owner, provider) { }

            protected override void DrawBackground(GraphicsCache cache)
            {
                cache.FillRectangle(Color.FromArgb(65, 105, 225), CaptionBounds);
            }

            protected override void DrawText(DevExpress.Utils.Drawing.GraphicsCache cache)
            {
                string text = Text;
                if (text == null || text.Length == 0 || TextBounds.IsEmpty)
                {
                    return;
                }
                AppearanceObject appearance = new AppearanceObject(GetDefaultAppearance());
                appearance.TextOptions.Trimming = Trimming.EllipsisCharacter;
                appearance.TextOptions.HAlignment = CaptionAlignment;
                if (AllowHtmlDraw)
                {
                    DrawHtmlText(cache, appearance);
                    return;
                }
                Rectangle r = RectangleHelper.GetCenterBounds(TextBounds, new Size(TextBounds.Width, CalcTextHeight(cache, appearance)));
                DrawTextShadow(cache, appearance, r);
                Color foreColor = Color.FromArgb(255, 250, 250);
                Brush textBrush = new SolidBrush(foreColor);
                cache.DrawString(text, appearance.Font, textBrush, r, appearance.GetStringFormat());
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            var acesso = new Acesso();
            string login = acesso.MostraAcesso();
            txtLogin.Text = login.ToString();
        }

        private void simpleButton3_Click(object sender, EventArgs e)
        {
            Cadastro frm = new Cadastro();
            frm.Show();
        }

        private void btnEsqueci_Click(object sender, EventArgs e)
        {
            Esqueci frm = new Esqueci();
            frm.Show();
        }

        private void btnLogar_Click(object sender, EventArgs e)
        {
            try
            {
                var acesso = new Acesso();
                string login = acesso.MostraAcesso();
                acesso.Login = txtLogin.Text;

                if (login == "")
                {
                    Acesso.AdicionarAcesso(acesso);
                }
                else
                {
                    if (login != txtLogin.Text)
                    {
                        Acesso.ExcluirAcesso(login);
                        Acesso.AdicionarAcesso(acesso);
                    }

                }

                Logar(txtLogin.Text, txtSenha.Text);

            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }

        private async void Logar(string login, string senha)
        {
            try
            {

                splashScreenManager1.ShowWaitForm();

                Acesso ac = new Acesso();
                ac.Login = login.ToString();

                if (ac.Login != login)
                {
                    Acesso.AdicionarAcesso(ac);
                }


                if (ValidacaoLogin(login, senha))
                {
                    Usuario _user = new Usuario();
                    _user.UserName = login.ToString();
                    _user.Password = senha.ToString();

                    string url = "http://localhost:58260/token";

                    var request = (HttpWebRequest)WebRequest.Create(url);

                    var postData = "username=" + Uri.EscapeDataString(_user.UserName.ToString());
                    postData += "&password=" + Uri.EscapeDataString(_user.Password.ToString());
                    postData += "&grant_type=" + Uri.EscapeDataString("password");
                    var data = Encoding.ASCII.GetBytes(postData);

                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.ContentLength = data.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(data, 0, data.Length);
                    }

                    var response = (HttpWebResponse)request.GetResponse();

                    var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                    dynamic dobj = JsonConvert.DeserializeObject<dynamic>(responseString);

                    string token = dobj["access_token"];


                    

                    string dados = await retornoJsonAsync(token, url);

                    

                    if (dados == "ok")
                    {
                        
                        Principal frm = new Principal(token, login);
                        frm.Show();
                        this.Visible = false;
                        //this.Close();
                    }
                    else
                    {
                        /*XtraMessageBox.Show(dados, "Confirmation");*/
                    }

                    splashScreenManager1.CloseWaitForm();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }



        public bool ValidacaoLogin(string login, string senha)
        {
            bool validacao = true;
            string msg = "";

            try
            {
                if (login == null || login.ToString() == "")
                {
                    validacao = false;
                    msg = "Campo Login não foi preenchido";
                    XtraMessageBox.Show(msg, "Confirmation");
                    txtLogin.Focus();
                    return validacao;

                }

                if (senha == null || senha.ToString() == "")
                {
                    validacao = false;
                    msg = "Campo Senha não foi preenchido";
                    XtraMessageBox.Show(msg, "Confirmation");
                    txtSenha.Focus();
                    return validacao;
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }

            return validacao;
        }

        private async Task<string> retornoJsonAsync(string token, string baseUrl)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/acesso");

                client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);
                
                HttpResponseMessage response = await client.GetAsync("/api/orders/acesso");
                
                if (response.IsSuccessStatusCode)
                {
                    
                    return "ok";
                }
                else
                {
                    return "erro";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        

        public void Limpar()
        {
            try
            {
                txtLogin.Text = "";
                txtSenha.Text = "";
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }

        private void txtSenha_EditValueChanged(object sender, EventArgs e)
        {

        }
    }
}