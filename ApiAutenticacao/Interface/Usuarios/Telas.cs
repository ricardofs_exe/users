﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using DevExpress.Skins.XtraForm;
using DevExpress.Utils.Drawing;
using DevExpress.Utils;
using DevExpress.LookAndFeel;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json;

namespace Interface
{
    public partial class Telas : DevExpress.XtraEditors.XtraForm
    {
        public Telas()
        {
            InitializeComponent();
            this.BackColor = Color.White;
        }

        protected override FormPainter CreateFormBorderPainter()
        {
            return new MyFormPainter(this, DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveLookAndFeel);
        }

        public class MyFormPainter : FormPainter
        {
            private HorzAlignment CaptionAlignment;

            public MyFormPainter(Control owner, ISkinProvider provider) : base(owner, provider) { }

            protected override void DrawBackground(GraphicsCache cache)
            {
                cache.FillRectangle(Color.FromArgb(65, 105, 225), CaptionBounds);
            }

            protected override void DrawText(DevExpress.Utils.Drawing.GraphicsCache cache)
            {
                string text = Text;
                if (text == null || text.Length == 0 || TextBounds.IsEmpty)
                {
                    return;
                }
                AppearanceObject appearance = new AppearanceObject(GetDefaultAppearance());
                appearance.TextOptions.Trimming = Trimming.EllipsisCharacter;
                appearance.TextOptions.HAlignment = CaptionAlignment;
                if (AllowHtmlDraw)
                {
                    DrawHtmlText(cache, appearance);
                    return;
                }
                Rectangle r = RectangleHelper.GetCenterBounds(TextBounds, new Size(TextBounds.Width, CalcTextHeight(cache, appearance)));
                DrawTextShadow(cache, appearance, r);
                Color foreColor = Color.FromArgb(255, 250, 250);
                Brush textBrush = new SolidBrush(foreColor);
                cache.DrawString(text, appearance.Font, textBrush, r, appearance.GetStringFormat());
            }
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            
        }

        public async void Inserir(string login, string senha, string ConfSenha, string Mail, string ConfMail)
        {
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            //Limpar();
        }

        

        private void Cadastro_Load(object sender, EventArgs e)
        {

        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void groupControl1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}