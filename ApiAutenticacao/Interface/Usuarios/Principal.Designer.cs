﻿namespace Interface
{
    partial class Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Principal));
            this.ribbon = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.barEditItem2 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemImageEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemImageEdit();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.btnCadastro = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem4 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem5 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem6 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem7 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem3 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem8 = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroup3 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonStatusBar = new DevExpress.XtraBars.Ribbon.RibbonStatusBar();
            this.ribbonPageGroup2 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.btnListaUsuarios = new DevExpress.XtraEditors.SimpleButton();
            this.btnTelas = new DevExpress.XtraEditors.SimpleButton();
            this.btnInstituicao = new DevExpress.XtraEditors.SimpleButton();
            this.btnStatus = new DevExpress.XtraEditors.SimpleButton();
            this.btnInstStatus = new DevExpress.XtraEditors.SimpleButton();
            this.btnInstituicaoResponsavel = new DevExpress.XtraEditors.SimpleButton();
            this.btnCentroAdministrativo = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbon
            // 
            this.ribbon.ExpandCollapseItem.Id = 0;
            this.ribbon.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbon.ExpandCollapseItem,
            this.ribbon.SearchEditItem,
            this.barEditItem1,
            this.barEditItem2,
            this.barButtonItem1,
            this.barButtonItem2,
            this.btnCadastro,
            this.barButtonItem4,
            this.barButtonItem5,
            this.barButtonItem6,
            this.barButtonItem7,
            this.barButtonItem3,
            this.barButtonItem8});
            this.ribbon.Location = new System.Drawing.Point(0, 0);
            this.ribbon.MaxItemId = 13;
            this.ribbon.Name = "ribbon";
            this.ribbon.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbon.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemImageEdit1,
            this.repositoryItemImageEdit2});
            this.ribbon.ShowExpandCollapseButton = DevExpress.Utils.DefaultBoolean.True;
            this.ribbon.Size = new System.Drawing.Size(523, 161);
            this.ribbon.StatusBar = this.ribbonStatusBar;
            this.ribbon.Click += new System.EventHandler(this.ribbon_Click);
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemImageEdit1;
            this.barEditItem1.Id = 2;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemImageEdit1
            // 
            this.repositoryItemImageEdit1.AutoHeight = false;
            this.repositoryItemImageEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit1.Name = "repositoryItemImageEdit1";
            // 
            // barEditItem2
            // 
            this.barEditItem2.Edit = this.repositoryItemImageEdit2;
            this.barEditItem2.EditWidth = 1000;
            this.barEditItem2.Id = 3;
            this.barEditItem2.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barEditItem2.ImageOptions.Image")));
            this.barEditItem2.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barEditItem2.ImageOptions.LargeImage")));
            this.barEditItem2.Name = "barEditItem2";
            // 
            // repositoryItemImageEdit2
            // 
            this.repositoryItemImageEdit2.AutoHeight = false;
            this.repositoryItemImageEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemImageEdit2.Name = "repositoryItemImageEdit2";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Cadastro de Usuarios";
            this.barButtonItem1.Id = 4;
            this.barButtonItem1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.Image")));
            this.barButtonItem1.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem1.ImageOptions.LargeImage")));
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "barButtonItem2";
            this.barButtonItem2.Id = 5;
            this.barButtonItem2.Name = "barButtonItem2";
            // 
            // btnCadastro
            // 
            this.btnCadastro.Caption = "Cadastro";
            this.btnCadastro.Id = 6;
            this.btnCadastro.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadastro.ImageOptions.Image")));
            this.btnCadastro.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("btnCadastro.ImageOptions.LargeImage")));
            this.btnCadastro.Name = "btnCadastro";
            this.btnCadastro.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnCadastro_ItemClick);
            // 
            // barButtonItem4
            // 
            this.barButtonItem4.Caption = "barButtonItem4";
            this.barButtonItem4.Id = 7;
            this.barButtonItem4.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.Image")));
            this.barButtonItem4.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem4.ImageOptions.LargeImage")));
            this.barButtonItem4.Name = "barButtonItem4";
            // 
            // barButtonItem5
            // 
            this.barButtonItem5.Caption = "Permições";
            this.barButtonItem5.Id = 8;
            this.barButtonItem5.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.Image")));
            this.barButtonItem5.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem5.ImageOptions.LargeImage")));
            this.barButtonItem5.Name = "barButtonItem5";
            // 
            // barButtonItem6
            // 
            this.barButtonItem6.Caption = "Permissões";
            this.barButtonItem6.Id = 9;
            this.barButtonItem6.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.Image")));
            this.barButtonItem6.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem6.ImageOptions.LargeImage")));
            this.barButtonItem6.Name = "barButtonItem6";
            // 
            // barButtonItem7
            // 
            this.barButtonItem7.Caption = "barButtonItem7";
            this.barButtonItem7.Id = 10;
            this.barButtonItem7.Name = "barButtonItem7";
            // 
            // barButtonItem3
            // 
            this.barButtonItem3.Caption = "Permissão";
            this.barButtonItem3.Id = 11;
            this.barButtonItem3.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.Image")));
            this.barButtonItem3.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem3.ImageOptions.LargeImage")));
            this.barButtonItem3.Name = "barButtonItem3";
            // 
            // barButtonItem8
            // 
            this.barButtonItem8.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barButtonItem8.Caption = "Sair";
            this.barButtonItem8.Id = 12;
            this.barButtonItem8.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.Image")));
            this.barButtonItem8.ImageOptions.LargeImage = ((System.Drawing.Image)(resources.GetObject("barButtonItem8.ImageOptions.LargeImage")));
            this.barButtonItem8.Name = "barButtonItem8";
            this.barButtonItem8.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem8_ItemClick);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1,
            this.ribbonPageGroup3});
            this.ribbonPage1.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ribbonPage1.ImageOptions.Image")));
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "Usuários";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.ItemLinks.Add(this.btnCadastro);
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            // 
            // ribbonPageGroup3
            // 
            this.ribbonPageGroup3.ItemLinks.Add(this.barButtonItem3);
            this.ribbonPageGroup3.Name = "ribbonPageGroup3";
            // 
            // ribbonStatusBar
            // 
            this.ribbonStatusBar.ItemLinks.Add(this.barButtonItem8);
            this.ribbonStatusBar.Location = new System.Drawing.Point(0, 625);
            this.ribbonStatusBar.Name = "ribbonStatusBar";
            this.ribbonStatusBar.Ribbon = this.ribbon;
            this.ribbonStatusBar.Size = new System.Drawing.Size(523, 24);
            // 
            // ribbonPageGroup2
            // 
            this.ribbonPageGroup2.ItemLinks.Add(this.barButtonItem5);
            this.ribbonPageGroup2.Name = "ribbonPageGroup2";
            this.ribbonPageGroup2.Text = "ribbonPageGroup2";
            // 
            // btnListaUsuarios
            // 
            this.btnListaUsuarios.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnListaUsuarios.ImageOptions.Image")));
            this.btnListaUsuarios.Location = new System.Drawing.Point(12, 177);
            this.btnListaUsuarios.Name = "btnListaUsuarios";
            this.btnListaUsuarios.Size = new System.Drawing.Size(162, 45);
            this.btnListaUsuarios.TabIndex = 2;
            this.btnListaUsuarios.Text = "Usuários";
            this.btnListaUsuarios.Click += new System.EventHandler(this.btnListaUsuarios_Click);
            // 
            // btnTelas
            // 
            this.btnTelas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTelas.ImageOptions.Image")));
            this.btnTelas.Location = new System.Drawing.Point(350, 177);
            this.btnTelas.Name = "btnTelas";
            this.btnTelas.Size = new System.Drawing.Size(162, 45);
            this.btnTelas.TabIndex = 3;
            this.btnTelas.Text = "Telas";
            this.btnTelas.Click += new System.EventHandler(this.btnTelas_Click);
            // 
            // btnInstituicao
            // 
            this.btnInstituicao.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInstituicao.ImageOptions.Image")));
            this.btnInstituicao.Location = new System.Drawing.Point(180, 177);
            this.btnInstituicao.Name = "btnInstituicao";
            this.btnInstituicao.Size = new System.Drawing.Size(163, 45);
            this.btnInstituicao.TabIndex = 4;
            this.btnInstituicao.Text = "Instituição";
            this.btnInstituicao.Click += new System.EventHandler(this.btnInstituicao_Click);
            // 
            // btnStatus
            // 
            this.btnStatus.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnStatus.ImageOptions.Image")));
            this.btnStatus.Location = new System.Drawing.Point(12, 228);
            this.btnStatus.Name = "btnStatus";
            this.btnStatus.Size = new System.Drawing.Size(162, 45);
            this.btnStatus.TabIndex = 8;
            this.btnStatus.Text = "Usuário Status";
            this.btnStatus.Click += new System.EventHandler(this.btnStatus_Click);
            // 
            // btnInstStatus
            // 
            this.btnInstStatus.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInstStatus.ImageOptions.Image")));
            this.btnInstStatus.Location = new System.Drawing.Point(180, 228);
            this.btnInstStatus.Name = "btnInstStatus";
            this.btnInstStatus.Size = new System.Drawing.Size(162, 45);
            this.btnInstStatus.TabIndex = 11;
            this.btnInstStatus.Text = "Instituição Status";
            this.btnInstStatus.Click += new System.EventHandler(this.btnInstStatus_Click);
            // 
            // btnInstituicaoResponsavel
            // 
            this.btnInstituicaoResponsavel.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInstituicaoResponsavel.ImageOptions.Image")));
            this.btnInstituicaoResponsavel.Location = new System.Drawing.Point(12, 279);
            this.btnInstituicaoResponsavel.Name = "btnInstituicaoResponsavel";
            this.btnInstituicaoResponsavel.Size = new System.Drawing.Size(162, 45);
            this.btnInstituicaoResponsavel.TabIndex = 14;
            this.btnInstituicaoResponsavel.Text = "Instituição Responsável";
            this.btnInstituicaoResponsavel.Click += new System.EventHandler(this.btnInstituicaoResponsavel_Click);
            // 
            // btnCentroAdministrativo
            // 
            this.btnCentroAdministrativo.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCentroAdministrativo.ImageOptions.Image")));
            this.btnCentroAdministrativo.Location = new System.Drawing.Point(350, 228);
            this.btnCentroAdministrativo.Name = "btnCentroAdministrativo";
            this.btnCentroAdministrativo.Size = new System.Drawing.Size(162, 45);
            this.btnCentroAdministrativo.TabIndex = 15;
            this.btnCentroAdministrativo.Text = "Centro Administrativo";
            this.btnCentroAdministrativo.Click += new System.EventHandler(this.btnCentroAdministrativo_Click);
            // 
            // Principal
            // 
            this.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.Appearance.Options.UseBackColor = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(523, 649);
            this.Controls.Add(this.btnCentroAdministrativo);
            this.Controls.Add(this.btnInstituicaoResponsavel);
            this.Controls.Add(this.btnInstStatus);
            this.Controls.Add(this.btnStatus);
            this.Controls.Add(this.btnInstituicao);
            this.Controls.Add(this.btnTelas);
            this.Controls.Add(this.ribbonStatusBar);
            this.Controls.Add(this.btnListaUsuarios);
            this.Controls.Add(this.ribbon);
            this.Name = "Principal";
            this.Ribbon = this.ribbon;
            this.StatusBar = this.ribbonStatusBar;
            this.Text = "Vegas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.ribbon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemImageEdit2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonControl ribbon;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonStatusBar ribbonStatusBar;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit1;
        private DevExpress.XtraBars.BarEditItem barEditItem2;
        private DevExpress.XtraEditors.Repository.RepositoryItemImageEdit repositoryItemImageEdit2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.BarButtonItem btnCadastro;
        private DevExpress.XtraBars.BarButtonItem barButtonItem4;
        private DevExpress.XtraBars.BarButtonItem barButtonItem5;
        private DevExpress.XtraBars.BarButtonItem barButtonItem6;
        private DevExpress.XtraBars.BarButtonItem barButtonItem7;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup2;
        private DevExpress.XtraBars.BarButtonItem barButtonItem3;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup3;
        private DevExpress.XtraBars.BarButtonItem barButtonItem8;
        private DevExpress.XtraEditors.SimpleButton btnListaUsuarios;
        private DevExpress.XtraEditors.SimpleButton btnTelas;
        private DevExpress.XtraEditors.SimpleButton btnInstituicao;
        private DevExpress.XtraEditors.SimpleButton btnStatus;
        private DevExpress.XtraEditors.SimpleButton btnInstStatus;
        private DevExpress.XtraEditors.SimpleButton btnInstituicaoResponsavel;
        private DevExpress.XtraEditors.SimpleButton btnCentroAdministrativo;
    }
}