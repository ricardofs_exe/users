﻿namespace Interface
{
    partial class ListaUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListaUsuarios));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gcLista = new DevExpress.XtraEditors.GroupControl();
            this.aspNetUsersBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.usuariosDataSet = new Interface.UsuariosDataSet();
            this.pcRodape = new DevExpress.XtraEditors.PanelControl();
            this.gcMenu = new DevExpress.XtraEditors.GroupControl();
            this.aspNetUsersTableAdapter = new Interface.UsuariosDataSetTableAdapters.AspNetUsersTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspNetUsersBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.usuariosDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcRodape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // gcLista
            // 
            this.gcLista.Location = new System.Drawing.Point(105, 54);
            this.gcLista.Name = "gcLista";
            this.gcLista.Size = new System.Drawing.Size(976, 425);
            this.gcLista.TabIndex = 22;
            this.gcLista.Text = "Lista de Usuários";
            this.gcLista.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // aspNetUsersBindingSource
            // 
            this.aspNetUsersBindingSource.DataMember = "AspNetUsers";
            this.aspNetUsersBindingSource.DataSource = this.usuariosDataSet;
            // 
            // usuariosDataSet
            // 
            this.usuariosDataSet.DataSetName = "UsuariosDataSet";
            this.usuariosDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // pcRodape
            // 
            this.pcRodape.AutoSize = true;
            this.pcRodape.Location = new System.Drawing.Point(0, 485);
            this.pcRodape.Name = "pcRodape";
            this.pcRodape.Size = new System.Drawing.Size(1094, 37);
            this.pcRodape.TabIndex = 23;
            // 
            // gcMenu
            // 
            this.gcMenu.Location = new System.Drawing.Point(5, 54);
            this.gcMenu.Name = "gcMenu";
            this.gcMenu.Size = new System.Drawing.Size(94, 425);
            this.gcMenu.TabIndex = 24;
            this.gcMenu.Text = "Menu";
            // 
            // aspNetUsersTableAdapter
            // 
            this.aspNetUsersTableAdapter.ClearBeforeFill = true;
            // 
            // ListaUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1093, 523);
            this.Controls.Add(this.gcMenu);
            this.Controls.Add(this.pcRodape);
            this.Controls.Add(this.gcLista);
            this.Controls.Add(this.pictureBox1);
            this.IconOptions.ColorizeInactiveIcon = DevExpress.Utils.DefaultBoolean.True;
            this.IconOptions.ShowIcon = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ListaUsuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LISTA DE USUÁRIOS";
            this.Load += new System.EventHandler(this.Cadastro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aspNetUsersBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.usuariosDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcRodape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.GroupControl gcLista;
        private DevExpress.XtraEditors.PanelControl pcRodape;
        private DevExpress.XtraEditors.GroupControl gcMenu;
        private UsuariosDataSet usuariosDataSet;
        private System.Windows.Forms.BindingSource aspNetUsersBindingSource;
        private UsuariosDataSetTableAdapters.AspNetUsersTableAdapter aspNetUsersTableAdapter;
    }
}