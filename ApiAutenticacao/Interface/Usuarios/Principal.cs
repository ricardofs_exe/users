﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace Interface
{
    public partial class Principal : DevExpress.XtraBars.Ribbon.RibbonForm
    {

        private string Rtoken;
        private string Rusuario;

        public Principal(string token, string usuario)
        {

            InitializeComponent();
            Rtoken = token;
            Rusuario = usuario;
        }

        

        private void btnCadastro_ItemClick(object sender, ItemClickEventArgs e)
        {
            Cadastro frm = new Cadastro();
            frm.Show();
        }

        private void barButtonItem8_ItemClick(object sender, ItemClickEventArgs e)
        {
            this.Close();
        }

        private void ribbon_Click(object sender, EventArgs e)
        {

        }

        private void btnListaUsuarios_Click(object sender, EventArgs e)
        {
            var frmmenu = Application.OpenForms["frmUsuarios"];
            if(frmmenu == null)
            {
                frmUsuarios frm = new frmUsuarios(Rtoken, "Usuarios", Rusuario);
                frm.Text = "Controle de Usuários";
                frm.Show();
            }
            else if(frmmenu.Text != "Controle de Usuários")
            {
                frmUsuarios frm = new frmUsuarios(Rtoken, "Usuarios", Rusuario);
                frm.Text = "Controle de Usuários";
                frm.Show();
            }
        }

        private void btnTelas_Click(object sender, EventArgs e)
        {

            Padrao frm = new Padrao(Rtoken,"Telas", Rusuario);
            frm.Text = "Telas";
            frm.Show();
        }

        private void btnInstituicao_Click(object sender, EventArgs e)
        {
            var frmmenu = Application.OpenForms["frmInstituicao"];
            if (frmmenu == null)
            {
                frmInstituicao frm = new frmInstituicao(Rtoken, "Instituicao", Rusuario);
                frm.Text = "Instituição";
                frm.Show();
            }
            else if (frmmenu.Text != "Instituicao")
            {
                frmUsuarioStatus frm = new frmUsuarioStatus(Rtoken, "Instituicao", Rusuario);
                frm.Text = "Instituição";
                frm.Show();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            frmAguarde frm = new frmAguarde();
            frm.Show();
        }

        private void btnStatus_Click(object sender, EventArgs e)
        {
            var frmmenu = Application.OpenForms["frmUsuarioStatus"];
            if (frmmenu == null)
            {
                frmUsuarioStatus frm = new frmUsuarioStatus(Rtoken, "Usuario Status", Rusuario);
                frm.Text = "Usuário Status";
                frm.Show();
            }
            else if (frmmenu.Text != "Usuário Status")
            {
                frmUsuarioStatus frm = new frmUsuarioStatus(Rtoken, "Usuario Status", Rusuario);
                frm.Text = "Usuário Status";
                frm.Show();
            }
        }

        private void btnInstStatus_Click(object sender, EventArgs e)
        {
            var frmmenu = Application.OpenForms["frmInstituicaoStatus"];
            if (frmmenu == null)
            {
                frmInstituicaoStatus frm = new frmInstituicaoStatus(Rtoken, "Instituição Status", Rusuario);
                frm.Text = "Instituição Status";
                frm.Show();
            }
            else if (frmmenu.Text != "Instituição Status")
            {
                frmInstituicaoStatus frm = new frmInstituicaoStatus(Rtoken, "Instituição Status", Rusuario);
                frm.Text = "Instituição Status";
                frm.Show();
            }
        }

        private void btnInstituicaoResponsavel_Click(object sender, EventArgs e)
        {
            var frmmenu = Application.OpenForms["frmInstituicaoResponsavel"];
            if (frmmenu == null)
            {
                frmInstituicaoResponsavel frm = new frmInstituicaoResponsavel(Rtoken, "Instituição Responsável", Rusuario);
                frm.Text = "Instituição Responsável";
                frm.Show();
            }
            else if (frmmenu.Text != "Instituição Responsável")
            {
                frmInstituicaoResponsavel frm = new frmInstituicaoResponsavel(Rtoken, "Instituição Responsável", Rusuario);
                frm.Text = "Instituição Responsável";
                frm.Show();
            }
        }

        private void btnCentroAdministrativo_Click(object sender, EventArgs e)
        {
            Padrao frm = new Padrao(Rtoken, "Centro Administrativo", Rusuario);
            frm.Text = "Centro Administrativo";
            frm.Show();
        }
    }
}