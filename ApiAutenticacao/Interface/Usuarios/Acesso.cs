﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Interface
{
    class Acesso
    {
        #region Atributos
        private string login;
        #endregion

        #region Propiedades
        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        
        #endregion

        #region Métodos

        public static void AdicionarAcesso(Acesso a)
        {
            XElement x = new XElement("Autentications");
            x.Add(new XAttribute("login", a.login.ToString()));
            XElement xml = XElement.Load("Acesso.xml");
            xml.Add(x);
            xml.Save("Acesso.xml");
        }

        public static void ExcluirAcesso(string login)
        {
            XElement xml = XElement.Load("Acesso.xml");
            XElement x = xml.Elements().Where(p => p.Attribute("login").Value.Equals(login.ToString())).First();
            if (x != null)
            {
                x.Remove();
            }
            xml.Save("Acesso.xml");
        }

        public string MostraAcesso()
        {
            //string Arquivo = @"C:\Users\User\Desktop\Vegas\ApiAutenticacao\ApiAutenticacao\Interface\bin\Debug\Acesso.xml";
            string Caminho_completo = Path.GetFullPath("Acesso.xml");
            string texto = "";
            XmlTextReader reader = new XmlTextReader(Caminho_completo);
            ArrayList elementos = new ArrayList();
            string tipo = "";

            while ((reader.Read()))
            {
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        //Se existirem atributos
                        if (reader.HasAttributes)
                        {
                            while (reader.MoveToNextAttribute())
                            {
                                //Pega o valor do atributo.
                                elementos.Add(reader.Value);
                                tipo = reader.Value;
                            }
                        }
                        break;
                    case XmlNodeType.Text:
                            elementos.Add(reader.Value);
                        break;
                }
            }
            foreach (var num in elementos)
            {
                //lstXML.Items.Add(num);
                texto = num.ToString();
            }

            reader.Close();

            return texto;
        }
        #endregion
    }
}
