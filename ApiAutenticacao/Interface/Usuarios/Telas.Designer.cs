﻿namespace Interface
{
    partial class Telas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Telas));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gcListagem = new DevExpress.XtraEditors.GroupControl();
            this.pcRodape = new DevExpress.XtraEditors.PanelControl();
            this.gcMenu = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcListagem)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcRodape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // gcListagem
            // 
            this.gcListagem.Location = new System.Drawing.Point(151, 72);
            this.gcListagem.Name = "gcListagem";
            this.gcListagem.Size = new System.Drawing.Size(832, 280);
            this.gcListagem.TabIndex = 22;
            this.gcListagem.Text = "Listagem";
            this.gcListagem.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // pcRodape
            // 
            this.pcRodape.AutoSize = true;
            this.pcRodape.Location = new System.Drawing.Point(0, 365);
            this.pcRodape.Name = "pcRodape";
            this.pcRodape.Size = new System.Drawing.Size(995, 35);
            this.pcRodape.TabIndex = 23;
            // 
            // gcMenu
            // 
            this.gcMenu.Location = new System.Drawing.Point(5, 72);
            this.gcMenu.Name = "gcMenu";
            this.gcMenu.Size = new System.Drawing.Size(140, 280);
            this.gcMenu.TabIndex = 24;
            this.gcMenu.Text = "Menu";
            // 
            // Telas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(995, 401);
            this.Controls.Add(this.gcMenu);
            this.Controls.Add(this.pcRodape);
            this.Controls.Add(this.gcListagem);
            this.Controls.Add(this.pictureBox1);
            this.IconOptions.ColorizeInactiveIcon = DevExpress.Utils.DefaultBoolean.True;
            this.IconOptions.ShowIcon = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Telas";
            this.Text = "TELAS";
            this.Load += new System.EventHandler(this.Cadastro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcListagem)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcRodape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.GroupControl gcListagem;
        private DevExpress.XtraEditors.PanelControl pcRodape;
        private DevExpress.XtraEditors.GroupControl gcMenu;
    }
}