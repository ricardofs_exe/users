﻿namespace Interface
{
    partial class frmInstituicaoStatus
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmInstituicaoStatus));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.gcLista = new DevExpress.XtraEditors.GroupControl();
            this.pcRodape = new DevExpress.XtraEditors.PanelControl();
            this.gcMenu = new DevExpress.XtraEditors.GroupControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcRodape)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMenu)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(5, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(78, 45);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // gcLista
            // 
            this.gcLista.Location = new System.Drawing.Point(105, 54);
            this.gcLista.Name = "gcLista";
            this.gcLista.Size = new System.Drawing.Size(990, 474);
            this.gcLista.TabIndex = 22;
            this.gcLista.Text = "Dados";
            this.gcLista.Paint += new System.Windows.Forms.PaintEventHandler(this.groupControl1_Paint);
            // 
            // pcRodape
            // 
            this.pcRodape.AutoSize = true;
            this.pcRodape.Location = new System.Drawing.Point(5, 534);
            this.pcRodape.Name = "pcRodape";
            this.pcRodape.Size = new System.Drawing.Size(1090, 37);
            this.pcRodape.TabIndex = 23;
            // 
            // gcMenu
            // 
            this.gcMenu.Location = new System.Drawing.Point(5, 54);
            this.gcMenu.Name = "gcMenu";
            this.gcMenu.Size = new System.Drawing.Size(94, 474);
            this.gcMenu.TabIndex = 24;
            this.gcMenu.Text = "Menu";
            // 
            // frmInstituicaoStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1100, 572);
            this.Controls.Add(this.gcMenu);
            this.Controls.Add(this.pcRodape);
            this.Controls.Add(this.gcLista);
            this.Controls.Add(this.pictureBox1);
            this.IconOptions.ColorizeInactiveIcon = DevExpress.Utils.DefaultBoolean.True;
            this.IconOptions.ShowIcon = false;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmInstituicaoStatus";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Instituição Status";
            this.Load += new System.EventHandler(this.Cadastro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pcRodape)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcMenu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.GroupControl gcLista;
        private DevExpress.XtraEditors.PanelControl pcRodape;
        private DevExpress.XtraEditors.GroupControl gcMenu;
    }
}