﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using DevExpress.Skins.XtraForm;
using DevExpress.Utils.Drawing;
using DevExpress.Utils;
using DevExpress.LookAndFeel;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json;

namespace Interface
{
    public partial class Esqueci : DevExpress.XtraEditors.XtraForm
    {
        public Esqueci()
        {
            InitializeComponent();
            this.BackColor = Color.White;
        }
        protected override FormPainter CreateFormBorderPainter()
        {
            return new MyFormPainter(this, DevExpress.LookAndFeel.UserLookAndFeel.Default.ActiveLookAndFeel);
        }

        public class MyFormPainter : FormPainter
        {
            private HorzAlignment CaptionAlignment;

            public MyFormPainter(Control owner, ISkinProvider provider) : base(owner, provider) { }

            protected override void DrawBackground(GraphicsCache cache)
            {
                cache.FillRectangle(Color.FromArgb(65, 105, 225), CaptionBounds);
            }

            protected override void DrawText(DevExpress.Utils.Drawing.GraphicsCache cache)
            {
                string text = Text;
                if (text == null || text.Length == 0 || TextBounds.IsEmpty)
                {
                    return;
                }
                AppearanceObject appearance = new AppearanceObject(GetDefaultAppearance());
                appearance.TextOptions.Trimming = Trimming.EllipsisCharacter;
                appearance.TextOptions.HAlignment = CaptionAlignment;
                if (AllowHtmlDraw)
                {
                    DrawHtmlText(cache, appearance);
                    return;
                }
                Rectangle r = RectangleHelper.GetCenterBounds(TextBounds, new Size(TextBounds.Width, CalcTextHeight(cache, appearance)));
                DrawTextShadow(cache, appearance, r);
                Color foreColor = Color.FromArgb(255, 250, 250);
                Brush textBrush = new SolidBrush(foreColor);
                cache.DrawString(text, appearance.Font, textBrush, r, appearance.GetStringFormat());
            }
        }


        private async void btnEnvio_Click(object sender, EventArgs e)
        {
            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            try
            {
                if (txtEmail.Text != "" || txtEmail.Text == null)
                {
                    if (rg.IsMatch(txtEmail.Text))
                    {
                        string url = "http://localhost:58260/api/account/esquecisenha";

                        EsqueciSenha _user = new EsqueciSenha();
                        _user.Email = txtEmail.Text;

                        using (var client = new HttpClient())
                        {
                            var serializedUsuario = JsonConvert.SerializeObject(_user);
                            var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                            var result = await client.PostAsync(url, content);
                            string CodStatus = result.StatusCode.ToString();

                            if (CodStatus.ToString() == "OK")
                            {
                                XtraMessageBox.Show("Email com token enviado", "Confirmation");
                                Limpar();
                                this.Close();
                            }
                            else
                            {
                                XtraMessageBox.Show("Email não encontrado", "Confirmation");
                                Limpar();
                            }
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("Email não encontrado", "Confirmation");
                    Limpar();
                }
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }

        public void Limpar()
        {
            txtEmail.Text = "";
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            Limpar();
        }
    }
}