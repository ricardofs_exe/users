﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Interface
{
    public partial class Cadastrar : Form
    {
        public Cadastrar()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string URL = txtUrl.Text;
            string Login = txtLogin.Text;
            string Senha = txtSenha.Text;
            string ConfirmarSenha = txtConfimaSenha.Text;

            AddUsuario(URL,Login,Senha,ConfirmarSenha);
        }

        private async void AddUsuario(string url, string login, string senha, string confirmasenha)
        {
            Usuario _user = new Usuario();
            _user.UserName = login.ToString();
            _user.Password = senha.ToString();
            _user.ConfirmPassword = confirmasenha.ToString();
            
            using (var client = new HttpClient())
            {
                var serializedUsuario = JsonConvert.SerializeObject(_user);
                var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                var result = await client.PostAsync(url, content);
            }

            MessageBox.Show("Dados Cadastrados");
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            string URL = txtUrl.Text;
            string Login = txtLogin.Text;
            string Senha = txtSenha.Text;
            Logar(URL,Login,Senha);
        }

        private async void Logar(string url,string login, string senha)
        {
            Usuario _user = new Usuario();
            _user.UserName = login.ToString();
            _user.Password = senha.ToString();


            var request = (HttpWebRequest)WebRequest.Create(url);

            var postData = "username=" + Uri.EscapeDataString(_user.UserName.ToString());
            postData += "&password=" + Uri.EscapeDataString(_user.Password.ToString());
            postData += "&grant_type=" + Uri.EscapeDataString("password");
            var data = Encoding.ASCII.GetBytes(postData);

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = data.Length;

            using (var stream = request.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }

            var response = (HttpWebResponse)request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            dynamic dobj = JsonConvert.DeserializeObject<dynamic>(responseString);

            string token = dobj["access_token"];

            retornoJsonAsync(token, url);

        }

        private async void retornoJsonAsync(string token, string baseUrl)
        {
            bool validar = false;
            
            Usuario _user = new Usuario();

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:58260/api/orders");

            client.DefaultRequestHeaders.Add("Authorization", "bearer " + token);

            HttpResponseMessage response = await client.GetAsync("/api/orders");
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                validar = true;
                txtRetorno.Text = dt.ToString();
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void txtRetorno_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
