﻿namespace Interface
{
    partial class UserControlStatusUsuario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcStatus = new DevExpress.XtraEditors.GroupControl();
            this.btnEditarStatus = new DevExpress.XtraEditors.SimpleButton();
            this.btnInseir = new DevExpress.XtraEditors.SimpleButton();
            this.txtIdStatus = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirStatus = new DevExpress.XtraEditors.SimpleButton();
            this.txtStatus = new DevExpress.XtraEditors.TextEdit();
            this.Status = new System.Windows.Forms.Label();
            this.txtDescricao = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).BeginInit();
            this.gcStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricao.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(968, 269);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.RowDeleted += new DevExpress.Data.RowDeletedEventHandler(this.gridView1_RowDeleted);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(6, 153);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(972, 294);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Listagem";
            // 
            // gcStatus
            // 
            this.gcStatus.Controls.Add(this.btnEditarStatus);
            this.gcStatus.Controls.Add(this.btnInseir);
            this.gcStatus.Controls.Add(this.txtIdStatus);
            this.gcStatus.Controls.Add(this.btnExcluirStatus);
            this.gcStatus.Controls.Add(this.txtStatus);
            this.gcStatus.Controls.Add(this.Status);
            this.gcStatus.Controls.Add(this.txtDescricao);
            this.gcStatus.Controls.Add(this.label13);
            this.gcStatus.Location = new System.Drawing.Point(6, 5);
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Size = new System.Drawing.Size(972, 142);
            this.gcStatus.TabIndex = 71;
            this.gcStatus.Text = "Status Usuário";
            // 
            // btnEditarStatus
            // 
            this.btnEditarStatus.Location = new System.Drawing.Point(811, 116);
            this.btnEditarStatus.Name = "btnEditarStatus";
            this.btnEditarStatus.Size = new System.Drawing.Size(75, 23);
            this.btnEditarStatus.TabIndex = 67;
            this.btnEditarStatus.Text = "Editar";
            this.btnEditarStatus.Click += new System.EventHandler(this.btnEditarStatus_Click);
            // 
            // btnInseir
            // 
            this.btnInseir.Location = new System.Drawing.Point(730, 116);
            this.btnInseir.Name = "btnInseir";
            this.btnInseir.Size = new System.Drawing.Size(75, 23);
            this.btnInseir.TabIndex = 66;
            this.btnInseir.Text = "Inserir";
            this.btnInseir.Click += new System.EventHandler(this.btnInseir_Click);
            // 
            // txtIdStatus
            // 
            this.txtIdStatus.Location = new System.Drawing.Point(867, 26);
            this.txtIdStatus.Name = "txtIdStatus";
            this.txtIdStatus.Size = new System.Drawing.Size(100, 20);
            this.txtIdStatus.TabIndex = 65;
            this.txtIdStatus.Visible = false;
            // 
            // btnExcluirStatus
            // 
            this.btnExcluirStatus.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirStatus.Name = "btnExcluirStatus";
            this.btnExcluirStatus.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirStatus.TabIndex = 64;
            this.btnExcluirStatus.Text = "Excluir";
            this.btnExcluirStatus.Click += new System.EventHandler(this.btnExcluirStatus_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(75, 35);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(164, 20);
            this.txtStatus.TabIndex = 58;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(16, 38);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(48, 13);
            this.Status.TabIndex = 57;
            this.Status.Text = "Status : ";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(75, 61);
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(164, 20);
            this.txtDescricao.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 43;
            this.label13.Text = "Descrição : ";
            // 
            // UserControlStatusUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcStatus);
            this.Controls.Add(this.groupControl2);
            this.Name = "UserControlStatusUsuario";
            this.Size = new System.Drawing.Size(985, 456);
            this.Load += new System.EventHandler(this.UserControlStatusUsuario_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).EndInit();
            this.gcStatus.ResumeLayout(false);
            this.gcStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricao.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl gcStatus;
        private DevExpress.XtraEditors.SimpleButton btnInseir;
        private DevExpress.XtraEditors.TextEdit txtIdStatus;
        private DevExpress.XtraEditors.SimpleButton btnExcluirStatus;
        private DevExpress.XtraEditors.TextEdit txtStatus;
        private System.Windows.Forms.Label Status;
        private DevExpress.XtraEditors.TextEdit txtDescricao;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.SimpleButton btnEditarStatus;
    }
}
