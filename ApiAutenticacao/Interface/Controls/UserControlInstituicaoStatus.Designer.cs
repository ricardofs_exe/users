﻿namespace Interface
{
    partial class UserControlInstituicaoStatus
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcStatusInst = new DevExpress.XtraEditors.GroupControl();
            this.btnEditarInst = new DevExpress.XtraEditors.SimpleButton();
            this.btnInserirInst = new DevExpress.XtraEditors.SimpleButton();
            this.txtInstId = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirInst = new DevExpress.XtraEditors.SimpleButton();
            this.txtStatusInst = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescricaoInst = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatusInst)).BeginInit();
            this.gcStatusInst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInst.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(968, 269);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.RowDeleted += new DevExpress.Data.RowDeletedEventHandler(this.gridView1_RowDeleted);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(6, 153);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(972, 294);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Listagem";
            // 
            // gcStatusInst
            // 
            this.gcStatusInst.Controls.Add(this.btnEditarInst);
            this.gcStatusInst.Controls.Add(this.btnInserirInst);
            this.gcStatusInst.Controls.Add(this.txtInstId);
            this.gcStatusInst.Controls.Add(this.btnExcluirInst);
            this.gcStatusInst.Controls.Add(this.txtStatusInst);
            this.gcStatusInst.Controls.Add(this.label4);
            this.gcStatusInst.Controls.Add(this.txtDescricaoInst);
            this.gcStatusInst.Controls.Add(this.label5);
            this.gcStatusInst.Location = new System.Drawing.Point(6, 5);
            this.gcStatusInst.Name = "gcStatusInst";
            this.gcStatusInst.Size = new System.Drawing.Size(972, 142);
            this.gcStatusInst.TabIndex = 72;
            this.gcStatusInst.Text = "Status Instituição";
            // 
            // btnEditarInst
            // 
            this.btnEditarInst.Location = new System.Drawing.Point(811, 116);
            this.btnEditarInst.Name = "btnEditarInst";
            this.btnEditarInst.Size = new System.Drawing.Size(75, 23);
            this.btnEditarInst.TabIndex = 67;
            this.btnEditarInst.Text = "Editar";
            this.btnEditarInst.Click += new System.EventHandler(this.btnEditarInst_Click);
            // 
            // btnInserirInst
            // 
            this.btnInserirInst.Location = new System.Drawing.Point(730, 116);
            this.btnInserirInst.Name = "btnInserirInst";
            this.btnInserirInst.Size = new System.Drawing.Size(75, 23);
            this.btnInserirInst.TabIndex = 66;
            this.btnInserirInst.Text = "Inserir";
            this.btnInserirInst.Click += new System.EventHandler(this.btnInserirInst_Click);
            // 
            // txtInstId
            // 
            this.txtInstId.Location = new System.Drawing.Point(867, 26);
            this.txtInstId.Name = "txtInstId";
            this.txtInstId.Size = new System.Drawing.Size(100, 20);
            this.txtInstId.TabIndex = 65;
            this.txtInstId.Visible = false;
            // 
            // btnExcluirInst
            // 
            this.btnExcluirInst.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirInst.Name = "btnExcluirInst";
            this.btnExcluirInst.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirInst.TabIndex = 64;
            this.btnExcluirInst.Text = "Excluir";
            this.btnExcluirInst.Click += new System.EventHandler(this.btnExcluirInst_Click);
            // 
            // txtStatusInst
            // 
            this.txtStatusInst.Location = new System.Drawing.Point(75, 35);
            this.txtStatusInst.Name = "txtStatusInst";
            this.txtStatusInst.Size = new System.Drawing.Size(164, 20);
            this.txtStatusInst.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Status : ";
            // 
            // txtDescricaoInst
            // 
            this.txtDescricaoInst.Location = new System.Drawing.Point(75, 61);
            this.txtDescricaoInst.Name = "txtDescricaoInst";
            this.txtDescricaoInst.Size = new System.Drawing.Size(164, 20);
            this.txtDescricaoInst.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "Descrição : ";
            // 
            // UserControlInstituicaoStatus
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcStatusInst);
            this.Controls.Add(this.groupControl2);
            this.Name = "UserControlInstituicaoStatus";
            this.Size = new System.Drawing.Size(983, 452);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcStatusInst)).EndInit();
            this.gcStatusInst.ResumeLayout(false);
            this.gcStatusInst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInst.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl gcStatusInst;
        private DevExpress.XtraEditors.SimpleButton btnEditarInst;
        private DevExpress.XtraEditors.SimpleButton btnInserirInst;
        private DevExpress.XtraEditors.TextEdit txtInstId;
        private DevExpress.XtraEditors.SimpleButton btnExcluirInst;
        private DevExpress.XtraEditors.TextEdit txtStatusInst;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtDescricaoInst;
        private System.Windows.Forms.Label label5;
    }
}
