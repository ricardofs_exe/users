﻿namespace Interface
{
    partial class MenuUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuUserControl));
            this.btnCadastro = new DevExpress.XtraEditors.SimpleButton();
            this.btnInstituicao = new DevExpress.XtraEditors.SimpleButton();
            this.btnTelas = new DevExpress.XtraEditors.SimpleButton();
            this.SuspendLayout();
            // 
            // btnCadastro
            // 
            this.btnCadastro.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnCadastro.ImageOptions.Image")));
            this.btnCadastro.Location = new System.Drawing.Point(3, 3);
            this.btnCadastro.Name = "btnCadastro";
            this.btnCadastro.Size = new System.Drawing.Size(75, 32);
            this.btnCadastro.TabIndex = 6;
            this.btnCadastro.Text = "Cadastro";
            this.btnCadastro.Click += new System.EventHandler(this.btnCadastro_Click);
            // 
            // btnInstituicao
            // 
            this.btnInstituicao.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnInstituicao.ImageOptions.Image")));
            this.btnInstituicao.Location = new System.Drawing.Point(3, 41);
            this.btnInstituicao.Name = "btnInstituicao";
            this.btnInstituicao.Size = new System.Drawing.Size(75, 32);
            this.btnInstituicao.TabIndex = 7;
            this.btnInstituicao.Text = "Instituição";
            this.btnInstituicao.Click += new System.EventHandler(this.btnInstituicao_Click);
            // 
            // btnTelas
            // 
            this.btnTelas.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnTelas.ImageOptions.Image")));
            this.btnTelas.Location = new System.Drawing.Point(3, 79);
            this.btnTelas.Name = "btnTelas";
            this.btnTelas.Size = new System.Drawing.Size(75, 32);
            this.btnTelas.TabIndex = 8;
            this.btnTelas.Text = "Telas";
            this.btnTelas.Click += new System.EventHandler(this.btnTelas_Click);
            // 
            // MenuUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnTelas);
            this.Controls.Add(this.btnInstituicao);
            this.Controls.Add(this.btnCadastro);
            this.Name = "MenuUserControl";
            this.Size = new System.Drawing.Size(90, 415);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnCadastro;
        private DevExpress.XtraEditors.SimpleButton btnInstituicao;
        private DevExpress.XtraEditors.SimpleButton btnTelas;
    }
}
