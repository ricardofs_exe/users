﻿namespace Interface
{
    partial class UserControlInstituicao
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label14;
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcInstituicao = new DevExpress.XtraEditors.GroupControl();
            this.cboInstStatus = new System.Windows.Forms.ComboBox();
            this.btnInstsInserir = new DevExpress.XtraEditors.SimpleButton();
            this.txtInstCnpj = new DevExpress.XtraEditors.TextEdit();
            this.txtIdInst = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirInstituicao = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditarInstituicao = new DevExpress.XtraEditors.SimpleButton();
            this.txtNomeInst = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDescricaoInsti = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcInstituicao)).BeginInit();
            this.gcInstituicao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstCnpj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInsti.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(11, 116);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(45, 13);
            label14.TabIndex = 51;
            label14.Text = "Status :";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(968, 269);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.RowDeleted += new DevExpress.Data.RowDeletedEventHandler(this.gridView1_RowDeleted);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(6, 153);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(972, 294);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Listagem";
            // 
            // gcInstituicao
            // 
            this.gcInstituicao.Controls.Add(this.cboInstStatus);
            this.gcInstituicao.Controls.Add(this.btnInstsInserir);
            this.gcInstituicao.Controls.Add(this.txtInstCnpj);
            this.gcInstituicao.Controls.Add(this.txtIdInst);
            this.gcInstituicao.Controls.Add(this.btnExcluirInstituicao);
            this.gcInstituicao.Controls.Add(this.btnEditarInstituicao);
            this.gcInstituicao.Controls.Add(this.txtNomeInst);
            this.gcInstituicao.Controls.Add(this.label10);
            this.gcInstituicao.Controls.Add(label14);
            this.gcInstituicao.Controls.Add(this.label15);
            this.gcInstituicao.Controls.Add(this.txtDescricaoInsti);
            this.gcInstituicao.Controls.Add(this.label16);
            this.gcInstituicao.Location = new System.Drawing.Point(6, 5);
            this.gcInstituicao.Name = "gcInstituicao";
            this.gcInstituicao.Size = new System.Drawing.Size(972, 142);
            this.gcInstituicao.TabIndex = 71;
            this.gcInstituicao.Text = "Instituição";
            // 
            // cboInstStatus
            // 
            this.cboInstStatus.FormattingEnabled = true;
            this.cboInstStatus.Location = new System.Drawing.Point(77, 113);
            this.cboInstStatus.Name = "cboInstStatus";
            this.cboInstStatus.Size = new System.Drawing.Size(164, 21);
            this.cboInstStatus.TabIndex = 72;
            // 
            // btnInstsInserir
            // 
            this.btnInstsInserir.Location = new System.Drawing.Point(730, 116);
            this.btnInstsInserir.Name = "btnInstsInserir";
            this.btnInstsInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInstsInserir.TabIndex = 68;
            this.btnInstsInserir.Text = "Inserir";
            this.btnInstsInserir.Click += new System.EventHandler(this.btnInstsInserir_Click);
            // 
            // txtInstCnpj
            // 
            this.txtInstCnpj.Location = new System.Drawing.Point(77, 87);
            this.txtInstCnpj.Name = "txtInstCnpj";
            this.txtInstCnpj.Properties.BeepOnError = false;
            this.txtInstCnpj.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.txtInstCnpj.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.txtInstCnpj.Properties.MaskSettings.Set("mask", "99.999.999/9999-99");
            this.txtInstCnpj.Size = new System.Drawing.Size(164, 20);
            this.txtInstCnpj.TabIndex = 71;
            // 
            // txtIdInst
            // 
            this.txtIdInst.Location = new System.Drawing.Point(867, 26);
            this.txtIdInst.Name = "txtIdInst";
            this.txtIdInst.Size = new System.Drawing.Size(100, 20);
            this.txtIdInst.TabIndex = 65;
            this.txtIdInst.Visible = false;
            // 
            // btnExcluirInstituicao
            // 
            this.btnExcluirInstituicao.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirInstituicao.Name = "btnExcluirInstituicao";
            this.btnExcluirInstituicao.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirInstituicao.TabIndex = 64;
            this.btnExcluirInstituicao.Text = "Excluir";
            this.btnExcluirInstituicao.Click += new System.EventHandler(this.btnExcluirInstituicao_Click);
            // 
            // btnEditarInstituicao
            // 
            this.btnEditarInstituicao.Location = new System.Drawing.Point(811, 116);
            this.btnEditarInstituicao.Name = "btnEditarInstituicao";
            this.btnEditarInstituicao.Size = new System.Drawing.Size(75, 23);
            this.btnEditarInstituicao.TabIndex = 63;
            this.btnEditarInstituicao.Text = "Editar";
            this.btnEditarInstituicao.Click += new System.EventHandler(this.btnEditarInstituicao_Click);
            // 
            // txtNomeInst
            // 
            this.txtNomeInst.Location = new System.Drawing.Point(77, 35);
            this.txtNomeInst.Name = "txtNomeInst";
            this.txtNomeInst.Size = new System.Drawing.Size(164, 20);
            this.txtNomeInst.TabIndex = 58;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 57;
            this.label10.Text = "Nome : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "CNPJ : ";
            // 
            // txtDescricaoInsti
            // 
            this.txtDescricaoInsti.Location = new System.Drawing.Point(77, 61);
            this.txtDescricaoInsti.Name = "txtDescricaoInsti";
            this.txtDescricaoInsti.Size = new System.Drawing.Size(164, 20);
            this.txtDescricaoInsti.TabIndex = 44;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Descrição : ";
            // 
            // UserControlInstituicao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcInstituicao);
            this.Controls.Add(this.groupControl2);
            this.Name = "UserControlInstituicao";
            this.Size = new System.Drawing.Size(984, 453);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcInstituicao)).EndInit();
            this.gcInstituicao.ResumeLayout(false);
            this.gcInstituicao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstCnpj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInsti.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl gcInstituicao;
        private DevExpress.XtraEditors.SimpleButton btnInstsInserir;
        private DevExpress.XtraEditors.TextEdit txtInstCnpj;
        private DevExpress.XtraEditors.TextEdit txtIdInst;
        private DevExpress.XtraEditors.SimpleButton btnExcluirInstituicao;
        private DevExpress.XtraEditors.SimpleButton btnEditarInstituicao;
        private DevExpress.XtraEditors.TextEdit txtNomeInst;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit txtDescricaoInsti;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboInstStatus;
    }
}
