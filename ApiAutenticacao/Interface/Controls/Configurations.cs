﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Controls
{
    public static class Configurations
    {
        public static void GridViewConfig(ref DevExpress.XtraGrid.Views.Grid.GridView GridViewContent, bool showHeaders = true, DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode SelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CellSelect, bool MultiSelect = true, bool RowHeader = false, bool AutoWidth = false, bool ShowGroupPanel = false, bool ColumnMoving = false, bool AllowSort = true, bool CellBackColor = false, bool ShowFooter = false, bool CopyToClipboardColumnHeader = false, bool ShowDetailButtons = false)
        {
            GridViewContent.ActiveFilterEnabled = false;

            GridViewContent.OptionsView.ShowFooter = ShowFooter;

            GridViewContent.Appearance.SelectedRow.Options.UseBackColor = CellBackColor;

            GridViewContent.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            GridViewContent.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            GridViewContent.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            GridViewContent.OptionsBehavior.Editable = false;
            GridViewContent.OptionsBehavior.ReadOnly = true;
            GridViewContent.OptionsBehavior.CopyToClipboardWithColumnHeaders = CopyToClipboardColumnHeader;
            GridViewContent.Columns.View.OptionsBehavior.KeepFocusedRowOnUpdate = true;

            GridViewContent.OptionsCustomization.AllowFilter = false;
            GridViewContent.OptionsCustomization.AllowQuickHideColumns = false;
            GridViewContent.OptionsCustomization.AllowColumnMoving = ColumnMoving;
            GridViewContent.OptionsCustomization.AllowSort = AllowSort;

            GridViewContent.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            GridViewContent.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            GridViewContent.OptionsFilter.AllowColumnMRUFilterList = false;
            GridViewContent.OptionsFilter.AllowMRUFilterList = false;
            GridViewContent.OptionsFilter.AllowFilterEditor = false;
            GridViewContent.OptionsFilter.AllowFilterIncrementalSearch = false;
            GridViewContent.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;

            // pensar
            GridViewContent.OptionsSelection.MultiSelectMode = SelectMode;
            GridViewContent.OptionsSelection.MultiSelect = MultiSelect;
            GridViewContent.OptionsSelection.EnableAppearanceFocusedCell = false;
            GridViewContent.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;

            GridViewContent.OptionsMenu.EnableColumnMenu = false;

            GridViewContent.OptionsView.ShowDetailButtons = ShowDetailButtons;
            GridViewContent.OptionsView.ShowGroupPanel = ShowGroupPanel;
            GridViewContent.OptionsView.ShowIndicator = RowHeader;
            GridViewContent.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

            GridViewContent.OptionsView.ShowColumnHeaders = showHeaders;
            GridViewContent.OptionsView.ColumnAutoWidth = AutoWidth;
        }
    }
}
