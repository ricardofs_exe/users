﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Interface
{
    public partial class MenuUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        private string Rtoken;
        private string Rusuario;
        public MenuUserControl( string token, string usuario)
        {
            InitializeComponent();
            Rtoken = token;
        }

        private void btnCadastro_Click(object sender, EventArgs e)
        {
            Cadastro frm = new Cadastro();
            frm.Show();
        }

        private void btnInstituicao_Click(object sender, EventArgs e)
        {
            Padrao frm = new Padrao(Rtoken,"Instituicao",Rusuario);
            frm.Text = "Instituição";
            frm.Show();
        }

        private void btnTelas_Click(object sender, EventArgs e)
        {
            Padrao frm = new Padrao(Rtoken, "Telas", Rusuario);
            frm.Text = "Telas";
            frm.Show();
        }
    }
}
