﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Script.Serialization;
using DevExpress.DataAccess.Native.Json;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using Interface.Models;

namespace Interface
{
    public partial class ListaUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        #region Variaveis
        /// <summary>
        /// Variavel tokenA Global que transporta o token nas classes
        /// </summary>
        private string tokenA { get; set; }

        /// <summary>
        /// Variavel usuarios Global retorno de json entre as classes
        /// </summary>
        private string usuarios { get; set; }

        /// <summary>
        /// Variavel status Global retorno de json entre as classes
        /// </summary>
        private string status { get; set; }

        /// <summary>
        /// Variavel Inststatus Global retorno de json entre as classes
        /// </summary>
        private string Inststatus { get; set; }

        /// <summary>
        /// Variavel Instituicao Global retorno de json entre as classes
        /// </summary>
        private string Instituicao { get; set; }

        /// <summary>
        /// Variavel InstituicaoResponsavel Global retorno de json entre as classes
        /// </summary>
        private string InstituicaoResponsavel { get; set; }

        /// <summary>
        /// Variavel CentroAdministrativo Global retorno de json entre as classes
        /// </summary>
        private string CentroAdministrativo { get; set;}

        /// <summary>
        /// Variavel botao Global retorna o botão que foi clicado para carregar a tela
        /// </summary>
        private string botao { get; set; }

        /// <summary>
        /// Variavel user Global retorna o botão que foi logado
        /// </summary>
        private string user { get; set; }
        
        #endregion

        #region Métodos

        public ListaUserControl(string token, string botao, string usuario)
        {
            
            InitializeComponent();

            try
            {
                tokenA = token;
                user = usuario;

                gcStatus.Visible = false;
                gcFormulario.Visible = false;
                gcStatusInst.Visible = false;
                gcInstituicao.Visible = false;
                gcInstituicaoResponsavel.Visible = false;
                gcCentroAdministrativo.Visible = false;

                #region CarregarCombos
                CarregarStatusCombo(token);
                CarregarStatusInstCombo(token);
                CarregarComboInstituicao(token);
                #endregion

                switch (botao)
                {
                    case "Usuarios":
                        gcFormulario.Visible = true;
                        gcFormulario.Location = new Point(6, 4);
                        CarregarUsuarios(token);
                        break;
                    case "Status":
                        gcStatus.Visible = true;
                        gcStatus.Location = new Point(6,4);
                        CarregarStatus(token);
                        break;
                    case "Instituição Status":
                        gcStatusInst.Visible = true;
                        gcStatusInst.Location = new Point(6, 4);
                        CarregarStatusInst(token);
                        break;
                    case "Instituição":
                        gcInstituicao.Visible = true;
                        gcInstituicao.Location = new Point(6, 4);
                        CarregarInstituicao(token);
                        break;
                    case "Instituição Responsável":
                        gcInstituicaoResponsavel.Visible = true;
                        gcInstituicaoResponsavel.Location = new Point(6, 4);
                        CarregarInstituicaoResponsavel(token);
                        break;
                    case "Centro Administrativo":
                        gcCentroAdministrativo.Visible = true;
                        gcCentroAdministrativo.Location = new Point(6, 4);
                        CarregarCentroAdministrativo(token);
                        break;
                }
                
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }


        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                string id = gridView1.GetFocusedRowCellValue("Id").ToString();

                switch (botao)
                {
                    case "Usuario":
                        CarregarUsuario(id);
                        break;
                    case "Status":
                        CarregarStatusId(int.Parse(id));
                        break;
                    case "Instituicao Status":
                        CarregarInstStatusId(int.Parse(id));
                        break;
                    case "Instituicao":
                        CarregarInstituicaoId(int.Parse(id));
                        break;
                    case "Instituicao Responsavel":
                        CarregarInstituicaoResponsavelId(int.Parse(id));
                        break;
                    case "Centro Administrativo":
                        CarregarCentroAdministrativoId(int.Parse(id));
                        break;



                }

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void gridView1_RowDeleted(object sender, DevExpress.Data.RowDeletedEventArgs e)
        {
            //string id;
            //id = gridView1.GetFocusedRowCellValue("Id").ToString();

        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            /*string id = gridView1.GetFocusedRowCellValue("Id").ToString();
            CarregarUsuario(id);*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textEdit3_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoUsuarios();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnEditarStatus_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnEditarInst_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoInstStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnEditarInstituicao_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoInstituicao();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private async void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirUsuarios();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirStatus_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirInst_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirInstStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirInstituicao_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirInstituicao();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void simpleButton2_Click(object sender, EventArgs e)
        {

        }

        private void btnInserirInst_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarInstStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInseir_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInstsInserir_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarInstituicao();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInserirInstResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarInstituicaoResponsavel();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInserirCentroAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarCentroAdministrativo();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnEditarInstResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoInstResponsavel();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirInstResponsavel_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirInstituicaoResponsavel();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnEditarCentroAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoCentroAdministrativo();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirCentroAdmin_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirCentroAdministrativo();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

        #region Classes

        /// <summary>
        /// Classe que carrega combo de Status de Usuaário
        /// </summary>
        /// <param name="tk">Token do Usuário</param>
        public async void CarregarStatusCombo(string tk)
        {
            try
            {
                
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/status");
                HttpResponseMessage response = await client.GetAsync("/api/orders/status");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(dt);

                var query = from h in dados
                            select h;

                cbStatus.DataSource = query.ToList();
                cbStatus.DisplayMember = "status";
                cbStatus.ValueMember = "usuario_status_id";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Edição de Insttuição
        /// </summary>
        public async void EdicaoInstituicao()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarinstituicao";

                instituicao _instituicao = new instituicao();

                if (txtIdInst.Text != "")
                    _instituicao.instituicao_id = int.Parse(txtIdInst.Text);

                if (txtNomeInst.Text != "")
                    _instituicao.nome = txtNomeInst.Text;

                if (txtDescricaoInsti.Text != "")
                    _instituicao.descricao = txtDescricaoInsti.Text;

                if (txtInstCnpj.Text != "")
                    _instituicao.cnpj = txtInstCnpj.Text;

                _instituicao.atualizado = DateTime.Parse(DateTime.Now.ToString());

                if (cboInstStatus.SelectedValue != null)
                    _instituicao.instituicao_status = int.Parse(cboInstStatus.SelectedValue.ToString());

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarinstituicao");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarinstituicao");

                    var serializedUsuario = JsonConvert.SerializeObject(_instituicao);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarInstituicao(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Edição de Status de Instituicao
        /// </summary>
        public async void EdicaoInstStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarinststatus";

                InstituicaoStatus _status = new InstituicaoStatus();

                if (txtInstId.Text != "")
                    _status.instituicao_status_id = int.Parse(txtInstId.Text);

                if (txtStatusInst.Text != "")
                    _status.status = txtStatusInst.Text;

                if (txtDescricaoInst.Text != "")
                    _status.descricao = txtDescricaoInst.Text;

                _status.atualizado = DateTime.Parse(DateTime.Now.ToString());


                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarinststatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarinststatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_status);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarStatusInst(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        
        /// <summary>
        /// Edição de Status de Usuario
        /// </summary>
        public async void EdicaoStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarstatus";

                UserStatus _status = new UserStatus();

                if (txtIdStatus.Text != "")
                    _status.usuario_status_id = int.Parse(txtIdStatus.Text);

                if (txtStatus.Text != "")
                    _status.status = txtStatus.Text;

                if (txtDescricao.Text != "")
                    _status.descricao = txtDescricao.Text;

                _status.atualizado = DateTime.Parse(DateTime.Now.ToString());
                _status.usuario = user;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarstatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarstatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_status);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarStatus(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Edição de Instituição Responsavel
        /// </summary>
        public async void EdicaoInstResponsavel()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/AtualizarInstituicaoResponsavel";

                instituicao_responsavel _instr = new instituicao_responsavel();

                if (cbInstResponsavel.SelectedValue.ToString() != "")
                    _instr.instituicao = int.Parse(cbInstResponsavel.SelectedValue.ToString());

                if (txtNomeInstResponsavel.Text != "")
                    _instr.nome = txtNomeInstResponsavel.Text;

                if (txtEmailInstResponsavel.Text != "")
                    _instr.email = txtEmailInstResponsavel.Text;

                if (txtTelefoneInstResponsavel.Text != "")
                    _instr.telefone = txtTelefoneInstResponsavel.Text;

                if (txtCelInstResponsavel.Text != "")
                    _instr.celular = txtCelInstResponsavel.Text;

                if (txtDescricaoInstResponsavel.Text != "")
                    _instr.descricao = txtDescricaoInstResponsavel.Text;

                if (txtCpfCnpjInstResponsavel.Text != "")
                    _instr.cpf_cnpj = txtCpfCnpjInstResponsavel.Text;


                _instr.atualizado = DateTime.Parse(DateTime.Now.ToString());
                _instr.usuario = user;
                _instr.instituicao_responsavel_id = int.Parse(txtIDInstResponsavel.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/AtualizarInstituicaoResponsavel");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/AtualizarInstituicaoResponsavel");

                    var serializedUsuario = JsonConvert.SerializeObject(_instr);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarInstituicaoResponsavel(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Edição de Centro Administrativo
        /// </summary>
        public async void EdicaoCentroAdministrativo()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/AtualizarCentroAdministrativo";

                centro_administrativo _ca = new centro_administrativo();

                if (cboInstituicaoCentroAdmin.SelectedValue.ToString() != "")
                    _ca.instituicao = int.Parse(cboInstituicaoCentroAdmin.SelectedValue.ToString());

                if (txtNomeCentroAdmin.Text != "")
                    _ca.nome = txtNomeCentroAdmin.Text;

                if (txtDescricaoCentroAdmin.Text != "")
                    _ca.descricao = txtDescricaoCentroAdmin.Text;

                if (txtCpfCnpjCentroAdmin.Text != "")
                    _ca.cpf_cnpj = txtCpfCnpjCentroAdmin.Text;


                _ca.atualizado = DateTime.Parse(DateTime.Now.ToString());
                _ca.usuario = user;
                _ca.centro_administrativo_id = int.Parse(txtIdCentroAdmin.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/AtualizarCentroAdministrativo");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/AtualizarCentroAdministrativo");

                    var serializedUsuario = JsonConvert.SerializeObject(_ca);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarCentroAdministrativo(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Classe que faz a Edição de Usuários
        /// </summary>
        public async void EdicaoUsuarios()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarusuario";

                UsuarioTabela _user = new UsuarioTabela();

                if (txtID.Text != "")
                    _user.Id = txtID.Text;

                if (txtEmail.Text != "")
                    _user.Email = txtEmail.Text;

                if (txtLogin.Text != "")
                    _user.UserName = txtLogin.Text;

                if (txtData.Text != "")
                    _user.data = DateTime.Parse(txtData.Text);

                if (cbStatus.SelectedValue != null)
                    _user.usuario_status = int.Parse(cbStatus.SelectedValue.ToString());

                if (cbEspecial.SelectedItem == "Sim")
                    _user.especial = true;
                else
                    _user.especial = false;


                if (cbCargo.SelectedItem != null)
                    _user.cargo = int.Parse(cbCargo.SelectedItem.ToString());

                if (txtNome.Text != "")
                    _user.nomeCompleto = txtNome.Text;

                _user.atualizado = DateTime.Parse(DateTime.Now.ToString());



                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarusuario");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarusuario");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarUsuarios(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        
        /// <summary>
        /// Carrega Status de Usuários
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatus(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/status");
                HttpResponseMessage response = await client.GetAsync("/api/orders/status");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                status = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(dt);
                var query = from _status in dados
                            select new
                            {
                                Id = _status.usuario_status_id,
                                Status = _status.status,
                                Descrição = _status.descricao,
                                Atualizado = _status.atualizado,
                                Usuario = _status.usuario,
                            };


                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
                botao = "Status";
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega combo de Status de Usuário
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatusInstCombo(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/inststatus");
                HttpResponseMessage response = await client.GetAsync("/api/orders/inststatus");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                var dados = JsonConvert.DeserializeObject<IEnumerable<InstituicaoStatus>>(dt);
                var query = from h in dados
                            select h;

                cboInstStatus.DataSource = query.ToList();
                cboInstStatus.DisplayMember = "status";
                cboInstStatus.ValueMember = "instituicao_status_id";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega Status de Usuário
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatusInst(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/inststatus");
                HttpResponseMessage response = await client.GetAsync("/api/orders/inststatus");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                Inststatus = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<InstituicaoStatus>>(dt);

                var query = from _inst in dados
                            select new
                            {
                                Id = _inst.instituicao_status_id,
                                Data = _inst.data,
                                Status = _inst.status,
                                Descrição = _inst.descricao,
                                Atualizado = _inst.atualizado,
                                Usuario = _inst.usuario
                            };

                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
                botao = "Instituicao Status";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega dados de Instituições
        /// </summary>
        /// <param name="tk">Token de Acesso de Usuário</param>
        public async void CarregarInstituicao (string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/Instituicao");
                HttpResponseMessage response = await client.GetAsync("/api/orders/Instituicao");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                Instituicao = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao>>(dt);
                var query = from g in dados
                            select new
                            {
                                Id = g.instituicao_id,
                                DataCriação = g.data,
                                Instituição = g.nome,
                                Status = g.status,
                                DescritivoInstituição = g.descricao,
                                CNPJ = g.cnpj,
                                Atualizado = g.atualizado,
                                Usuario = g.usuario
                            };
                gridControl1.DataSource = query;
                gridView1.Columns[0].Visible = false;
                botao = "Instituicao";
                
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega dados de Combo de Instituições
        /// </summary>
        /// <param name="tk">Token de Acesso de Usuário</param>
        public async void CarregarComboInstituicao(string tk)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/Instituicao");
                HttpResponseMessage response = await client.GetAsync("/api/orders/Instituicao");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao>>(dt);
                var query = from g in dados
                            select g;

                cbInstResponsavel.DataSource = query.ToList();
                cbInstResponsavel.DisplayMember = "nome";
                cbInstResponsavel.ValueMember = "instituicao_id";

                cboInstituicaoCentroAdmin.DataSource = query.ToList();
                cboInstituicaoCentroAdmin.DisplayMember = "nome";
                cboInstituicaoCentroAdmin.ValueMember = "instituicao_id";
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega dados de Instituições
        /// </summary>
        /// <param name="tk">Token de Acesso de Usuário</param>
        public async void CarregarCentroAdministrativo(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/CentroAdministrativo");
                HttpResponseMessage response = await client.GetAsync("/api/orders/CentroAdministrativo");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                CentroAdministrativo = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<centro_administrativo>>(dt);
                var query = from g in dados
                            select new
                            {
                                Id = g.centro_administrativo_id,
                                DataCriação = g.data,
                                Instituição = g.instituicaoN,
                                Nome = g.nome,
                                Descrição = g.descricao,
                                CpfCnpj = g.cpf_cnpj,
                                Atualizado = g.atualizado,
                                Usuario = g.usuario
                            };
                gridControl1.DataSource = query;
                gridView1.Columns[0].Visible = false;
                botao = "Centro Administrativo";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega dados de Instituições Responsável
        /// </summary>
        /// <param name="tk">Token de Acesso de Usuário</param>
        public async void CarregarInstituicaoResponsavel(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/InstituicaoResponsavel");
                HttpResponseMessage response = await client.GetAsync("/api/orders/InstituicaoResponsavel");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                InstituicaoResponsavel = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao_responsavel>>(dt);
                var query = from g in dados
                            select new
                            {
                                Id = g.instituicao_responsavel_id,
                                Instituição = g.instituicaoN,
                                Nome = g.nome,
                                Email = g.email,
                                Telefone = g.telefone,
                                Celular = g.celular,
                                Descrição = g.descricao,
                                CpfCnpj = g.cpf_cnpj,
                                Atualizado = g.atualizado,
                                Usuario = g.usuario
                            };
                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
                botao = "Instituicao Responsavel";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega os dados ds Usuários no Grid a partir de um json
        /// </summary>
        /// <param name="tk">Token de Usuário</param>
        public async void CarregarUsuarios(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/usuarios");
                HttpResponseMessage response = await client.GetAsync("/api/orders/usuarios");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                usuarios = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<JsonModelUsuario>>(dt);

                var query = from user in dados
                            select new
                            {
                                Id = user.Id,
                                Nome = user.Nome,
                                Email = user.Email,
                                Login = user.Login,
                                Data = user.Data,
                                Criado = user.Criado,
                                Atualizado = user.Atualizado,
                                Status = user.Mstatus,
                                AcessoEspecial = user.Especial,
                                Cargo = user.Cargo
                            };

                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
                botao = "Usuario";
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega usuário em formulário de acordo com Id
        /// </summary>
        /// <param name="id">Id de Usuário</param>
        public async void CarregarUsuario(string id)
        {
            try
            {
                string criado = "", atualizado = "", nome = "", login = "", data = "", email = "";
                int? status, cargo;
                bool especial;

                cbEspecial.Properties.Items.Clear();
                cbEspecial.Properties.Items.Add("Sim");
                cbEspecial.Properties.Items.Add("Não");

                var dados = JsonConvert.DeserializeObject<IEnumerable<JsonModelUsuario>>(usuarios);

                var query = from g in dados
                            where g.Id == id
                            select new
                            {
                                Id = g.Id,
                                Email = g.Email,
                                Login = g.Login,
                                Data = g.Data,
                                Criado = g.Criado,
                                Atualizado = g.Atualizado,
                                Status = g.Status,
                                Especial = g.Especial,
                                Cargo = g.Cargo,
                                Nome = g.Nome
                            };

                var usuario = query.ToList();

                especial = bool.Parse(usuario[0].Especial.ToString());

                if (usuario[0].Email == null)
                    email = "";
                else
                    email = usuario[0].Email.ToString();

                if (usuario[0].Criado == null)
                    criado = "";
                else
                    criado = usuario[0].Criado.ToString();

                if (usuario[0].Data == null)
                    data = "";
                else
                    data = usuario[0].Data.ToString();

                if (usuario[0].Login == null)
                    login = "";
                else
                    login = usuario[0].Login.ToString();

                if (usuario[0].Criado == null)
                    criado = "";
                else
                    criado = usuario[0].Criado.ToString();

                if (usuario[0].Atualizado == null)
                    atualizado = "";
                else
                    atualizado = usuario[0].Atualizado.ToString();

                if (usuario[0].Status == null)
                    status = null;
                else
                    status = int.Parse(usuario[0].Status.ToString());

                if (usuario[0].Cargo == null)
                    cargo = null;
                else
                    cargo = int.Parse(usuario[0].Cargo.ToString());

                if (usuario[0].Nome == null)
                    nome = "";
                else
                    nome = usuario[0].Nome.ToString();

                txtID.Text = id;
                txtEmail.Text = email.ToString();
                txtLogin.Text = login;
                txtData.Text = data.ToString();
                cbStatus.SelectedItem = status;
                cbCargo.SelectedItem = cargo;
                txtNome.Text = nome;

                if (especial)
                    cbEspecial.SelectedItem = "Sim";
                else
                    cbEspecial.SelectedItem = "Não";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        
        /// <summary>
        /// Carrega formulário de Instituição deAcordo com Id
        /// </summary>
        /// <param name="id">Id de instituição</param>
        public async void CarregarInstituicaoId(int id)
        {
            try
            {
                string nome, descricao, cnpj, status;
                int ? statusId;

                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao>>(Instituicao);

                var query = from g in dados
                            where g.instituicao_id == id
                            select new
                            {
                                Id = g.instituicao_id,
                                Status = g.status,
                                StatusId = g.instituicao_status,
                                Nome = g.nome,
                                Descricao = g.descricao,
                                Cnpj = g.cnpj
                                
                            };

                var qIntituicao = query.ToList();

                if (qIntituicao[0].Status == null)
                    status = "";
                else
                   status = qIntituicao[0].Status.ToString();

                if (qIntituicao[0].StatusId == null)
                    statusId = null;
                else
                    statusId = int.Parse(qIntituicao[0].StatusId.ToString());

                if (qIntituicao[0].Nome == null)
                    nome = "";
                else
                    nome = qIntituicao[0].Nome.ToString();

                if (qIntituicao[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qIntituicao[0].Descricao.ToString();

                if (qIntituicao[0].Cnpj == null)
                    cnpj = "";
                else
                    cnpj = qIntituicao[0].Cnpj.ToString();

                cboInstStatus.SelectedValue = statusId;
                txtNomeInst.Text = nome;
                txtDescricaoInsti.Text = descricao;
                txtInstCnpj.Text = cnpj;
                txtIdInst.Text = id.ToString();

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Carrega formulário de Instituição Responsável por Id
        /// </summary>
        /// <param name="id">Id de instituição</param>
        public async void CarregarInstituicaoResponsavelId(int id)
        {
            try
            {
                string nome, email, telefone, celular, descricao, cpf_cnpj;
                int? intituicaoI;

                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao_responsavel>>(InstituicaoResponsavel);

                var query = from g in dados
                            where g.instituicao_responsavel_id == id
                            select new
                            {
                                Instituicao = g.instituicao,
                                Nome = g.nome,
                                Email = g.email,
                                Telefone = g.telefone,
                                Celular = g.celular,
                                Descricao = g.descricao,
                                CpfCnpj = g.cpf_cnpj
                            };

                var qInstResp = query.ToList();

                txtIDInstResponsavel.Text = id.ToString();

                if (qInstResp[0].Instituicao == null)
                    intituicaoI = 0;
                else
                    intituicaoI = qInstResp[0].Instituicao;

                if (qInstResp[0].Nome == null)
                    nome = null;
                else
                    nome = qInstResp[0].Nome.ToString();

                if (qInstResp[0].Email == null)
                    email = "";
                else
                    email = qInstResp[0].Email.ToString();

                if (qInstResp[0].Telefone == null)
                    telefone = "";
                else
                    telefone = qInstResp[0].Telefone.ToString();

                if (qInstResp[0].Celular == null)
                    celular = "";
                else
                    celular = qInstResp[0].Celular.ToString();

                if (qInstResp[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qInstResp[0].Descricao.ToString();

                if (qInstResp[0].CpfCnpj == null)
                    cpf_cnpj = "";
                else
                    cpf_cnpj = qInstResp[0].CpfCnpj.ToString();

                cbInstResponsavel.SelectedValue = intituicaoI;
                txtNomeInstResponsavel.Text = nome;
                txtEmailInstResponsavel.Text = email;
                txtTelefoneInstResponsavel.Text = telefone;
                txtCelInstResponsavel.Text = celular;
                txtDescricaoInstResponsavel.Text = descricao;
                txtCpfCnpjInstResponsavel.Text = cpf_cnpj;
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Carrega Formulário de  Centro Administrativo
        /// </summary>
        /// <param name="id">Id de Centro Administrativo</param>
        public async void CarregarCentroAdministrativoId(int id)
        {
            try
            {
                string nome, descricao, cpf_cnpj;
                int? intituicaoI;

                var dados = JsonConvert.DeserializeObject<IEnumerable<centro_administrativo>>(CentroAdministrativo);

                var query = from g in dados
                            where g.centro_administrativo_id == id
                            select new
                            {
                                Instituicao = g.instituicao,
                                Nome = g.nome,
                                Descricao = g.descricao,
                                CpfCnpj = g.cpf_cnpj,
                                Id = g.centro_administrativo_id
                            };

                var qCa = query.ToList();
                cboInstituicaoCentroAdmin.SelectedValue = int.Parse(qCa[0].Instituicao.ToString());
                txtIdCentroAdmin.Text = id.ToString();

                if (qCa[0].Nome == null)
                    nome = null;
                else
                    nome = qCa[0].Nome.ToString();

                if (qCa[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qCa[0].Descricao.ToString();

                if (qCa[0].CpfCnpj == null)
                    cpf_cnpj = "";
                else
                    cpf_cnpj = qCa[0].CpfCnpj.ToString();

                txtNomeCentroAdmin.Text = nome;
                txtDescricaoCentroAdmin.Text = descricao;
                txtCpfCnpjCentroAdmin.Text = cpf_cnpj;
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }


        /// <summary>
        /// Carrega formulário de Status de Usuário
        /// </summary>
        /// <param name="id">Id de Sattus de Usuário</param>
        public async void CarregarStatusId(int id)
        {
            try
            {
                string t_status, descricao;
                
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(status);

                var query = from g in dados
                            where g.usuario_status_id == id
                            select new
                            {
                                Id = g.usuario_status_id,
                                Status = g.status,
                                Descricao = g.descricao
                            };

                var qstatus = query.ToList();

                if (qstatus[0].Status == null)
                    t_status = "";
                else
                    t_status = qstatus[0].Status.ToString();

                if (qstatus[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qstatus[0].Descricao.ToString();

                txtIdStatus.Text = id.ToString();
                txtStatus.Text = t_status;
                txtDescricao.Text = descricao;                

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        
        /// <summary>
        /// Carrega formulário de Status de Instituição
        /// </summary>
        /// <param name="id">Id de Instituição Status</param>
        public async void CarregarInstStatusId(int id)
        {
            try
            {
                string t_status, descricao;

                var dados = JsonConvert.DeserializeObject<IEnumerable<InstituicaoStatus>>(Inststatus);

                var query = from g in dados
                            where g.instituicao_status_id == id
                            select new
                            {
                                Id = g.instituicao_status_id,
                                Status = g.status,
                                Descricao = g.descricao
                            };

                var qstatus = query.ToList();

                if (qstatus[0].Status == null)
                    t_status = "";
                else
                    t_status = qstatus[0].Status.ToString();

                if (qstatus[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qstatus[0].Descricao.ToString();

                txtInstId.Text = id.ToString();
                txtStatusInst.Text = t_status;
                txtDescricaoInst.Text = descricao;

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Instituição
        /// </summary>
        public async void ExcluirInstituicao()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagainstituicao";
                instituicao _inst = new instituicao();
                if (txtIdInst.Text != "")
                    _inst.instituicao_id = int.Parse(txtIdInst.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagainstituicao");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagainstituicao");

                    var serializedUsuario = JsonConvert.SerializeObject(_inst);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarInstituicao(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Excluir Centro Administrativo
        /// </summary>
        public async void ExcluirCentroAdministrativo()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagaCentroAdministrativo";
                centro_administrativo _ca = new centro_administrativo();
                if (txtIdCentroAdmin.Text != "")
                    _ca.centro_administrativo_id = int.Parse(txtIdCentroAdmin.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagaCentroAdministrativo");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagaCentroAdministrativo");

                    var serializedUsuario = JsonConvert.SerializeObject(_ca);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarCentroAdministrativo(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Instituição
        /// </summary>
        public async void AdicionarInstituicao()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/inseririnstituicao";

                DateTime data = DateTime.Now;
                instituicao _inst = new instituicao();

                _inst.data = data;
                _inst.nome = txtNomeInst.Text;
                _inst.descricao = txtDescricaoInsti.Text;
                _inst.cnpj = txtInstCnpj.Text;
                _inst.instituicao_status = int.Parse(cboInstStatus.SelectedValue.ToString());

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/inseririnstituicao");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/inseririnstituicao");

                    var serializedUsuario = JsonConvert.SerializeObject(_inst);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                        CarregarInstituicao(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Instituição Responsavel
        /// </summary>
        public async void AdicionarInstituicaoResponsavel()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/inseririnstituicaoresponsavel";

                DateTime data = DateTime.Now;
                instituicao_responsavel _inst = new instituicao_responsavel();

                _inst.data = data;
                _inst.nome = txtNomeInstResponsavel.Text;
                _inst.email = txtEmailInstResponsavel.Text;
                _inst.telefone = txtTelefoneInstResponsavel.Text;
                _inst.celular = txtCelInstResponsavel.Text;
                _inst.descricao = txtDescricaoInstResponsavel.Text;
                _inst.cpf_cnpj = txtCpfCnpjInstResponsavel.Text;
                _inst.instituicao = int.Parse(cbInstResponsavel.SelectedValue.ToString());
                _inst.usuario = user;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/inseririnstituicaoresponsavel");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/inseririnstituicaoresponsavel");

                    var serializedUsuario = JsonConvert.SerializeObject(_inst);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                        CarregarInstituicaoResponsavel(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Centro Administrativo
        /// </summary>
        public async void AdicionarCentroAdministrativo()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/inserircentroadministrativo";

                DateTime data = DateTime.Now;
                centro_administrativo _ca = new centro_administrativo();

                _ca.data = data;
                _ca.nome = txtNomeCentroAdmin.Text;
                _ca.descricao = txtDescricaoCentroAdmin.Text;
                _ca.cpf_cnpj = txtCpfCnpjCentroAdmin.Text;
                _ca.instituicao = int.Parse(cboInstituicaoCentroAdmin.SelectedValue.ToString());
                _ca.usuario = user;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/inserircentroadministrativo");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/inserircentroadministrativo");

                    var serializedUsuario = JsonConvert.SerializeObject(_ca);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                        CarregarInstituicaoResponsavel(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Status de Instituição
        /// </summary>
        public async void AdicionarInstStatus()
        {
            try
            {
                try
                {
                    string url = "http://localhost:58260/api/orders/inseririnststatus";

                    DateTime data = DateTime.Now;
                    InstituicaoStatus _user = new InstituicaoStatus();

                    _user.data = data;
                    _user.status = txtStatusInst.Text;
                    _user.descricao = txtDescricaoInst.Text;
                    _user.usuario = user.ToString();

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:58260/api/orders/inseririnststatus");

                        client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                        HttpResponseMessage response = await client.GetAsync("/api/orders/inseririnststatus");

                        var serializedUsuario = JsonConvert.SerializeObject(_user);
                        var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                        var result = await client.PostAsync(url, content);
                        string CodStatus = result.StatusCode.ToString();

                        if (CodStatus.ToString() == "OK")
                        {
                            XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                            CarregarStatusInst(tokenA);
                        }
                        else
                        {
                            XtraMessageBox.Show("Erro", "Confirmation");
                        }
                    }
                }
                catch (Exception er)
                {
                    XtraMessageBox.Show(er.Message, "Erro");
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Status de usuário
        /// </summary>
        public async void AdicionarStatus()
        {
            try
            {
                try
                {
                    string url = "http://localhost:58260/api/orders/inserirstatus";

                    DateTime data = DateTime.Now;

                    



                    UserStatus _user = new UserStatus();

                    _user.data = data;
                    _user.status = txtStatus.Text;
                    _user.descricao = txtDescricao.Text;
                    _user.usuario = user.ToString();

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:58260/api/orders/inserirstatus");

                        client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                        HttpResponseMessage response = await client.GetAsync("/api/orders/inserirstatus");

                        var serializedUsuario = JsonConvert.SerializeObject(_user);
                        var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                        var result = await client.PostAsync(url, content);
                        string CodStatus = result.StatusCode.ToString();

                        if (CodStatus.ToString() == "OK")
                        {
                            XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                            CarregarStatus(tokenA);
                        }
                        else
                        {
                            XtraMessageBox.Show("Erro", "Confirmation");
                        }
                    }
                }
                catch (Exception er)
                {
                    XtraMessageBox.Show(er.Message, "Erro");
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Usuário
        /// </summary>
        public async void ExcluirUsuarios()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagausuario";

                UsuarioTabela _user = new UsuarioTabela();

                if (txtID.Text != "")
                    _user.Id = txtID.Text;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagausuario");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagausuario");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarUsuarios(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Status de Instituição
        /// </summary>
        public async void ExcluirInstStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagainststatus";
                InstituicaoStatus _user = new InstituicaoStatus();
                if (txtInstId.Text != "")
                    _user.instituicao_status_id = int.Parse(txtInstId.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagainststatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagainststatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarStatusInst(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Status de usuário
        /// </summary>
        public async void ExcluirStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagastatus";
                UserStatus _user = new UserStatus();
                if (txtIdStatus.Text != "")
                    _user.usuario_status_id = int.Parse(txtIdStatus.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagaatatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagaatatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarStatus(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Instituicao Responsavel
        /// </summary>
        public async void ExcluirInstituicaoResponsavel()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/ApagaInstituicaoResponsavel";
                instituicao_responsavel _instr = new instituicao_responsavel();
                if (txtIDInstResponsavel.Text != "")
                    _instr.instituicao_responsavel_id = int.Parse(txtIDInstResponsavel.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/ApagaInstituicaoResponsavel");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/ApagaInstituicaoResponsavel");

                    var serializedUsuario = JsonConvert.SerializeObject(_instr);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarInstituicaoResponsavel(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }






        #endregion

        
    }
}