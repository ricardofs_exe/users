﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Script.Serialization;
using DevExpress.DataAccess.Native.Json;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using Interface.Models;

namespace Interface
{
    public partial class UserControlUsuario : DevExpress.XtraEditors.XtraUserControl
    {
        
        #region Variaveis
        /// <summary>
        /// Variavel tokenA Global que transporta o token nas classes
        /// </summary>
        private string tokenA { get; set; }

        /// <summary>
        /// Variavel usuarios Global retorno de json entre as classes
        /// </summary>
        private string usuarios { get; set; }

       /// <summary>
        /// Variavel user Global retorna o botão que foi logado
        /// </summary>
        private string user { get; set; }
        
        #endregion

        #region Métodos

        public UserControlUsuario(string token, string botao, string usuario)
        {
            
            InitializeComponent();

            #region ConfGrid

            gridView1.ActiveFilterEnabled = false;

            gridView1.OptionsView.ShowFooter = false;

            gridView1.Appearance.SelectedRow.Options.UseBackColor = false;

            gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            gridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
            gridView1.OptionsBehavior.Editable = false;
            gridView1.OptionsBehavior.ReadOnly = true;
            gridView1.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
            gridView1.Columns.View.OptionsBehavior.KeepFocusedRowOnUpdate = true;

            gridView1.OptionsCustomization.AllowFilter = false;
            gridView1.OptionsCustomization.AllowQuickHideColumns = false;
            gridView1.OptionsCustomization.AllowColumnMoving = false;
            gridView1.OptionsCustomization.AllowSort = true;

            gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
            gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
            gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
            gridView1.OptionsFilter.AllowMRUFilterList = false;
            gridView1.OptionsFilter.AllowFilterEditor = false;
            gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
            gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;

            // pensar
            //gridView1.OptionsSelection.MultiSelectMode = true;
            gridView1.OptionsSelection.MultiSelect = true;
            gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;

            gridView1.OptionsMenu.EnableColumnMenu = false;

            gridView1.OptionsView.ShowDetailButtons = false;
            gridView1.OptionsView.ShowGroupPanel = false;
            gridView1.OptionsView.ShowIndicator = false;
            gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

            gridView1.OptionsView.ShowColumnHeaders = true;
            gridView1.OptionsView.ColumnAutoWidth = false;

            #endregion



            try
            {
                tokenA = token;
                user = usuario;
                gcFormulario.Visible = false;
                CarregarStatusCombo(token);

                switch (botao)
                {
                    case "Usuarios":
                        gcFormulario.Visible = true;
                        gcFormulario.Location = new Point(6, 4);
                        CarregarUsuarios(token);
                        break;
                }
                
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                string id = gridView1.GetFocusedRowCellValue("Id").ToString();
                CarregarUsuario(id);
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void gridView1_RowDeleted(object sender, DevExpress.Data.RowDeletedEventArgs e)
        {
            //string id;
            //id = gridView1.GetFocusedRowCellValue("Id").ToString();

        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            /*string id = gridView1.GetFocusedRowCellValue("Id").ToString();
            CarregarUsuario(id);*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textEdit3_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private async void btnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoUsuarios();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private async void btnExcluir_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirUsuarios();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        #endregion

        #region Classes        
        /// <summary>
        /// Classe que carrega combo de Status de Usuaário
        /// </summary>
        /// <param name="tk">Token do Usuário</param>
        public async void CarregarStatusCombo(string tk)
        {
            try
            {

                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/status");
                HttpResponseMessage response = await client.GetAsync("/api/orders/status");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(dt);

                var query = from h in dados
                            select h;

                cbStatus.DataSource = query.ToList();
                cbStatus.DisplayMember = "status";
                cbStatus.ValueMember = "usuario_status_id";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Classe que faz a Edição de Usuários
        /// </summary>
        public async void EdicaoUsuarios()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarusuario";

                UsuarioTabela _user = new UsuarioTabela();

                if (txtID.Text != "")
                    _user.Id = txtID.Text;

                if (txtEmail.Text != "")
                    _user.Email = txtEmail.Text;

                if (txtLogin.Text != "")
                    _user.UserName = txtLogin.Text;

                if (txtData.Text != "")
                    _user.data = DateTime.Parse(txtData.Text);

                if (cbStatus.SelectedValue != null)
                    _user.usuario_status = int.Parse(cbStatus.SelectedValue.ToString());

                if (cbEspecial.SelectedItem == "Sim")
                    _user.especial = true;
                else
                    _user.especial = false;


                if (cbCargo.SelectedItem != null)
                    _user.cargo = int.Parse(cbCargo.SelectedItem.ToString());

                if (txtNome.Text != "")
                    _user.nomeCompleto = txtNome.Text;

                _user.atualizado = DateTime.Parse(DateTime.Now.ToString());



                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarusuario");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarusuario");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarUsuarios(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Carrega os dados ds Usuários no Grid a partir de um json
        /// </summary>
        /// <param name="tk">Token de Usuário</param>
        public async void CarregarUsuarios(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/usuarios");
                HttpResponseMessage response = await client.GetAsync("/api/orders/usuarios");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                usuarios = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<JsonModelUsuario>>(dt);

                var query = from user in dados
                            select new
                            {
                                Id = user.Id,
                                Nome = user.Nome,
                                Email = user.Email,
                                Login = user.Login,
                                Data = user.Data,
                                Criado = user.Criado,
                                Atualizado = user.Atualizado,
                                Status = user.Mstatus,
                                AcessoEspecial = user.Especial,
                                Cargo = user.Cargo
                            };

                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega usuário em formulário de acordo com Id
        /// </summary>
        /// <param name="id">Id de Usuário</param>
        public async void CarregarUsuario(string id)
        {
            try
            {
                string criado = "", atualizado = "", nome = "", login = "", data = "", email = "";
                int? status, cargo;
                bool especial;

                cbEspecial.Properties.Items.Clear();
                cbEspecial.Properties.Items.Add("Sim");
                cbEspecial.Properties.Items.Add("Não");

                var dados = JsonConvert.DeserializeObject<IEnumerable<JsonModelUsuario>>(usuarios);

                var query = from g in dados
                            where g.Id == id
                            select new
                            {
                                Id = g.Id,
                                Email = g.Email,
                                Login = g.Login,
                                Data = g.Data,
                                Criado = g.Criado,
                                Atualizado = g.Atualizado,
                                Status = g.Status,
                                Especial = g.Especial,
                                Cargo = g.Cargo,
                                Nome = g.Nome
                            };

                var usuario = query.ToList();

                especial = bool.Parse(usuario[0].Especial.ToString());

                if (usuario[0].Email == null)
                    email = "";
                else
                    email = usuario[0].Email.ToString();

                if (usuario[0].Criado == null)
                    criado = "";
                else
                    criado = usuario[0].Criado.ToString();

                if (usuario[0].Data == null)
                    data = "";
                else
                    data = usuario[0].Data.ToString();

                if (usuario[0].Login == null)
                    login = "";
                else
                    login = usuario[0].Login.ToString();

                if (usuario[0].Criado == null)
                    criado = "";
                else
                    criado = usuario[0].Criado.ToString();

                if (usuario[0].Atualizado == null)
                    atualizado = "";
                else
                    atualizado = usuario[0].Atualizado.ToString();

                if (usuario[0].Status == null)
                    status = null;
                else
                    status = int.Parse(usuario[0].Status.ToString());

                if (usuario[0].Cargo == null)
                    cargo = null;
                else
                    cargo = int.Parse(usuario[0].Cargo.ToString());

                if (usuario[0].Nome == null)
                    nome = "";
                else
                    nome = usuario[0].Nome.ToString();

                txtID.Text = id;
                txtEmail.Text = email.ToString();
                txtLogin.Text = login;
                txtData.Text = data.ToString();
                cbStatus.SelectedItem = status;
                cbCargo.SelectedItem = cargo;
                txtNome.Text = nome;

                if (especial)
                    cbEspecial.SelectedItem = "Sim";
                else
                    cbEspecial.SelectedItem = "Não";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Usuário
        /// </summary>
        public async void ExcluirUsuarios()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagausuario";

                UsuarioTabela _user = new UsuarioTabela();

                if (txtID.Text != "")
                    _user.Id = txtID.Text;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagausuario");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagausuario");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarUsuarios(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

        
    }
}