﻿namespace Interface.Controls
{
    partial class StatusUsuario
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gcStatus = new DevExpress.XtraEditors.GroupControl();
            this.btnEditarStatus = new DevExpress.XtraEditors.SimpleButton();
            this.btnInseir = new DevExpress.XtraEditors.SimpleButton();
            this.txtIdStatus = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirStatus = new DevExpress.XtraEditors.SimpleButton();
            this.txtStatus = new DevExpress.XtraEditors.TextEdit();
            this.Status = new System.Windows.Forms.Label();
            this.txtDescricao = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.gcBusca = new DevExpress.XtraEditors.GroupControl();
            this.resourcesComboBoxControl1 = new DevExpress.XtraScheduler.UI.ResourcesComboBoxControl();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonEdit1 = new DevExpress.XtraEditors.ButtonEdit();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).BeginInit();
            this.gcStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBusca)).BeginInit();
            this.gcBusca.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesComboBoxControl1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // gcStatus
            // 
            this.gcStatus.Controls.Add(this.btnEditarStatus);
            this.gcStatus.Controls.Add(this.btnInseir);
            this.gcStatus.Controls.Add(this.txtIdStatus);
            this.gcStatus.Controls.Add(this.btnExcluirStatus);
            this.gcStatus.Controls.Add(this.txtStatus);
            this.gcStatus.Controls.Add(this.Status);
            this.gcStatus.Controls.Add(this.txtDescricao);
            this.gcStatus.Controls.Add(this.label13);
            this.gcStatus.Location = new System.Drawing.Point(3, 3);
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Size = new System.Drawing.Size(972, 142);
            this.gcStatus.TabIndex = 72;
            this.gcStatus.Text = "Status Usuário";
            // 
            // btnEditarStatus
            // 
            this.btnEditarStatus.Location = new System.Drawing.Point(811, 116);
            this.btnEditarStatus.Name = "btnEditarStatus";
            this.btnEditarStatus.Size = new System.Drawing.Size(75, 23);
            this.btnEditarStatus.TabIndex = 67;
            this.btnEditarStatus.Text = "Editar";
            // 
            // btnInseir
            // 
            this.btnInseir.Location = new System.Drawing.Point(730, 116);
            this.btnInseir.Name = "btnInseir";
            this.btnInseir.Size = new System.Drawing.Size(75, 23);
            this.btnInseir.TabIndex = 66;
            this.btnInseir.Text = "Inserir";
            // 
            // txtIdStatus
            // 
            this.txtIdStatus.Location = new System.Drawing.Point(867, 26);
            this.txtIdStatus.Name = "txtIdStatus";
            this.txtIdStatus.Size = new System.Drawing.Size(100, 20);
            this.txtIdStatus.TabIndex = 65;
            this.txtIdStatus.Visible = false;
            // 
            // btnExcluirStatus
            // 
            this.btnExcluirStatus.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirStatus.Name = "btnExcluirStatus";
            this.btnExcluirStatus.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirStatus.TabIndex = 64;
            this.btnExcluirStatus.Text = "Excluir";
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(75, 35);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(164, 20);
            this.txtStatus.TabIndex = 58;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(16, 38);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(48, 13);
            this.Status.TabIndex = 57;
            this.Status.Text = "Status : ";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(75, 61);
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(164, 20);
            this.txtDescricao.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 43;
            this.label13.Text = "Descrição : ";
            // 
            // gcBusca
            // 
            this.gcBusca.Controls.Add(this.buttonEdit1);
            this.gcBusca.Controls.Add(this.label1);
            this.gcBusca.Controls.Add(this.resourcesComboBoxControl1);
            this.gcBusca.Location = new System.Drawing.Point(3, 151);
            this.gcBusca.Name = "gcBusca";
            this.gcBusca.Size = new System.Drawing.Size(972, 54);
            this.gcBusca.TabIndex = 73;
            this.gcBusca.Text = "Busca";
            // 
            // resourcesComboBoxControl1
            // 
            this.resourcesComboBoxControl1.Location = new System.Drawing.Point(58, 26);
            this.resourcesComboBoxControl1.Name = "resourcesComboBoxControl1";
            this.resourcesComboBoxControl1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.resourcesComboBoxControl1.Size = new System.Drawing.Size(100, 20);
            this.resourcesComboBoxControl1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 68;
            this.label1.Text = "Campo: ";
            // 
            // buttonEdit1
            // 
            this.buttonEdit1.Location = new System.Drawing.Point(165, 27);
            this.buttonEdit1.Name = "buttonEdit1";
            this.buttonEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.buttonEdit1.Size = new System.Drawing.Size(415, 20);
            this.buttonEdit1.TabIndex = 69;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(3, 211);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(972, 294);
            this.groupControl2.TabIndex = 74;
            this.groupControl2.Text = "Listagem";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(968, 269);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // StatusUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.gcBusca);
            this.Controls.Add(this.gcStatus);
            this.Name = "StatusUsuario";
            this.Size = new System.Drawing.Size(980, 509);
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).EndInit();
            this.gcStatus.ResumeLayout(false);
            this.gcStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcBusca)).EndInit();
            this.gcBusca.ResumeLayout(false);
            this.gcBusca.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.resourcesComboBoxControl1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl gcStatus;
        private DevExpress.XtraEditors.SimpleButton btnEditarStatus;
        private DevExpress.XtraEditors.SimpleButton btnInseir;
        private DevExpress.XtraEditors.TextEdit txtIdStatus;
        private DevExpress.XtraEditors.SimpleButton btnExcluirStatus;
        private DevExpress.XtraEditors.TextEdit txtStatus;
        private System.Windows.Forms.Label Status;
        private DevExpress.XtraEditors.TextEdit txtDescricao;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.GroupControl gcBusca;
        private DevExpress.XtraEditors.ButtonEdit buttonEdit1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraScheduler.UI.ResourcesComboBoxControl resourcesComboBoxControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
    }
}
