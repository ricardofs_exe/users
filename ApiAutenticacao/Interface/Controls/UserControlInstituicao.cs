﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Script.Serialization;
using DevExpress.DataAccess.Native.Json;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using Interface.Models;

namespace Interface
{
    public partial class UserControlInstituicao : DevExpress.XtraEditors.XtraUserControl
    {
        #region Variaveis
        /// <summary>
        /// Variavel tokenA Global que transporta o token nas classes
        /// </summary>
        private string tokenA { get; set; }

        /// <summary>
        /// Variavel usuarios Global retorno de json entre as classes
        /// </summary>
        private string usuarios { get; set; }

        /// <summary>
        /// Variavel Instituicao Global retorno de json entre as classes
        /// </summary>
        private string Instituicao { get; set; }

        /// <summary>
        /// Variavel botao Global retorna o botão que foi clicado para carregar a tela
        /// </summary>
        private string botao { get; set; }

        /// <summary>
        /// Variavel user Global retorna o botão que foi logado
        /// </summary>
        private string user { get; set; }
        
        #endregion

        #region Métodos

        public UserControlInstituicao(string token, string botao, string usuario)
        {
            
            InitializeComponent();

            try
            {
                tokenA = token;
                user = usuario;
                gcInstituicao.Visible = false;
                CarregarStatusInstCombo(token);

                #region ConfGrid

                gridView1.ActiveFilterEnabled = false;

                gridView1.OptionsView.ShowFooter = false;

                gridView1.Appearance.SelectedRow.Options.UseBackColor = false;

                gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.Editable = false;
                gridView1.OptionsBehavior.ReadOnly = true;
                gridView1.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
                gridView1.Columns.View.OptionsBehavior.KeepFocusedRowOnUpdate = true;

                gridView1.OptionsCustomization.AllowFilter = false;
                gridView1.OptionsCustomization.AllowQuickHideColumns = false;
                gridView1.OptionsCustomization.AllowColumnMoving = false;
                gridView1.OptionsCustomization.AllowSort = true;

                gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
                gridView1.OptionsFilter.AllowMRUFilterList = false;
                gridView1.OptionsFilter.AllowFilterEditor = false;
                gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
                gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;

                // pensar
                //gridView1.OptionsSelection.MultiSelectMode = true;
                gridView1.OptionsSelection.MultiSelect = true;
                gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
                gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;

                gridView1.OptionsMenu.EnableColumnMenu = false;

                gridView1.OptionsView.ShowDetailButtons = false;
                gridView1.OptionsView.ShowGroupPanel = false;
                gridView1.OptionsView.ShowIndicator = false;
                gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

                gridView1.OptionsView.ShowColumnHeaders = true;
                gridView1.OptionsView.ColumnAutoWidth = false;

                #endregion

                switch (botao)
                {
                    case "Instituicao":
                        gcInstituicao.Visible = true;
                        gcInstituicao.Location = new Point(6, 4);
                        CarregarInstituicao(token);
                        break;
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }


        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                string id = gridView1.GetFocusedRowCellValue("Id").ToString();
                CarregarInstituicaoId(int.Parse(id));
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void gridView1_RowDeleted(object sender, DevExpress.Data.RowDeletedEventArgs e)
        {
            //string id;
            //id = gridView1.GetFocusedRowCellValue("Id").ToString();

        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            /*string id = gridView1.GetFocusedRowCellValue("Id").ToString();
            CarregarUsuario(id);*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textEdit3_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEditarInstituicao_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoInstituicao();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirInstituicao_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirInstituicao();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInstsInserir_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarInstituicao();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        #endregion

        #region Classes

        /// <summary>
        /// Carrega combo de Status de Usuário
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatusInstCombo(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/inststatus");
                HttpResponseMessage response = await client.GetAsync("/api/orders/inststatus");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                var dados = JsonConvert.DeserializeObject<IEnumerable<InstituicaoStatus>>(dt);
                var query = from h in dados
                            select h;

                cboInstStatus.DataSource = query.ToList();
                cboInstStatus.DisplayMember = "status";
                cboInstStatus.ValueMember = "instituicao_status_id";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Adiciona Instituição
        /// </summary>
        public async void AdicionarInstituicao()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/inseririnstituicao";

                DateTime data = DateTime.Now;
                instituicao _inst = new instituicao();

                _inst.data = data;
                _inst.nome = txtNomeInst.Text;
                _inst.descricao = txtDescricaoInsti.Text;
                _inst.cnpj = txtInstCnpj.Text;
                _inst.instituicao_status = int.Parse(cboInstStatus.SelectedValue.ToString());

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/inseririnstituicao");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/inseririnstituicao");

                    var serializedUsuario = JsonConvert.SerializeObject(_inst);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                        CarregarInstituicao(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Edição de Insttuição
        /// </summary>
        public async void EdicaoInstituicao()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarinstituicao";

                instituicao _instituicao = new instituicao();

                if (txtIdInst.Text != "")
                    _instituicao.instituicao_id = int.Parse(txtIdInst.Text);

                if (txtNomeInst.Text != "")
                    _instituicao.nome = txtNomeInst.Text;

                if (txtDescricaoInsti.Text != "")
                    _instituicao.descricao = txtDescricaoInsti.Text;

                if (txtInstCnpj.Text != "")
                    _instituicao.cnpj = txtInstCnpj.Text;

                _instituicao.atualizado = DateTime.Parse(DateTime.Now.ToString());

                if (cboInstStatus.SelectedValue != null)
                    _instituicao.instituicao_status = int.Parse(cboInstStatus.SelectedValue.ToString());

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarinstituicao");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarinstituicao");

                    var serializedUsuario = JsonConvert.SerializeObject(_instituicao);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarInstituicao(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Carrega dados de Instituições
        /// </summary>
        /// <param name="tk">Token de Acesso de Usuário</param>
        public async void CarregarInstituicao (string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/Instituicao");
                HttpResponseMessage response = await client.GetAsync("/api/orders/Instituicao");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                Instituicao = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao>>(dt);
                var query = from g in dados
                            select new
                            {
                                Id = g.instituicao_id,
                                DataCriação = g.data,
                                Instituição = g.nome,
                                Status = g.status,
                                DescritivoInstituição = g.descricao,
                                CNPJ = g.cnpj,
                                Atualizado = g.atualizado,
                                Usuario = g.usuario
                            };
                gridControl1.DataSource = query;
                gridView1.Columns[0].Visible = false;
                botao = "Instituicao";
                
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega formulário de Instituição deAcordo com Id
        /// </summary>
        /// <param name="id">Id de instituição</param>
        public async void CarregarInstituicaoId(int id)
        {
            try
            {
                string nome, descricao, cnpj, status;
                int ? statusId;

                var dados = JsonConvert.DeserializeObject<IEnumerable<instituicao>>(Instituicao);

                var query = from g in dados
                            where g.instituicao_id == id
                            select new
                            {
                                Id = g.instituicao_id,
                                Status = g.status,
                                StatusId = g.instituicao_status,
                                Nome = g.nome,
                                Descricao = g.descricao,
                                Cnpj = g.cnpj
                                
                            };

                var qIntituicao = query.ToList();

                if (qIntituicao[0].Status == null)
                    status = "";
                else
                   status = qIntituicao[0].Status.ToString();

                if (qIntituicao[0].StatusId == null)
                    statusId = null;
                else
                    statusId = int.Parse(qIntituicao[0].StatusId.ToString());

                if (qIntituicao[0].Nome == null)
                    nome = "";
                else
                    nome = qIntituicao[0].Nome.ToString();

                if (qIntituicao[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qIntituicao[0].Descricao.ToString();

                if (qIntituicao[0].Cnpj == null)
                    cnpj = "";
                else
                    cnpj = qIntituicao[0].Cnpj.ToString();

                cboInstStatus.SelectedValue = statusId;
                txtNomeInst.Text = nome;
                txtDescricaoInsti.Text = descricao;
                txtInstCnpj.Text = cnpj;
                txtIdInst.Text = id.ToString();

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Instituição
        /// </summary>
        public async void ExcluirInstituicao()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagainstituicao";
                instituicao _inst = new instituicao();
                if (txtIdInst.Text != "")
                    _inst.instituicao_id = int.Parse(txtIdInst.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagainstituicao");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagainstituicao");

                    var serializedUsuario = JsonConvert.SerializeObject(_inst);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarInstituicao(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

        
    }
}