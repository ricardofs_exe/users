﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;

namespace Interface
{
    public partial class UsuariosUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        

        public UsuariosUserControl(string usuarios, string id)
        {
            InitializeComponent();

            Limpar();

            if (usuarios != "")
            {
                var dados = JsonConvert.DeserializeObject<IEnumerable<JsonModelUsuario>>(usuarios);

                var query = from g in dados
                            where g.Id == id
                            select new
                            {
                                Id = g.Id,
                                Email = g.Email,
                                Login = g.Login,
                                Data = g.Data,
                                Criado = g.Criado,
                                Atualizado = g.Atualizado,
                                Status = g.Status,
                                Especial = g.Especial,
                                Cargo = g.Cargo,
                                Nome = g.Nome
                            };

                var usuario = query.ToList();

                string criado = "", atualizado = "", status = "", especial = "", cargo = "", nome = "", login = "", data = "", email = "";

                if (usuario[0].Email == null)
                    email = "";
                else
                    email = usuario[0].Email.ToString();

                if (usuario[0].Criado == null)
                    criado = "";
                else
                    criado = usuario[0].Criado.ToString();

                if (usuario[0].Data == null)
                    data = "";
                else
                    data = usuario[0].Data.ToString();

                if (usuario[0].Login == null)
                    login = "";
                else
                    login = usuario[0].Login.ToString();

                if (usuario[0].Criado == null)
                    criado = "";
                else
                    criado = usuario[0].Criado.ToString();

                if (usuario[0].Atualizado == null)
                    atualizado = "";
                else
                    atualizado = usuario[0].Atualizado.ToString();

                if (usuario[0].Status == null)
                    status = "";
                else
                    status = usuario[0].Atualizado.ToString();

                if (usuario[0].Especial == null)
                    especial = "";
                else
                    especial = usuario[0].Especial.ToString();

                if (usuario[0].Cargo == null)
                    cargo = "";
                else
                    cargo = usuario[0].Cargo.ToString();

                if (usuario[0].Nome == null)
                    nome = "";
                else
                    nome = usuario[0].Nome.ToString();

                txtEmail.Text = email.ToString();
                txtLogin.Text = login;
                txtData.Text = data.ToString();
                txtCriado.Text = criado;
                txtAtualizado.Text = atualizado.ToString();
                txtStatus.Text = status;
                txtEspecial.Text = especial;
                txtCargo.Text = cargo;
                txtNome.Text = nome;
                usuarios = "";

            }            
        }

        public void Limpar()
        {
            txtEmail.Text = "";
            txtLogin.Text = "";
            txtData.Text = "";
            txtCriado.Text = "";
            txtAtualizado.Text = "";
            txtStatus.Text = "";
            txtEspecial.Text = "";
            txtCargo.Text = "";
            txtNome.Text = "";
        }

        /*public async string CarregarUsuario(string usuarios,string id)
        {
            var dados = JsonConvert.DeserializeObject<IEnumerable<JsonModelUsuario>>(usuarios);

            var query = from g in dados
                        where g.Id == id
                        select new
                        {
                            Id = g.Id,
                            Email = g.Email,
                            Login = g.Login,
                            Data = g.Data,
                            Criado = g.Criado,
                            Atualizado = g.Atualizado,
                            Status = g.Status,
                            Especial = g.Especial,
                            Cargo = g.Cargo,
                            Nome = g.Nome
                        };

            var usuario = query.ToList();

            string criado = "", atualizado = "", status = "", especial = "", cargo = "", nome = "", login = "", data = "", email = "";

            if (usuario[0].Email == null)
                email = "";
            else
                email = usuario[0].Email.ToString();

            if (usuario[0].Criado == null)
                criado = "";
            else
                criado = usuario[0].Criado.ToString();

            if (usuario[0].Data == null)
                data = "";
            else
                data = usuario[0].Data.ToString();

            if (usuario[0].Login == null)
                login = "";
            else
                login = usuario[0].Login.ToString();

            if (usuario[0].Criado == null)
                criado = "";
            else
                criado = usuario[0].Criado.ToString();

            if (usuario[0].Atualizado == null)
                atualizado = "";
            else
                atualizado = usuario[0].Atualizado.ToString();

            if (usuario[0].Status == null)
                status = "";
            else
                status = usuario[0].Atualizado.ToString();

            if (usuario[0].Especial == null)
                especial = "";
            else
                especial = usuario[0].Especial.ToString();

            if (usuario[0].Cargo == null)
                cargo = "";
            else
                cargo = usuario[0].Cargo.ToString();

            if (usuario[0].Nome == null)
                nome = "";
            else
                nome = usuario[0].Nome.ToString();

            txtEmail.Text = email.ToString();
            txtLogin.Text = login;
            txtData.Text = data.ToString();
            txtCriado.Text = criado;
            txtAtualizado.Text = atualizado.ToString();
            txtStatus.Text = status;
            txtEspecial.Text = especial;
            txtCargo.Text = cargo;
            txtNome.Text = nome;
        }*/
    }
}
