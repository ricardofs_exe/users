﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace Interface
{
    public partial class RodapeUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public RodapeUserControl()
        {
            InitializeComponent();
            
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            ((Form)this.Parent.Parent).Close();
        }
    }
}
