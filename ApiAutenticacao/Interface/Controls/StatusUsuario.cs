﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Net.Http;
using Newtonsoft.Json;
using Interface.Models;

namespace Interface.Controls
{
    public partial class StatusUsuario : DevExpress.XtraEditors.XtraUserControl
    {
        #region Váriaveis
        /// <summary>
        /// Variavel status Global retorno de json entre as classes
        /// </summary>
        private string status { get; set; }
        #endregion

        #region Método
        public StatusUsuario(string token, string botao, string usuario)
        {
            InitializeComponent();

            try
            {
                CarregarStatus(token);   
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        #endregion

        #region Classes
        /// <summary>
        /// Carrega Status de Usuários
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatus(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/status");
                HttpResponseMessage response = await client.GetAsync("/api/orders/status");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                status = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(dt);
                var query = from _status in dados
                            select new
                            {
                                Id = _status.usuario_status_id,
                                Status = _status.status,
                                Descrição = _status.descricao,
                                Atualizado = _status.atualizado,
                                Usuario = _status.usuario,
                            };


                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }
        #endregion
    }
}
