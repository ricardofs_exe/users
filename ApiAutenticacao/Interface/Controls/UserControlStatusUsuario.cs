﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Script.Serialization;
using DevExpress.DataAccess.Native.Json;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using Interface.Models;

namespace Interface
{
    public partial class UserControlStatusUsuario : DevExpress.XtraEditors.XtraUserControl
    {
        #region Variaveis
        /// <summary>
        /// Variavel tokenA Global que transporta o token nas classes
        /// </summary>
        private string tokenA { get; set; }

        /// <summary>
        /// Variavel usuarios Global retorno de json entre as classes
        /// </summary>
        private string usuarios { get; set; }

        /// <summary>
        /// Variavel status Global retorno de json entre as classes
        /// </summary>
        private string status { get; set; }

        /// <summary>
        /// Variavel botao Global retorna o botão que foi clicado para carregar a tela
        /// </summary>
        private string botao { get; set; }

        /// <summary>
        /// Variavel user Global retorna o botão que foi logado
        /// </summary>
        private string user { get; set; }
        
        #endregion

        #region Métodos

        public UserControlStatusUsuario(string token, string botao, string usuario)
        {
            
            InitializeComponent();


            try
            {

                #region ConfGrid

                gridView1.ActiveFilterEnabled = false;

                gridView1.OptionsView.ShowFooter = false;

                gridView1.Appearance.SelectedRow.Options.UseBackColor = false;

                gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.Editable = false;
                gridView1.OptionsBehavior.ReadOnly = true;
                gridView1.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
                gridView1.Columns.View.OptionsBehavior.KeepFocusedRowOnUpdate = true;

                gridView1.OptionsCustomization.AllowFilter = false;
                gridView1.OptionsCustomization.AllowQuickHideColumns = false;
                gridView1.OptionsCustomization.AllowColumnMoving = false;
                gridView1.OptionsCustomization.AllowSort = true;

                gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
                gridView1.OptionsFilter.AllowMRUFilterList = false;
                gridView1.OptionsFilter.AllowFilterEditor = false;
                gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
                gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;

                // pensar
                //gridView1.OptionsSelection.MultiSelectMode = true;
                gridView1.OptionsSelection.MultiSelect = true;
                gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
                gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;

                gridView1.OptionsMenu.EnableColumnMenu = false;

                gridView1.OptionsView.ShowDetailButtons = false;
                gridView1.OptionsView.ShowGroupPanel = false;
                gridView1.OptionsView.ShowIndicator = false;
                gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

                gridView1.OptionsView.ShowColumnHeaders = true;
                gridView1.OptionsView.ColumnAutoWidth = false;

                #endregion

                tokenA = token;
                user = usuario;

                gcStatus.Visible = false;
                
                switch (botao)
                {
                    case "Usuario Status":
                        gcStatus.Visible = true;
                        gcStatus.Location = new Point(6,4);
                        CarregarStatus(token);
                        break;   
                }
                
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }


        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                string id = gridView1.GetFocusedRowCellValue("Id").ToString();
                CarregarStatusId(int.Parse(id));
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void gridView1_RowDeleted(object sender, DevExpress.Data.RowDeletedEventArgs e)
        {
            //string id;
            //id = gridView1.GetFocusedRowCellValue("Id").ToString();

        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            /*string id = gridView1.GetFocusedRowCellValue("Id").ToString();
            CarregarUsuario(id);*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textEdit3_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEditarStatus_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirStatus_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInseir_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

        #region Classes

        /// <summary>
        /// Edição de Status de Usuario
        /// </summary>
        public async void EdicaoStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarstatus";

                UserStatus _status = new UserStatus();

                if (txtIdStatus.Text != "")
                    _status.usuario_status_id = int.Parse(txtIdStatus.Text);

                if (txtStatus.Text != "")
                    _status.status = txtStatus.Text;

                if (txtDescricao.Text != "")
                    _status.descricao = txtDescricao.Text;

                _status.atualizado = DateTime.Parse(DateTime.Now.ToString());
                _status.usuario = user;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarstatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarstatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_status);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarStatus(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Carrega Status de Usuários
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatus(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/status");
                HttpResponseMessage response = await client.GetAsync("/api/orders/status");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                status = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(dt);
                var query = from _status in dados
                            select new
                            {
                                Id = _status.usuario_status_id,
                                Status = _status.status,
                                Descrição = _status.descricao,
                                Atualizado = _status.atualizado,
                                Usuario = _status.usuario,
                            };


                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
                botao = "Status";
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega formulário de Status de Usuário
        /// </summary>
        /// <param name="id">Id de Sattus de Usuário</param>
        public async void CarregarStatusId(int id)
        {
            try
            {
                string t_status, descricao;
                
                var dados = JsonConvert.DeserializeObject<IEnumerable<UserStatus>>(status);

                var query = from g in dados
                            where g.usuario_status_id == id
                            select new
                            {
                                Id = g.usuario_status_id,
                                Status = g.status,
                                Descricao = g.descricao
                            };

                var qstatus = query.ToList();

                if (qstatus[0].Status == null)
                    t_status = "";
                else
                    t_status = qstatus[0].Status.ToString();

                if (qstatus[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qstatus[0].Descricao.ToString();

                txtIdStatus.Text = id.ToString();
                txtStatus.Text = t_status;
                txtDescricao.Text = descricao;                

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Status de usuário
        /// </summary>
        public async void AdicionarStatus()
        {
            try
            {
                try
                {
                    string url = "http://localhost:58260/api/orders/inserirstatus";

                    DateTime data = DateTime.Now;

                    



                    UserStatus _user = new UserStatus();

                    _user.data = data;
                    _user.status = txtStatus.Text;
                    _user.descricao = txtDescricao.Text;
                    _user.usuario = user.ToString();

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:58260/api/orders/inserirstatus");

                        client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                        HttpResponseMessage response = await client.GetAsync("/api/orders/inserirstatus");

                        var serializedUsuario = JsonConvert.SerializeObject(_user);
                        var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                        var result = await client.PostAsync(url, content);
                        string CodStatus = result.StatusCode.ToString();

                        if (CodStatus.ToString() == "OK")
                        {
                            XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                            CarregarStatus(tokenA);
                        }
                        else
                        {
                            XtraMessageBox.Show("Erro", "Confirmation");
                        }
                    }
                }
                catch (Exception er)
                {
                    XtraMessageBox.Show(er.Message, "Erro");
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Status de usuário
        /// </summary>
        public async void ExcluirStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagastatus";
                UserStatus _user = new UserStatus();
                if (txtIdStatus.Text != "")
                    _user.usuario_status_id = int.Parse(txtIdStatus.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagaatatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagaatatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarStatus(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

        private void UserControlStatusUsuario_Load(object sender, EventArgs e)
        {

        }
    }
}