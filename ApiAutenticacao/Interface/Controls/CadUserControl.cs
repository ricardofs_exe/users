﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Text.RegularExpressions;
using System.Net.Http;
using Newtonsoft.Json;

namespace Interface
{
    public partial class CadUserControl : DevExpress.XtraEditors.XtraUserControl
    {
        public CadUserControl()
        {
            InitializeComponent();
            this.BackColor = Color.White;
        }

        private void labelControl11_Click(object sender, EventArgs e)
        {

        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            try
            {
                Inserir
                (
                    txtLogin.Text,
                    txtSenha.Text,
                    txtCSenha.Text,
                    txtMail.Text,
                    txtCMail.Text
                );
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }

        public async void Inserir(string login, string senha, string ConfSenha, string Mail, string ConfMail)
        {
            try
            {
                string url = "http://localhost:58260/api/account/registrar";

                if (Validacao(login, senha, ConfSenha, Mail, ConfMail))
                {
                    Usuario _user = new Usuario();
                    _user.UserName = login.ToString();
                    _user.Password = senha.ToString();
                    _user.Email = Mail.ToString();
                    _user.ConfirmPassword = ConfSenha.ToString();

                    using (var client = new HttpClient())
                    {
                        var serializedUsuario = JsonConvert.SerializeObject(_user);
                        var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                        var result = await client.PostAsync(url, content);
                        string CodStatus = result.StatusCode.ToString();

                        if (CodStatus.ToString() == "OK")
                        {
                            XtraMessageBox.Show("Usuario Cadastrado", "Confirmation");
                            Limpar();
                        }
                        else
                        {
                            XtraMessageBox.Show("Usuario ja existe", "Confirmation");
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
            }
        }

        public bool Validacao(string login, string senha, string ConfSenha, string Mail, string ConfMail)
        {
            bool validacao = true;
            string msg = "";
            Regex rg = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");

            try
            {
                if( login == null || login.ToString() == "")
                {
                    validacao = false;
                    msg = "Campo Login não foi preenchido";
                    XtraMessageBox.Show(msg, "Confirmation");
                    txtLogin.Focus();
                    return validacao;
                }

                if (senha == null || senha.ToString() == "")
                {
                    validacao = false;
                    msg = "Campo Senha não foi preenchido";
                    XtraMessageBox.Show(msg, "Confirmation");
                    txtSenha.Focus();
                    return validacao;
                }

                if (senha != null || senha.ToString() != "")
                {
                    if (ConfSenha == null || ConfSenha.ToString() == "")
                    {
                        validacao = false;
                        msg = "Campo Confirma Senha não foi preenchido";
                        XtraMessageBox.Show(msg, "Confirmation");
                        txtCSenha.Focus();
                        return validacao;
                    }
                    else if (ConfSenha != senha)
                    {
                        validacao = false;
                        msg = "Campo Senha e Confirma Senha não são iguais";
                        XtraMessageBox.Show(msg, "Confirmation");
                        txtCSenha.Focus();
                        return validacao;
                    }
                }

                if (Mail == null || Mail.ToString() == "")
                {
                    validacao = false;
                    msg = "Campo Email não foi preenchido";
                    XtraMessageBox.Show(msg, "Confirmation");
                    txtMail.Focus();
                    return validacao;
                }

                if (!rg.IsMatch(Mail))
                {
                    validacao = false;
                    msg = "Campo Email Inválido!";
                    XtraMessageBox.Show(msg, "Confirmation");
                    txtMail.Focus();
                    return validacao;
                }

                if (Mail != null || Mail.ToString() != "")
                {
                    if (ConfMail == null || ConfMail.ToString() == "")
                    {
                        validacao = false;
                        msg = "Campo Confirma E-Mail não foi preenchido";
                        XtraMessageBox.Show(msg, "Confirmation");
                        txtCMail.Focus();
                        return validacao;
                    }
                    else if (ConfMail != Mail)
                    {
                        validacao = false;
                        msg = "Campo E-Mail e Confirma E-mail não são iguais";
                        XtraMessageBox.Show(msg, "Confirmation");
                        txtCMail.Focus();
                        return validacao;
                    }
                }

            }
            catch(Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "Confirmation");
                return false;
            }

            return validacao;
        }

        public void Limpar()
        {
            txtLogin.Text = "";
            txtSenha.Text = "";
            txtCSenha.Text = "";
            txtMail.Text = "";
            txtCMail.Text = "";
        }
    }
}
