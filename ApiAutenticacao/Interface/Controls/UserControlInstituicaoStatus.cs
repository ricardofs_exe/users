﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Script.Serialization;
using DevExpress.DataAccess.Native.Json;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraEditors.Controls;
using Interface.Models;

namespace Interface
{
    public partial class UserControlInstituicaoStatus : DevExpress.XtraEditors.XtraUserControl
    {
        #region Variaveis
        /// <summary>
        /// Variavel tokenA Global que transporta o token nas classes
        /// </summary>
        private string tokenA { get; set; }

        /// <summary>
        /// Variavel Inststatus Global retorno de json entre as classes
        /// </summary>
        private string Inststatus { get; set; }

        /// <summary>
        /// Variavel botao Global retorna o botão que foi clicado para carregar a tela
        /// </summary>
        private string botao { get; set; }

        /// <summary>
        /// Variavel user Global retorna o botão que foi logado
        /// </summary>
        private string user { get; set; }
        
        #endregion

        #region Métodos

        public UserControlInstituicaoStatus(string token, string botao, string usuario)
        {
            
            InitializeComponent();

            try
            {
                tokenA = token;
                user = usuario;
                gcStatusInst.Visible = false;

                #region ConfGrid

                gridView1.ActiveFilterEnabled = false;

                gridView1.OptionsView.ShowFooter = false;

                gridView1.Appearance.SelectedRow.Options.UseBackColor = false;

                gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.AllowFixedGroups = DevExpress.Utils.DefaultBoolean.False;
                gridView1.OptionsBehavior.Editable = false;
                gridView1.OptionsBehavior.ReadOnly = true;
                gridView1.OptionsBehavior.CopyToClipboardWithColumnHeaders = false;
                gridView1.Columns.View.OptionsBehavior.KeepFocusedRowOnUpdate = true;

                gridView1.OptionsCustomization.AllowFilter = false;
                gridView1.OptionsCustomization.AllowQuickHideColumns = false;
                gridView1.OptionsCustomization.AllowColumnMoving = false;
                gridView1.OptionsCustomization.AllowSort = true;

                gridView1.OptionsFilter.FilterEditorUseMenuForOperandsAndOperators = false;
                gridView1.OptionsFilter.ShowAllTableValuesInCheckedFilterPopup = false;
                gridView1.OptionsFilter.AllowColumnMRUFilterList = false;
                gridView1.OptionsFilter.AllowMRUFilterList = false;
                gridView1.OptionsFilter.AllowFilterEditor = false;
                gridView1.OptionsFilter.AllowFilterIncrementalSearch = false;
                gridView1.OptionsFilter.AllowMultiSelectInCheckedFilterPopup = false;

                // pensar
                //gridView1.OptionsSelection.MultiSelectMode = true;
                gridView1.OptionsSelection.MultiSelect = true;
                gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
                gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;

                gridView1.OptionsMenu.EnableColumnMenu = false;

                gridView1.OptionsView.ShowDetailButtons = false;
                gridView1.OptionsView.ShowGroupPanel = false;
                gridView1.OptionsView.ShowIndicator = false;
                gridView1.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;

                gridView1.OptionsView.ShowColumnHeaders = true;
                gridView1.OptionsView.ColumnAutoWidth = false;

                #endregion

                switch (botao)
                {
                    case "Instituição Status":
                        gcStatusInst.Visible = true;
                        gcStatusInst.Location = new Point(6, 4);
                        CarregarStatusInst(token);
                        break;
                }
                
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        private void gridView1_Click(object sender, EventArgs e)
        {
            try
            {
                string id = gridView1.GetFocusedRowCellValue("Id").ToString();
                CarregarInstStatusId(int.Parse(id));
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void gridView1_RowDeleted(object sender, DevExpress.Data.RowDeletedEventArgs e)
        {
            //string id;
            //id = gridView1.GetFocusedRowCellValue("Id").ToString();

        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            /*string id = gridView1.GetFocusedRowCellValue("Id").ToString();
            CarregarUsuario(id);*/
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textEdit3_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnEditarInst_Click(object sender, EventArgs e)
        {
            try
            {
                EdicaoInstStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnExcluirInst_Click(object sender, EventArgs e)
        {
            try
            {
                ExcluirInstStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        private void btnInserirInst_Click(object sender, EventArgs e)
        {
            try
            {
                AdicionarInstStatus();
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

        #region Classes
        /// <summary>
        /// Edição de Status de Instituicao
        /// </summary>
        public async void EdicaoInstStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/atualizarinststatus";

                InstituicaoStatus _status = new InstituicaoStatus();

                if (txtInstId.Text != "")
                    _status.instituicao_status_id = int.Parse(txtInstId.Text);

                if (txtStatusInst.Text != "")
                    _status.status = txtStatusInst.Text;

                if (txtDescricaoInst.Text != "")
                    _status.descricao = txtDescricaoInst.Text;

                _status.atualizado = DateTime.Parse(DateTime.Now.ToString());


                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/atualizarinststatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/atualizarinststatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_status);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();


                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Atulizados", "Confirmation");
                        CarregarStatusInst(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }
        
        /// <summary>
        /// Carrega Status de Usuário
        /// </summary>
        /// <param name="tk">Token de acesso de usuário</param>
        public async void CarregarStatusInst(string tk)
        {
            try
            {
                gridControl1.DataSource = null;
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("Authorization", "bearer " + tk);
                client.BaseAddress = new Uri("http://localhost:58260/api/orders/inststatus");
                HttpResponseMessage response = await client.GetAsync("/api/orders/inststatus");
                var jsonString = await response.Content.ReadAsStringAsync();
                string dt = JsonConvert.DeserializeObject<dynamic>(jsonString);
                Inststatus = dt;
                var dados = JsonConvert.DeserializeObject<IEnumerable<InstituicaoStatus>>(dt);

                var query = from _inst in dados
                            select new
                            {
                                Id = _inst.instituicao_status_id,
                                Data = _inst.data,
                                Status = _inst.status,
                                Descrição = _inst.descricao,
                                Atualizado = _inst.atualizado,
                                Usuario = _inst.usuario
                            };

                gridControl1.DataSource = query.ToList();
                gridView1.Columns[0].Visible = false;
                botao = "Instituicao Status";

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Confirmation");
            }
        }

        /// <summary>
        /// Carrega formulário de Status de Instituição
        /// </summary>
        /// <param name="id">Id de Instituição Status</param>
        public async void CarregarInstStatusId(int id)
        {
            try
            {
                string t_status, descricao;

                var dados = JsonConvert.DeserializeObject<IEnumerable<InstituicaoStatus>>(Inststatus);

                var query = from g in dados
                            where g.instituicao_status_id == id
                            select new
                            {
                                Id = g.instituicao_status_id,
                                Status = g.status,
                                Descricao = g.descricao
                            };

                var qstatus = query.ToList();

                if (qstatus[0].Status == null)
                    t_status = "";
                else
                    t_status = qstatus[0].Status.ToString();

                if (qstatus[0].Descricao == null)
                    descricao = "";
                else
                    descricao = qstatus[0].Descricao.ToString();

                txtInstId.Text = id.ToString();
                txtStatusInst.Text = t_status;
                txtDescricaoInst.Text = descricao;

            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Adiciona Status de Instituição
        /// </summary>
        public async void AdicionarInstStatus()
        {
            try
            {
                try
                {
                    string url = "http://localhost:58260/api/orders/inseririnststatus";

                    DateTime data = DateTime.Now;
                    InstituicaoStatus _user = new InstituicaoStatus();

                    _user.data = data;
                    _user.status = txtStatusInst.Text;
                    _user.descricao = txtDescricaoInst.Text;
                    _user.usuario = user.ToString();

                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri("http://localhost:58260/api/orders/inseririnststatus");

                        client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                        HttpResponseMessage response = await client.GetAsync("/api/orders/inseririnststatus");

                        var serializedUsuario = JsonConvert.SerializeObject(_user);
                        var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                        var result = await client.PostAsync(url, content);
                        string CodStatus = result.StatusCode.ToString();

                        if (CodStatus.ToString() == "OK")
                        {
                            XtraMessageBox.Show("Dados Incluidos", "Confirmation");
                            CarregarStatusInst(tokenA);
                        }
                        else
                        {
                            XtraMessageBox.Show("Erro", "Confirmation");
                        }
                    }
                }
                catch (Exception er)
                {
                    XtraMessageBox.Show(er.Message, "Erro");
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        /// <summary>
        /// Exclui Status de Instituição
        /// </summary>
        public async void ExcluirInstStatus()
        {
            try
            {
                string url = "http://localhost:58260/api/orders/apagainststatus";
                InstituicaoStatus _user = new InstituicaoStatus();
                if (txtInstId.Text != "")
                    _user.instituicao_status_id = int.Parse(txtInstId.Text);

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:58260/api/orders/apagainststatus");

                    client.DefaultRequestHeaders.Add("Authorization", "bearer " + tokenA);

                    HttpResponseMessage response = await client.GetAsync("/api/orders/apagainststatus");

                    var serializedUsuario = JsonConvert.SerializeObject(_user);
                    var content = new StringContent(serializedUsuario, Encoding.UTF8, "application/json");
                    var result = await client.PostAsync(url, content);
                    string CodStatus = result.StatusCode.ToString();

                    if (CodStatus.ToString() == "OK")
                    {
                        XtraMessageBox.Show("Dados Excluidos", "Confirmation");
                        CarregarStatusInst(tokenA);
                    }
                    else
                    {
                        XtraMessageBox.Show("Erro", "Confirmation");
                    }
                }
            }
            catch (Exception er)
            {
                XtraMessageBox.Show(er.Message, "Erro");
            }
        }

        #endregion

    }
}