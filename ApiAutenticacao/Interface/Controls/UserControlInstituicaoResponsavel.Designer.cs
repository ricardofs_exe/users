﻿namespace Interface
{
    partial class UserControlInstituicaoResponsavel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label14;
            System.Windows.Forms.Label label12;
            System.Windows.Forms.Label label23;
            System.Windows.Forms.Label label24;
            System.Windows.Forms.Label label25;
            System.Windows.Forms.Label label20;
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gcFormulario = new DevExpress.XtraEditors.GroupControl();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.cbEspecial = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cbCargo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.txtID = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluir = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditar = new DevExpress.XtraEditors.SimpleButton();
            this.txtEmail = new DevExpress.XtraEditors.TextEdit();
            this.txtNome = new DevExpress.XtraEditors.TextEdit();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtData = new DevExpress.XtraEditors.TextEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLogin = new DevExpress.XtraEditors.TextEdit();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcStatus = new DevExpress.XtraEditors.GroupControl();
            this.btnEditarStatus = new DevExpress.XtraEditors.SimpleButton();
            this.btnInseir = new DevExpress.XtraEditors.SimpleButton();
            this.txtIdStatus = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirStatus = new DevExpress.XtraEditors.SimpleButton();
            this.txtStatus = new DevExpress.XtraEditors.TextEdit();
            this.Status = new System.Windows.Forms.Label();
            this.txtDescricao = new DevExpress.XtraEditors.TextEdit();
            this.label13 = new System.Windows.Forms.Label();
            this.gcStatusInst = new DevExpress.XtraEditors.GroupControl();
            this.btnEditarInst = new DevExpress.XtraEditors.SimpleButton();
            this.btnInserirInst = new DevExpress.XtraEditors.SimpleButton();
            this.txtInstId = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirInst = new DevExpress.XtraEditors.SimpleButton();
            this.txtStatusInst = new DevExpress.XtraEditors.TextEdit();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDescricaoInst = new DevExpress.XtraEditors.TextEdit();
            this.label5 = new System.Windows.Forms.Label();
            this.gcInstituicao = new DevExpress.XtraEditors.GroupControl();
            this.cboInstStatus = new System.Windows.Forms.ComboBox();
            this.btnInstsInserir = new DevExpress.XtraEditors.SimpleButton();
            this.txtInstCnpj = new DevExpress.XtraEditors.TextEdit();
            this.txtIdInst = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirInstituicao = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditarInstituicao = new DevExpress.XtraEditors.SimpleButton();
            this.txtNomeInst = new DevExpress.XtraEditors.TextEdit();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDescricaoInsti = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.gcInstituicaoResponsavel = new DevExpress.XtraEditors.GroupControl();
            this.txtCpfCnpjInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            this.txtDescricaoInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            this.txtCelInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            this.txtTelefoneInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            this.cbInstResponsavel = new System.Windows.Forms.ComboBox();
            this.btnInserirInstResponsavel = new DevExpress.XtraEditors.SimpleButton();
            this.txtIDInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirInstResponsavel = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditarInstResponsavel = new DevExpress.XtraEditors.SimpleButton();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNomeInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            this.label18 = new System.Windows.Forms.Label();
            this.gcCentroAdministrativo = new DevExpress.XtraEditors.GroupControl();
            this.txtCpfCnpjCentroAdmin = new DevExpress.XtraEditors.TextEdit();
            this.cboInstituicaoCentroAdmin = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.btnInserirCentroAdmin = new DevExpress.XtraEditors.SimpleButton();
            this.txtDescricaoCentroAdmin = new DevExpress.XtraEditors.TextEdit();
            this.txtIdCentroAdmin = new DevExpress.XtraEditors.TextEdit();
            this.btnExcluirCentroAdmin = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditarCentroAdmin = new DevExpress.XtraEditors.SimpleButton();
            this.label21 = new System.Windows.Forms.Label();
            this.txtNomeCentroAdmin = new DevExpress.XtraEditors.TextEdit();
            this.label22 = new System.Windows.Forms.Label();
            this.txtEmailInstResponsavel = new DevExpress.XtraEditors.TextEdit();
            label6 = new System.Windows.Forms.Label();
            label14 = new System.Windows.Forms.Label();
            label12 = new System.Windows.Forms.Label();
            label23 = new System.Windows.Forms.Label();
            label24 = new System.Windows.Forms.Label();
            label25 = new System.Windows.Forms.Label();
            label20 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFormulario)).BeginInit();
            this.gcFormulario.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEspecial.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCargo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).BeginInit();
            this.gcStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricao.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatusInst)).BeginInit();
            this.gcStatusInst.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstId.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcInstituicao)).BeginInit();
            this.gcInstituicao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstCnpj.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeInst.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInsti.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcInstituicaoResponsavel)).BeginInit();
            this.gcInstituicaoResponsavel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpfCnpjInstResponsavel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInstResponsavel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCelInstResponsavel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefoneInstResponsavel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDInstResponsavel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeInstResponsavel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcCentroAdministrativo)).BeginInit();
            this.gcCentroAdministrativo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpfCnpjCentroAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoCentroAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCentroAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeCentroAdmin.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailInstResponsavel.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new System.Drawing.Point(11, 116);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(45, 13);
            label6.TabIndex = 51;
            label6.Text = "Status :";
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new System.Drawing.Point(11, 116);
            label14.Name = "label14";
            label14.Size = new System.Drawing.Size(45, 13);
            label14.TabIndex = 51;
            label14.Text = "Status :";
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new System.Drawing.Point(16, 116);
            label12.Name = "label12";
            label12.Size = new System.Drawing.Size(56, 13);
            label12.TabIndex = 51;
            label12.Text = "Telefone :";
            // 
            // label23
            // 
            label23.AutoSize = true;
            label23.Location = new System.Drawing.Point(271, 38);
            label23.Name = "label23";
            label23.Size = new System.Drawing.Size(47, 13);
            label23.TabIndex = 75;
            label23.Text = "Celular :";
            // 
            // label24
            // 
            label24.AutoSize = true;
            label24.Location = new System.Drawing.Point(271, 64);
            label24.Name = "label24";
            label24.Size = new System.Drawing.Size(60, 13);
            label24.TabIndex = 77;
            label24.Text = "Descrição :";
            // 
            // label25
            // 
            label25.AutoSize = true;
            label25.Location = new System.Drawing.Point(271, 90);
            label25.Name = "label25";
            label25.Size = new System.Drawing.Size(68, 13);
            label25.TabIndex = 79;
            label25.Text = "CPF / CNPJ :";
            // 
            // label20
            // 
            label20.AutoSize = true;
            label20.Location = new System.Drawing.Point(16, 116);
            label20.Name = "label20";
            label20.Size = new System.Drawing.Size(68, 13);
            label20.TabIndex = 81;
            label20.Text = "CPF / CNPJ :";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(2, 23);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(968, 269);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.RowDeleted += new DevExpress.Data.RowDeletedEventHandler(this.gridView1_RowDeleted);
            this.gridView1.RowUpdated += new DevExpress.XtraGrid.Views.Base.RowObjectEventHandler(this.gridView1_RowUpdated);
            this.gridView1.Click += new System.EventHandler(this.gridView1_Click);
            // 
            // gcFormulario
            // 
            this.gcFormulario.Controls.Add(this.cbStatus);
            this.gcFormulario.Controls.Add(this.cbEspecial);
            this.gcFormulario.Controls.Add(this.cbCargo);
            this.gcFormulario.Controls.Add(this.txtID);
            this.gcFormulario.Controls.Add(this.btnExcluir);
            this.gcFormulario.Controls.Add(this.btnEditar);
            this.gcFormulario.Controls.Add(this.txtEmail);
            this.gcFormulario.Controls.Add(this.txtNome);
            this.gcFormulario.Controls.Add(this.label9);
            this.gcFormulario.Controls.Add(this.label8);
            this.gcFormulario.Controls.Add(this.label7);
            this.gcFormulario.Controls.Add(label6);
            this.gcFormulario.Controls.Add(this.txtData);
            this.gcFormulario.Controls.Add(this.label3);
            this.gcFormulario.Controls.Add(this.txtLogin);
            this.gcFormulario.Controls.Add(this.label2);
            this.gcFormulario.Controls.Add(this.label1);
            this.gcFormulario.Location = new System.Drawing.Point(6, 4);
            this.gcFormulario.Name = "gcFormulario";
            this.gcFormulario.Size = new System.Drawing.Size(972, 142);
            this.gcFormulario.TabIndex = 1;
            this.gcFormulario.Text = "Usuários";
            // 
            // cbStatus
            // 
            this.cbStatus.FormattingEnabled = true;
            this.cbStatus.Location = new System.Drawing.Point(66, 113);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(164, 21);
            this.cbStatus.TabIndex = 73;
            // 
            // cbEspecial
            // 
            this.cbEspecial.Location = new System.Drawing.Point(310, 31);
            this.cbEspecial.Name = "cbEspecial";
            this.cbEspecial.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbEspecial.Size = new System.Drawing.Size(167, 20);
            this.cbEspecial.TabIndex = 70;
            // 
            // cbCargo
            // 
            this.cbCargo.Location = new System.Drawing.Point(310, 61);
            this.cbCargo.Name = "cbCargo";
            this.cbCargo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cbCargo.Size = new System.Drawing.Size(167, 20);
            this.cbCargo.TabIndex = 68;
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(867, 26);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(100, 20);
            this.txtID.TabIndex = 65;
            this.txtID.Visible = false;
            // 
            // btnExcluir
            // 
            this.btnExcluir.Location = new System.Drawing.Point(892, 116);
            this.btnExcluir.Name = "btnExcluir";
            this.btnExcluir.Size = new System.Drawing.Size(75, 23);
            this.btnExcluir.TabIndex = 64;
            this.btnExcluir.Text = "Excluir";
            this.btnExcluir.Click += new System.EventHandler(this.btnExcluir_Click);
            // 
            // btnEditar
            // 
            this.btnEditar.Location = new System.Drawing.Point(811, 116);
            this.btnEditar.Name = "btnEditar";
            this.btnEditar.Size = new System.Drawing.Size(75, 23);
            this.btnEditar.TabIndex = 63;
            this.btnEditar.Text = "Editar";
            this.btnEditar.Click += new System.EventHandler(this.btnEditar_Click);
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(310, 87);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(167, 20);
            this.txtEmail.TabIndex = 62;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(66, 35);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(164, 20);
            this.txtNome.TabIndex = 58;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(16, 38);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 57;
            this.label9.Text = "Nome : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(245, 64);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 55;
            this.label8.Text = "Cargo : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(245, 38);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 53;
            this.label7.Text = "Especial : ";
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(66, 87);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(164, 20);
            this.txtData.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 45;
            this.label3.Text = "Data : ";
            // 
            // txtLogin
            // 
            this.txtLogin.Location = new System.Drawing.Point(66, 61);
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.Size = new System.Drawing.Size(164, 20);
            this.txtLogin.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 43;
            this.label2.Text = "Login : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(245, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Email : ";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gridControl1);
            this.groupControl2.Location = new System.Drawing.Point(6, 153);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(972, 294);
            this.groupControl2.TabIndex = 2;
            this.groupControl2.Text = "Listagem";
            // 
            // gcStatus
            // 
            this.gcStatus.Controls.Add(this.btnEditarStatus);
            this.gcStatus.Controls.Add(this.btnInseir);
            this.gcStatus.Controls.Add(this.txtIdStatus);
            this.gcStatus.Controls.Add(this.btnExcluirStatus);
            this.gcStatus.Controls.Add(this.txtStatus);
            this.gcStatus.Controls.Add(this.Status);
            this.gcStatus.Controls.Add(this.txtDescricao);
            this.gcStatus.Controls.Add(this.label13);
            this.gcStatus.Location = new System.Drawing.Point(6, 451);
            this.gcStatus.Name = "gcStatus";
            this.gcStatus.Size = new System.Drawing.Size(972, 142);
            this.gcStatus.TabIndex = 71;
            this.gcStatus.Text = "Status Usuário";
            // 
            // btnEditarStatus
            // 
            this.btnEditarStatus.Location = new System.Drawing.Point(811, 116);
            this.btnEditarStatus.Name = "btnEditarStatus";
            this.btnEditarStatus.Size = new System.Drawing.Size(75, 23);
            this.btnEditarStatus.TabIndex = 67;
            this.btnEditarStatus.Text = "Editar";
            this.btnEditarStatus.Click += new System.EventHandler(this.btnEditarStatus_Click);
            // 
            // btnInseir
            // 
            this.btnInseir.Location = new System.Drawing.Point(730, 116);
            this.btnInseir.Name = "btnInseir";
            this.btnInseir.Size = new System.Drawing.Size(75, 23);
            this.btnInseir.TabIndex = 66;
            this.btnInseir.Text = "Inserir";
            this.btnInseir.Click += new System.EventHandler(this.btnInseir_Click);
            // 
            // txtIdStatus
            // 
            this.txtIdStatus.Location = new System.Drawing.Point(867, 26);
            this.txtIdStatus.Name = "txtIdStatus";
            this.txtIdStatus.Size = new System.Drawing.Size(100, 20);
            this.txtIdStatus.TabIndex = 65;
            this.txtIdStatus.Visible = false;
            // 
            // btnExcluirStatus
            // 
            this.btnExcluirStatus.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirStatus.Name = "btnExcluirStatus";
            this.btnExcluirStatus.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirStatus.TabIndex = 64;
            this.btnExcluirStatus.Text = "Excluir";
            this.btnExcluirStatus.Click += new System.EventHandler(this.btnExcluirStatus_Click);
            // 
            // txtStatus
            // 
            this.txtStatus.Location = new System.Drawing.Point(75, 35);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(164, 20);
            this.txtStatus.TabIndex = 58;
            // 
            // Status
            // 
            this.Status.AutoSize = true;
            this.Status.Location = new System.Drawing.Point(16, 38);
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(48, 13);
            this.Status.TabIndex = 57;
            this.Status.Text = "Status : ";
            // 
            // txtDescricao
            // 
            this.txtDescricao.Location = new System.Drawing.Point(75, 61);
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(164, 20);
            this.txtDescricao.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(16, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(63, 13);
            this.label13.TabIndex = 43;
            this.label13.Text = "Descrição : ";
            // 
            // gcStatusInst
            // 
            this.gcStatusInst.Controls.Add(this.btnEditarInst);
            this.gcStatusInst.Controls.Add(this.btnInserirInst);
            this.gcStatusInst.Controls.Add(this.txtInstId);
            this.gcStatusInst.Controls.Add(this.btnExcluirInst);
            this.gcStatusInst.Controls.Add(this.txtStatusInst);
            this.gcStatusInst.Controls.Add(this.label4);
            this.gcStatusInst.Controls.Add(this.txtDescricaoInst);
            this.gcStatusInst.Controls.Add(this.label5);
            this.gcStatusInst.Location = new System.Drawing.Point(6, 599);
            this.gcStatusInst.Name = "gcStatusInst";
            this.gcStatusInst.Size = new System.Drawing.Size(972, 142);
            this.gcStatusInst.TabIndex = 72;
            this.gcStatusInst.Text = "Status Instituição";
            // 
            // btnEditarInst
            // 
            this.btnEditarInst.Location = new System.Drawing.Point(811, 116);
            this.btnEditarInst.Name = "btnEditarInst";
            this.btnEditarInst.Size = new System.Drawing.Size(75, 23);
            this.btnEditarInst.TabIndex = 67;
            this.btnEditarInst.Text = "Editar";
            this.btnEditarInst.Click += new System.EventHandler(this.btnEditarInst_Click);
            // 
            // btnInserirInst
            // 
            this.btnInserirInst.Location = new System.Drawing.Point(730, 116);
            this.btnInserirInst.Name = "btnInserirInst";
            this.btnInserirInst.Size = new System.Drawing.Size(75, 23);
            this.btnInserirInst.TabIndex = 66;
            this.btnInserirInst.Text = "Inserir";
            this.btnInserirInst.Click += new System.EventHandler(this.btnInserirInst_Click);
            // 
            // txtInstId
            // 
            this.txtInstId.Location = new System.Drawing.Point(867, 26);
            this.txtInstId.Name = "txtInstId";
            this.txtInstId.Size = new System.Drawing.Size(100, 20);
            this.txtInstId.TabIndex = 65;
            this.txtInstId.Visible = false;
            // 
            // btnExcluirInst
            // 
            this.btnExcluirInst.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirInst.Name = "btnExcluirInst";
            this.btnExcluirInst.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirInst.TabIndex = 64;
            this.btnExcluirInst.Text = "Excluir";
            this.btnExcluirInst.Click += new System.EventHandler(this.btnExcluirInst_Click);
            // 
            // txtStatusInst
            // 
            this.txtStatusInst.Location = new System.Drawing.Point(75, 35);
            this.txtStatusInst.Name = "txtStatusInst";
            this.txtStatusInst.Size = new System.Drawing.Size(164, 20);
            this.txtStatusInst.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 57;
            this.label4.Text = "Status : ";
            // 
            // txtDescricaoInst
            // 
            this.txtDescricaoInst.Location = new System.Drawing.Point(75, 61);
            this.txtDescricaoInst.Name = "txtDescricaoInst";
            this.txtDescricaoInst.Size = new System.Drawing.Size(164, 20);
            this.txtDescricaoInst.TabIndex = 44;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 13);
            this.label5.TabIndex = 43;
            this.label5.Text = "Descrição : ";
            // 
            // gcInstituicao
            // 
            this.gcInstituicao.Controls.Add(this.cboInstStatus);
            this.gcInstituicao.Controls.Add(this.btnInstsInserir);
            this.gcInstituicao.Controls.Add(this.txtInstCnpj);
            this.gcInstituicao.Controls.Add(this.txtIdInst);
            this.gcInstituicao.Controls.Add(this.btnExcluirInstituicao);
            this.gcInstituicao.Controls.Add(this.btnEditarInstituicao);
            this.gcInstituicao.Controls.Add(this.txtNomeInst);
            this.gcInstituicao.Controls.Add(this.label10);
            this.gcInstituicao.Controls.Add(label14);
            this.gcInstituicao.Controls.Add(this.label15);
            this.gcInstituicao.Controls.Add(this.txtDescricaoInsti);
            this.gcInstituicao.Controls.Add(this.label16);
            this.gcInstituicao.Location = new System.Drawing.Point(6, 747);
            this.gcInstituicao.Name = "gcInstituicao";
            this.gcInstituicao.Size = new System.Drawing.Size(972, 142);
            this.gcInstituicao.TabIndex = 71;
            this.gcInstituicao.Text = "Instituição";
            // 
            // cboInstStatus
            // 
            this.cboInstStatus.FormattingEnabled = true;
            this.cboInstStatus.Location = new System.Drawing.Point(77, 113);
            this.cboInstStatus.Name = "cboInstStatus";
            this.cboInstStatus.Size = new System.Drawing.Size(164, 21);
            this.cboInstStatus.TabIndex = 72;
            // 
            // btnInstsInserir
            // 
            this.btnInstsInserir.Location = new System.Drawing.Point(730, 116);
            this.btnInstsInserir.Name = "btnInstsInserir";
            this.btnInstsInserir.Size = new System.Drawing.Size(75, 23);
            this.btnInstsInserir.TabIndex = 68;
            this.btnInstsInserir.Text = "Inserir";
            this.btnInstsInserir.Click += new System.EventHandler(this.btnInstsInserir_Click);
            // 
            // txtInstCnpj
            // 
            this.txtInstCnpj.Location = new System.Drawing.Point(77, 87);
            this.txtInstCnpj.Name = "txtInstCnpj";
            this.txtInstCnpj.Properties.BeepOnError = false;
            this.txtInstCnpj.Properties.MaskSettings.Set("MaskManagerType", typeof(DevExpress.Data.Mask.NumericMaskManager));
            this.txtInstCnpj.Properties.MaskSettings.Set("MaskManagerSignature", "allowNull=False");
            this.txtInstCnpj.Properties.MaskSettings.Set("mask", "99.999.999/9999-99");
            this.txtInstCnpj.Size = new System.Drawing.Size(164, 20);
            this.txtInstCnpj.TabIndex = 71;
            // 
            // txtIdInst
            // 
            this.txtIdInst.Location = new System.Drawing.Point(867, 26);
            this.txtIdInst.Name = "txtIdInst";
            this.txtIdInst.Size = new System.Drawing.Size(100, 20);
            this.txtIdInst.TabIndex = 65;
            this.txtIdInst.Visible = false;
            // 
            // btnExcluirInstituicao
            // 
            this.btnExcluirInstituicao.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirInstituicao.Name = "btnExcluirInstituicao";
            this.btnExcluirInstituicao.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirInstituicao.TabIndex = 64;
            this.btnExcluirInstituicao.Text = "Excluir";
            this.btnExcluirInstituicao.Click += new System.EventHandler(this.btnExcluirInstituicao_Click);
            // 
            // btnEditarInstituicao
            // 
            this.btnEditarInstituicao.Location = new System.Drawing.Point(811, 116);
            this.btnEditarInstituicao.Name = "btnEditarInstituicao";
            this.btnEditarInstituicao.Size = new System.Drawing.Size(75, 23);
            this.btnEditarInstituicao.TabIndex = 63;
            this.btnEditarInstituicao.Text = "Editar";
            this.btnEditarInstituicao.Click += new System.EventHandler(this.btnEditarInstituicao_Click);
            // 
            // txtNomeInst
            // 
            this.txtNomeInst.Location = new System.Drawing.Point(77, 35);
            this.txtNomeInst.Name = "txtNomeInst";
            this.txtNomeInst.Size = new System.Drawing.Size(164, 20);
            this.txtNomeInst.TabIndex = 58;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(16, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 57;
            this.label10.Text = "Nome : ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(16, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "CNPJ : ";
            // 
            // txtDescricaoInsti
            // 
            this.txtDescricaoInsti.Location = new System.Drawing.Point(77, 61);
            this.txtDescricaoInsti.Name = "txtDescricaoInsti";
            this.txtDescricaoInsti.Size = new System.Drawing.Size(164, 20);
            this.txtDescricaoInsti.TabIndex = 44;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(16, 64);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Descrição : ";
            // 
            // gcInstituicaoResponsavel
            // 
            this.gcInstituicaoResponsavel.Controls.Add(this.txtEmailInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.txtCpfCnpjInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(label25);
            this.gcInstituicaoResponsavel.Controls.Add(this.txtDescricaoInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(label24);
            this.gcInstituicaoResponsavel.Controls.Add(this.txtCelInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(label23);
            this.gcInstituicaoResponsavel.Controls.Add(this.txtTelefoneInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.cbInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.btnInserirInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.txtIDInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.btnExcluirInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.btnEditarInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.label11);
            this.gcInstituicaoResponsavel.Controls.Add(label12);
            this.gcInstituicaoResponsavel.Controls.Add(this.label17);
            this.gcInstituicaoResponsavel.Controls.Add(this.txtNomeInstResponsavel);
            this.gcInstituicaoResponsavel.Controls.Add(this.label18);
            this.gcInstituicaoResponsavel.Location = new System.Drawing.Point(997, 5);
            this.gcInstituicaoResponsavel.Name = "gcInstituicaoResponsavel";
            this.gcInstituicaoResponsavel.Size = new System.Drawing.Size(972, 142);
            this.gcInstituicaoResponsavel.TabIndex = 73;
            this.gcInstituicaoResponsavel.Text = "Instituição Responsável";
            // 
            // txtCpfCnpjInstResponsavel
            // 
            this.txtCpfCnpjInstResponsavel.Location = new System.Drawing.Point(344, 87);
            this.txtCpfCnpjInstResponsavel.Name = "txtCpfCnpjInstResponsavel";
            this.txtCpfCnpjInstResponsavel.Properties.BeepOnError = false;
            this.txtCpfCnpjInstResponsavel.Size = new System.Drawing.Size(164, 20);
            this.txtCpfCnpjInstResponsavel.TabIndex = 80;
            // 
            // txtDescricaoInstResponsavel
            // 
            this.txtDescricaoInstResponsavel.Location = new System.Drawing.Point(344, 61);
            this.txtDescricaoInstResponsavel.Name = "txtDescricaoInstResponsavel";
            this.txtDescricaoInstResponsavel.Properties.BeepOnError = false;
            this.txtDescricaoInstResponsavel.Size = new System.Drawing.Size(164, 20);
            this.txtDescricaoInstResponsavel.TabIndex = 78;
            // 
            // txtCelInstResponsavel
            // 
            this.txtCelInstResponsavel.Location = new System.Drawing.Point(344, 35);
            this.txtCelInstResponsavel.Name = "txtCelInstResponsavel";
            this.txtCelInstResponsavel.Properties.BeepOnError = false;
            this.txtCelInstResponsavel.Size = new System.Drawing.Size(164, 20);
            this.txtCelInstResponsavel.TabIndex = 76;
            // 
            // txtTelefoneInstResponsavel
            // 
            this.txtTelefoneInstResponsavel.Location = new System.Drawing.Point(89, 113);
            this.txtTelefoneInstResponsavel.Name = "txtTelefoneInstResponsavel";
            this.txtTelefoneInstResponsavel.Properties.BeepOnError = false;
            this.txtTelefoneInstResponsavel.Size = new System.Drawing.Size(164, 20);
            this.txtTelefoneInstResponsavel.TabIndex = 74;
            // 
            // cbInstResponsavel
            // 
            this.cbInstResponsavel.FormattingEnabled = true;
            this.cbInstResponsavel.Location = new System.Drawing.Point(89, 34);
            this.cbInstResponsavel.Name = "cbInstResponsavel";
            this.cbInstResponsavel.Size = new System.Drawing.Size(164, 21);
            this.cbInstResponsavel.TabIndex = 73;
            // 
            // btnInserirInstResponsavel
            // 
            this.btnInserirInstResponsavel.Location = new System.Drawing.Point(730, 116);
            this.btnInserirInstResponsavel.Name = "btnInserirInstResponsavel";
            this.btnInserirInstResponsavel.Size = new System.Drawing.Size(75, 23);
            this.btnInserirInstResponsavel.TabIndex = 68;
            this.btnInserirInstResponsavel.Text = "Inserir";
            this.btnInserirInstResponsavel.Click += new System.EventHandler(this.btnInserirInstResponsavel_Click);
            // 
            // txtIDInstResponsavel
            // 
            this.txtIDInstResponsavel.Location = new System.Drawing.Point(867, 26);
            this.txtIDInstResponsavel.Name = "txtIDInstResponsavel";
            this.txtIDInstResponsavel.Size = new System.Drawing.Size(100, 20);
            this.txtIDInstResponsavel.TabIndex = 65;
            this.txtIDInstResponsavel.Visible = false;
            // 
            // btnExcluirInstResponsavel
            // 
            this.btnExcluirInstResponsavel.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirInstResponsavel.Name = "btnExcluirInstResponsavel";
            this.btnExcluirInstResponsavel.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirInstResponsavel.TabIndex = 64;
            this.btnExcluirInstResponsavel.Text = "Excluir";
            this.btnExcluirInstResponsavel.Click += new System.EventHandler(this.btnExcluirInstResponsavel_Click);
            // 
            // btnEditarInstResponsavel
            // 
            this.btnEditarInstResponsavel.Location = new System.Drawing.Point(811, 116);
            this.btnEditarInstResponsavel.Name = "btnEditarInstResponsavel";
            this.btnEditarInstResponsavel.Size = new System.Drawing.Size(75, 23);
            this.btnEditarInstResponsavel.TabIndex = 63;
            this.btnEditarInstResponsavel.Text = "Editar";
            this.btnEditarInstResponsavel.Click += new System.EventHandler(this.btnEditarInstResponsavel_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 38);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 57;
            this.label11.Text = "Instituição : ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(16, 90);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(45, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "E-mail : ";
            // 
            // txtNomeInstResponsavel
            // 
            this.txtNomeInstResponsavel.Location = new System.Drawing.Point(89, 61);
            this.txtNomeInstResponsavel.Name = "txtNomeInstResponsavel";
            this.txtNomeInstResponsavel.Size = new System.Drawing.Size(164, 20);
            this.txtNomeInstResponsavel.TabIndex = 44;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(16, 64);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 43;
            this.label18.Text = "Nome : ";
            // 
            // gcCentroAdministrativo
            // 
            this.gcCentroAdministrativo.Controls.Add(this.txtCpfCnpjCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(label20);
            this.gcCentroAdministrativo.Controls.Add(this.cboInstituicaoCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.label19);
            this.gcCentroAdministrativo.Controls.Add(this.btnInserirCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.txtDescricaoCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.txtIdCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.btnExcluirCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.btnEditarCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.label21);
            this.gcCentroAdministrativo.Controls.Add(this.txtNomeCentroAdmin);
            this.gcCentroAdministrativo.Controls.Add(this.label22);
            this.gcCentroAdministrativo.Location = new System.Drawing.Point(997, 153);
            this.gcCentroAdministrativo.Name = "gcCentroAdministrativo";
            this.gcCentroAdministrativo.Size = new System.Drawing.Size(972, 142);
            this.gcCentroAdministrativo.TabIndex = 74;
            this.gcCentroAdministrativo.Text = "Centro Administrativo";
            // 
            // txtCpfCnpjCentroAdmin
            // 
            this.txtCpfCnpjCentroAdmin.Location = new System.Drawing.Point(89, 113);
            this.txtCpfCnpjCentroAdmin.Name = "txtCpfCnpjCentroAdmin";
            this.txtCpfCnpjCentroAdmin.Properties.BeepOnError = false;
            this.txtCpfCnpjCentroAdmin.Size = new System.Drawing.Size(164, 20);
            this.txtCpfCnpjCentroAdmin.TabIndex = 82;
            // 
            // cboInstituicaoCentroAdmin
            // 
            this.cboInstituicaoCentroAdmin.FormattingEnabled = true;
            this.cboInstituicaoCentroAdmin.Location = new System.Drawing.Point(89, 34);
            this.cboInstituicaoCentroAdmin.Name = "cboInstituicaoCentroAdmin";
            this.cboInstituicaoCentroAdmin.Size = new System.Drawing.Size(164, 21);
            this.cboInstituicaoCentroAdmin.TabIndex = 82;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(16, 38);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(67, 13);
            this.label19.TabIndex = 81;
            this.label19.Text = "Instituição : ";
            // 
            // btnInserirCentroAdmin
            // 
            this.btnInserirCentroAdmin.Location = new System.Drawing.Point(730, 116);
            this.btnInserirCentroAdmin.Name = "btnInserirCentroAdmin";
            this.btnInserirCentroAdmin.Size = new System.Drawing.Size(75, 23);
            this.btnInserirCentroAdmin.TabIndex = 68;
            this.btnInserirCentroAdmin.Text = "Inserir";
            this.btnInserirCentroAdmin.Click += new System.EventHandler(this.btnInserirCentroAdmin_Click);
            // 
            // txtDescricaoCentroAdmin
            // 
            this.txtDescricaoCentroAdmin.Location = new System.Drawing.Point(89, 87);
            this.txtDescricaoCentroAdmin.Name = "txtDescricaoCentroAdmin";
            this.txtDescricaoCentroAdmin.Properties.BeepOnError = false;
            this.txtDescricaoCentroAdmin.Size = new System.Drawing.Size(164, 20);
            this.txtDescricaoCentroAdmin.TabIndex = 71;
            // 
            // txtIdCentroAdmin
            // 
            this.txtIdCentroAdmin.Location = new System.Drawing.Point(867, 26);
            this.txtIdCentroAdmin.Name = "txtIdCentroAdmin";
            this.txtIdCentroAdmin.Size = new System.Drawing.Size(100, 20);
            this.txtIdCentroAdmin.TabIndex = 65;
            this.txtIdCentroAdmin.Visible = false;
            // 
            // btnExcluirCentroAdmin
            // 
            this.btnExcluirCentroAdmin.Location = new System.Drawing.Point(892, 116);
            this.btnExcluirCentroAdmin.Name = "btnExcluirCentroAdmin";
            this.btnExcluirCentroAdmin.Size = new System.Drawing.Size(75, 23);
            this.btnExcluirCentroAdmin.TabIndex = 64;
            this.btnExcluirCentroAdmin.Text = "Excluir";
            this.btnExcluirCentroAdmin.Click += new System.EventHandler(this.btnExcluirCentroAdmin_Click);
            // 
            // btnEditarCentroAdmin
            // 
            this.btnEditarCentroAdmin.Location = new System.Drawing.Point(811, 116);
            this.btnEditarCentroAdmin.Name = "btnEditarCentroAdmin";
            this.btnEditarCentroAdmin.Size = new System.Drawing.Size(75, 23);
            this.btnEditarCentroAdmin.TabIndex = 63;
            this.btnEditarCentroAdmin.Text = "Editar";
            this.btnEditarCentroAdmin.Click += new System.EventHandler(this.btnEditarCentroAdmin_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(16, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(63, 13);
            this.label21.TabIndex = 45;
            this.label21.Text = "Descricão : ";
            // 
            // txtNomeCentroAdmin
            // 
            this.txtNomeCentroAdmin.Location = new System.Drawing.Point(89, 61);
            this.txtNomeCentroAdmin.Name = "txtNomeCentroAdmin";
            this.txtNomeCentroAdmin.Size = new System.Drawing.Size(164, 20);
            this.txtNomeCentroAdmin.TabIndex = 44;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(16, 64);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(44, 13);
            this.label22.TabIndex = 43;
            this.label22.Text = "Nome : ";
            // 
            // txtEmailInstResponsavel
            // 
            this.txtEmailInstResponsavel.Location = new System.Drawing.Point(89, 87);
            this.txtEmailInstResponsavel.Name = "txtEmailInstResponsavel";
            this.txtEmailInstResponsavel.Size = new System.Drawing.Size(164, 20);
            this.txtEmailInstResponsavel.TabIndex = 81;
            // 
            // ListaUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gcCentroAdministrativo);
            this.Controls.Add(this.gcInstituicaoResponsavel);
            this.Controls.Add(this.gcInstituicao);
            this.Controls.Add(this.gcStatusInst);
            this.Controls.Add(this.gcStatus);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.gcFormulario);
            this.Name = "ListaUserControl";
            this.Size = new System.Drawing.Size(2000, 934);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcFormulario)).EndInit();
            this.gcFormulario.ResumeLayout(false);
            this.gcFormulario.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbEspecial.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbCargo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtID.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmail.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNome.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLogin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcStatus)).EndInit();
            this.gcStatus.ResumeLayout(false);
            this.gcStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatus.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricao.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcStatusInst)).EndInit();
            this.gcStatusInst.ResumeLayout(false);
            this.gcStatusInst.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstId.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStatusInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcInstituicao)).EndInit();
            this.gcInstituicao.ResumeLayout(false);
            this.gcInstituicao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtInstCnpj.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeInst.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInsti.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcInstituicaoResponsavel)).EndInit();
            this.gcInstituicaoResponsavel.ResumeLayout(false);
            this.gcInstituicaoResponsavel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpfCnpjInstResponsavel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoInstResponsavel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCelInstResponsavel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTelefoneInstResponsavel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIDInstResponsavel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeInstResponsavel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gcCentroAdministrativo)).EndInit();
            this.gcCentroAdministrativo.ResumeLayout(false);
            this.gcCentroAdministrativo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCpfCnpjCentroAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDescricaoCentroAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIdCentroAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNomeCentroAdmin.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailInstResponsavel.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraEditors.GroupControl gcFormulario;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.TextEdit txtEmail;
        private DevExpress.XtraEditors.TextEdit txtNome;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private DevExpress.XtraEditors.TextEdit txtData;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.TextEdit txtLogin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnExcluir;
        private DevExpress.XtraEditors.SimpleButton btnEditar;
        private DevExpress.XtraEditors.TextEdit txtID;
        private DevExpress.XtraEditors.ComboBoxEdit cbCargo;
        private DevExpress.XtraEditors.ComboBoxEdit cbEspecial;
        private DevExpress.XtraEditors.GroupControl gcStatus;
        private DevExpress.XtraEditors.SimpleButton btnInseir;
        private DevExpress.XtraEditors.TextEdit txtIdStatus;
        private DevExpress.XtraEditors.SimpleButton btnExcluirStatus;
        private DevExpress.XtraEditors.TextEdit txtStatus;
        private System.Windows.Forms.Label Status;
        private DevExpress.XtraEditors.TextEdit txtDescricao;
        private System.Windows.Forms.Label label13;
        private DevExpress.XtraEditors.SimpleButton btnEditarStatus;
        private DevExpress.XtraEditors.GroupControl gcStatusInst;
        private DevExpress.XtraEditors.SimpleButton btnEditarInst;
        private DevExpress.XtraEditors.SimpleButton btnInserirInst;
        private DevExpress.XtraEditors.TextEdit txtInstId;
        private DevExpress.XtraEditors.SimpleButton btnExcluirInst;
        private DevExpress.XtraEditors.TextEdit txtStatusInst;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.TextEdit txtDescricaoInst;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraEditors.GroupControl gcInstituicao;
        private DevExpress.XtraEditors.SimpleButton btnInstsInserir;
        private DevExpress.XtraEditors.TextEdit txtInstCnpj;
        private DevExpress.XtraEditors.TextEdit txtIdInst;
        private DevExpress.XtraEditors.SimpleButton btnExcluirInstituicao;
        private DevExpress.XtraEditors.SimpleButton btnEditarInstituicao;
        private DevExpress.XtraEditors.TextEdit txtNomeInst;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label15;
        private DevExpress.XtraEditors.TextEdit txtDescricaoInsti;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox cboInstStatus;
        private System.Windows.Forms.ComboBox cbStatus;
        private DevExpress.XtraEditors.GroupControl gcInstituicaoResponsavel;
        private System.Windows.Forms.ComboBox cbInstResponsavel;
        private DevExpress.XtraEditors.SimpleButton btnInserirInstResponsavel;
        private DevExpress.XtraEditors.TextEdit txtIDInstResponsavel;
        private DevExpress.XtraEditors.SimpleButton btnExcluirInstResponsavel;
        private DevExpress.XtraEditors.SimpleButton btnEditarInstResponsavel;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label17;
        private DevExpress.XtraEditors.TextEdit txtNomeInstResponsavel;
        private System.Windows.Forms.Label label18;
        private DevExpress.XtraEditors.GroupControl gcCentroAdministrativo;
        private DevExpress.XtraEditors.SimpleButton btnInserirCentroAdmin;
        private DevExpress.XtraEditors.TextEdit txtDescricaoCentroAdmin;
        private DevExpress.XtraEditors.TextEdit txtIdCentroAdmin;
        private DevExpress.XtraEditors.SimpleButton btnExcluirCentroAdmin;
        private DevExpress.XtraEditors.SimpleButton btnEditarCentroAdmin;
        private System.Windows.Forms.Label label21;
        private DevExpress.XtraEditors.TextEdit txtNomeCentroAdmin;
        private System.Windows.Forms.Label label22;
        private DevExpress.XtraEditors.TextEdit txtDescricaoInstResponsavel;
        private DevExpress.XtraEditors.TextEdit txtCelInstResponsavel;
        private DevExpress.XtraEditors.TextEdit txtTelefoneInstResponsavel;
        private DevExpress.XtraEditors.TextEdit txtCpfCnpjInstResponsavel;
        private DevExpress.XtraEditors.TextEdit txtCpfCnpjCentroAdmin;
        private System.Windows.Forms.ComboBox cboInstituicaoCentroAdmin;
        private System.Windows.Forms.Label label19;
        private DevExpress.XtraEditors.TextEdit txtEmailInstResponsavel;
    }
}
