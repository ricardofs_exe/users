﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Models
{
    class UserStatus
    {
        [Key]
        public int usuario_status_id { get; set; }
        public DateTime ? data { get; set; }
        public string status { get; set; }
        public string descricao { get; set; }
        public string criado_por { get; set; }
        public DateTime? atualizado { get; set; }
        public string usuario { get; set; }

    }
}
