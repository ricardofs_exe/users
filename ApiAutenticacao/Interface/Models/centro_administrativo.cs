﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Models
{
    public class centro_administrativo
    {
        [Key]
        public int centro_administrativo_id { get; set; }

        public DateTime? data { get; set; }

        public int instituicao { get; set; }

        public string nome { get; set; }

        public string descricao { get; set; }

        public string cpf_cnpj { get; set; }

        public int? criado_por { get; set; }

        public DateTime? atualizado { get; set; }

        public string usuario { get; set; }

        public string instituicaoN { get; set; }
    }
}
