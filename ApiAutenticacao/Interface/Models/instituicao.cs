﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface.Models
{
    public class instituicao
    {
        public int instituicao_id { get; set; }

        public DateTime? data { get; set; }

        public int instituicao_status { get; set; }

        public string status { get; set; }

        public string nome { get; set; }

        public string descricao { get; set; }

        public string cnpj { get; set; }

        public DateTime? atualizado { get; set; }

        public string usuario { get; set; }

        public string criado_por { get; set; }

    }
}
