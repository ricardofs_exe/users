﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    class UsuarioTabela
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public bool EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public bool TwoFactorEnabled { get; set; }
        public DateTime? LockoutEndDateUtc { get; set; }
        public bool LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public string UserName { get; set; }
        public int? token { get; set; }
        public string nomeCompleto { get; set; }
        public DateTime? data { get; set; }
        public string criado_por { get; set; }
        public DateTime? atualizado { get; set; }
        public int? usuario_status { get; set; }
        public bool especial { get; set; }
        public int? cargo { get; set; }
    }
}
