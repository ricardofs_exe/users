﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    class JsonModelUsuario
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public DateTime ? Data { get; set; }
        public object Criado { get; set; }
        public object Atualizado { get; set; }
        public object Status { get; set; }
        public bool Especial { get; set; }
        public object Cargo { get; set; }
        public string Mstatus { get; set; }
    }
}
